var express = require("express");
var app = express();
var http = require("http").createServer(app);
const _ = require("lodash"); // Import the lodash library

const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

var socketIO = require("socket.io")(http, {
  cors: {
    origin: "http://localhost:3000",
  },
});

const { SerialPort } = require("serialport");
const { ReadlineParser } = require("@serialport/parser-readline");
const sqlite3 = require("sqlite3").verbose(); // Import the sqlite3 package
const db = new sqlite3.Database("mydb.sqlite"); // Create or open a SQLite database file

// Create a table to store data if it doesn't exist
// db.serialize(function () {
//   db.run(
//     "CREATE TABLE IF NOT EXISTS sensor_data (id INTEGER PRIMARY KEY AUTOINCREMENT, data TEXT)"
//   );
// });

const port = new SerialPort({ path: "/dev/ttyUSB0", baudRate: 9600 });
const parser = port.pipe(new ReadlineParser({ delimiter: "\r\n" }));
// async function duplicate_remover() {
//   console.log("duplicate remover called");
//   // Fetch all records
//   const allRecords = await prisma.G1_calibration_mix.findMany();
//   console.log("number of records", allRecords.length);

//   // Group records by the field that should be unique
//   const grouped = _.groupBy(allRecords, "calibration_factor_mix");
//   console.log("number of unique records", grouped.length);

//   // Identify duplicates
//   const duplicates = Object.values(grouped).filter((group) => group.length > 1);

//   console.log("number of dupicates", duplicates.length);

//   // Delete duplicates
//   for (const duplicateGroup of duplicates) {
//     // Keep the first record, delete the rest
//     for (let i = 1; i < duplicateGroup.length; i++) {
//       await prisma.G1_calibration_mix.delete({
//         where: { id: duplicateGroup[i].id },
//       });
//     }
//   }
// }

async function duplicate_remover() {
  const prisma = new PrismaClient();

  try {
    console.log("duplicate remover called");

    // Fetch all records
    const allRecords = await prisma.G1_calibration_mix.findMany();
    console.log("number of records", allRecords.length);

    // Group records by the field that should be unique
    const grouped = _.groupBy(allRecords, "calibration_factor_mix");
    console.log("number of unique records", Object.keys(grouped).length);

    // Identify duplicates
    const duplicates = Object.values(grouped).filter(
      (group) => group.length > 1
    );
    console.log("number of duplicates", duplicates.length);

    // Delete duplicates
    for (const duplicateGroup of duplicates) {
      // Keep the first record, delete the rest
      for (let i = 1; i < duplicateGroup.length; i++) {
        await prisma.G1_calibration_mix.delete({
          where: { id: duplicateGroup[i].id },
        });
      }
    }

    console.log("Duplicates removed successfully");
  } catch (error) {
    console.error("Error:", error);
  } finally {
    await prisma.$disconnect();
  }
}

function pJson(data) {
  try {
    const jsonObject = JSON.parse(data);
    // console.log(jsonObject);

    //console.log(jsonObject);
    return jsonObject;
  } catch (error) {
    console.error("Error parsing JSON:", error);
  }
}

function sendJsonData_initialization(
  // init_data,
  calibration_mix_json_G1,
  calibration_alcohol_json_G1,

  calibration_mix_json_G2,
  calibration_alcohol_json_G2,

  calibration_mix_json_G3,
  calibration_alcohol_json_G3,

  calibration_mix_json_G4,
  calibration_alcohol_json_G4,

  // calibration_json_G2,
  // calibration_json_G3,
  // calibration_json_G4,

  G1_mix_particulars,
  G1_alcohol_particulars,

  G2_mix_particulars,
  G2_alcohol_particulars,

  G3_mix_particulars,
  G3_alcohol_particulars,

  G4_mix_particulars,
  G4_alcohol_particulars
) {
  // console.log("receieved new data", init_data);
  console.log("mix received:-", G1_mix_particulars);
  // console.log("alcohol_received:-", alcohol_particulars);

  const data_to_send = {
    status: "intialization",
    // tap_type: init_data.tap_type,

    // calibration_factor_alcohol: init_data.calibration_factor_alcohol,
    // calibration_factor_mix: init_data.calibration_factor_mix,
    /************************GROUP 1******************************* */
    g1_mix_max_vol: G1_mix_particulars.max_vol,
    g1_mix_interval_vol: G1_mix_particulars.interval_vol,

    g1_alcohol_max_vol: G1_alcohol_particulars.max_vol,
    g1_alcohol_interval_vol: G1_alcohol_particulars.interval_vol,

    G1_calibration_factor_alcohol:
      calibration_alcohol_json_G1.calibration_factor_alcohol,
    G1_calibration_factor_mix: calibration_mix_json_G1.calibration_factor_mix,
    /*********************************************************/

    /***********************GROUP 2******************************** */
    g2_mix_max_vol: G2_mix_particulars.max_vol,
    g2_mix_interval_vol: G2_mix_particulars.max_vol,

    g2_alcohol_max_vol: G2_alcohol_particulars.max_vol,
    g2_alcohol_interval_vol: G2_alcohol_particulars.interval_vol,

    G2_calibration_factor_alcohol:
      calibration_alcohol_json_G2.calibration_factor_alcohol,
    G2_calibration_factor_mix: calibration_mix_json_G2.calibration_factor_mix,
    /******************************************************* */

    /*********************GROUP 3******************************** */
    g3_mix_max_vol: G3_mix_particulars.max_vol,
    g3_mix_interval_vol: G3_mix_particulars.interval_vol,

    g3_alcohol_max_vol: G3_alcohol_particulars.max_vol,
    g3_alcohol_interval_vol: G3_alcohol_particulars.interval_vol,

    G3_calibration_factor_alcohol:
      calibration_alcohol_json_G3.calibration_factor_alcohol,
    G3_calibration_factor_mix: calibration_mix_json_G3.calibration_factor_mix,
    /***************************************************** */

    /*********************GROUP 4******************************** */
    g4_mix_max_vol: G4_mix_particulars.max_vol,
    g4_mix_interval_vol: G4_mix_particulars.interval_vol,

    g4_alcohol_max_vol: G4_alcohol_particulars.max_vol,
    g4_alcohol_interval_vol: G4_alcohol_particulars.interval_vol,

    G4_calibration_factor_alcohol:
      calibration_alcohol_json_G4.calibration_factor_alcohol,
    G4_calibration_factor_mix: calibration_mix_json_G4.calibration_factor_mix,
    /***************************************************** */
  };

  console.log("json object", data_to_send);

  const jsonToSend = JSON.stringify(data_to_send);

  port.write(jsonToSend, (error) => {
    if (error) {
      console.error("Error sending JSON data:", error);
    } else {
      console.log("JSON data sent:", jsonToSend);
    }
  });
}
function sendJsonData_start_main_measurement(init_data) {
  console.log("receieved new data", init_data);

  const data_to_send = {
    status: "main_measure",
    tap_type: init_data.tap_type,
    calibration_factor_alcohol: init_data.calibration_factor_alcohol,
    calibration_factor_mix: init_data.calibration_factor_mix,
  };

  const jsonToSend = JSON.stringify(data_to_send);

  console.log("json object", jsonToSend);

  port.write(jsonToSend, (error) => {
    if (error) {
      console.error("Error sending JSON data:", error);
    } else {
      console.log("JSON data sent:", jsonToSend);
    }
  });
}

function sendJsonData(data) {
  //console.log("receieved new data", data);

  const data_to_send = {
    status: "calibration",
    max_vol_A: 0,
    max_vol_B: 0,
    interval_vol_A: 0,
    interval_vol_B: 0,
    calibration_factor: 0,
    //tap_type: calibration.tap_type,
  };

  const jsonToSend = JSON.stringify(data_to_send);

  port.write(jsonToSend, (error) => {
    if (error) {
      console.error("Error sending JSON data:", error);
    } else {
      console.log("JSON data sent:", jsonToSend);
    }
  });
}

function send_measured_volume(data) {
  console.log("measured_vol", data.tap_type);

  const data_to_send = {
    status: "calibration",
    tap_type: data.tap_type,
    measured_vol: data.volume,
    tap_number: data.tap_number,
  };

  const jsonToSend = JSON.stringify(data_to_send);

  port.write(jsonToSend, (error) => {
    if (error) {
      console.error("Error sending JSON data:", error);
    } else {
      console.log("JSON data sent:", jsonToSend);
    }
  });
}

function sendMeasureStatus(data) {
  console.log("measure?", data.start);

  const data_to_send = {
    status: data.start,
    tap_type: data.tap_type,
    tap_number: data.tap,
  };

  const jsonToSend = JSON.stringify(data_to_send);

  port.write(jsonToSend, (error) => {
    if (error) {
      console.error("Error sending JSON data:", error);
    } else {
      console.log("JSON data sent:", jsonToSend);
    }
  });
}

function sendGaugeMeasurement(data) {
  console.log("start gauge measurement");

  const data_to_send = {
    status: "start_gauge_measurement",
    // tap_type: data.tap_type,
    // tap_number: data.tap,
  };

  const jsonToSend = JSON.stringify(data_to_send);

  port.write(jsonToSend, (error) => {
    if (error) {
      console.error("Error sending JSON data:", error);
    } else {
      console.log("JSON data sent:", jsonToSend);
    }
  });
}

function extractData(callback) {
  const query = "SELECT * FROM sensor_data";

  db.all(query, (err, rows) => {
    if (err) {
      console.error("Error extracting data from the database:", err);
      callback(err, null);
    } else {
      // Extracted data is an array of objects, each containing a "data" field
      const extractedData = rows.map((row) => row.data);
      callback(null, extractedData);
    }
  });
}

async function updateCalFunction_mix(userId, newName) {
  try {
    const user = await prisma.calibration.update({
      where: {
        id: userId,
      },
      data: {
        calibration_factor_mix: newName,
      },
    });
    return user;
  } catch (error) {
    throw error;
  } finally {
    await prisma.$disconnect();
  }
}

async function updateCalFunction_alcohol(userId, newName) {
  try {
    const user = await prisma.calibration.update({
      where: {
        id: userId,
      },
      data: {
        calibration_factor_alcohol: newName,
      },
    });
    return user;
  } catch (error) {
    throw error;
  } finally {
    await prisma.$disconnect();
  }
}

async function updateMeasurement_particulars(userId, new_data) {
  try {
    const user = await prisma.measurement_particulars.update({
      where: {
        id: userId,
      },
      data: {
        new_data,
      },
    });
    return user;
  } catch (error) {
    throw error;
  } finally {
    await prisma.$disconnect();
  }
}

async function saveGroup_1_particulars_mix(sampleData) {
  const newUser = await prisma.G1_measurement_particulars_mix.create({
    data: sampleData,
  });

  console.log("Sample data added to the database:", newUser);
}

async function saveGroup_1_particulars_alcohol(sampleData) {
  const newUser = await prisma.G1_measurement_particulars_alcohol.create({
    data: sampleData,
  });

  console.log("Sample data added to the database:", newUser);
}

async function saveGroup_2_particulars_mix(sampleData) {
  const newUser = await prisma.G2_measurement_particulars_mix.create({
    data: sampleData,
  });

  console.log("Sample data added to the database:", newUser);
}

async function saveGroup_2_particulars_alcohol(sampleData) {
  const newUser = await prisma.G2_measurement_particulars_alcohol.create({
    data: sampleData,
  });

  console.log("Sample data added to the database:", newUser);
}

async function saveGroup_3_particulars_mix(sampleData) {
  const newUser = await prisma.G3_measurement_particulars_mix.create({
    data: sampleData,
  });

  console.log("Sample data added to the database:", newUser);
}

async function saveGroup_3_particulars_alcohol(sampleData) {
  const newUser = await prisma.G3_measurement_particulars_alcohol.create({
    data: sampleData,
  });

  console.log("Sample data added to the database:", newUser);
}

async function saveGroup_4_particulars_mix(sampleData) {
  const newUser = await prisma.G4_measurement_particulars_mix.create({
    data: sampleData,
  });

  console.log("Sample data added to the database:", newUser);
}

async function saveGroup_4_particulars_alcohol(sampleData) {
  const newUser = await prisma.G4_measurement_particulars_alcohol.create({
    data: sampleData,
  });

  console.log("Sample data added to the database:", newUser);
}

async function save_initializers(incoming_data) {
  const obj_G1_measurement_particulars_mix = {
    max_vol: incoming_data.g1_mix_max_vol,
    interval_vol: incoming_data.g1_mix_interval_vol,
  };

  const obj_G1_measurement_particulars_alcohol = {
    max_vol: incoming_data.g1_alcohol_max_vol,
    interval_vol: incoming_data.g1_alcohol_interval_vol,
  };

  const obj_G2_measurement_particulars_mix = {
    max_vol: incoming_data.g2_mix_max_vol,
    interval_vol: incoming_data.g2_mix_interval_vol,
  };

  const obj_G2_measurement_particulars_alcohol = {
    max_vol: incoming_data.g2_alcohol_max_vol,
    interval_vol: incoming_data.g2_alcohol_interval_vol,
  };

  const obj_G3_measurement_particulars_mix = {
    max_vol: incoming_data.g3_mix_max_vol,
    interval_vol: incoming_data.g3_mix_interval_vol,
  };

  const obj_G3_measurement_particulars_alcohol = {
    max_vol: incoming_data.g3_alcohol_max_vol,
    interval_vol: incoming_data.g3_alcohol_interval_vol,
  };

  const obj_G4_measurement_particulars_mix = {
    max_vol: incoming_data.g4_mix_max_vol,
    interval_vol: incoming_data.g4_mix_interval_vol,
  };

  const obj_G4_measurement_particulars_alcohol = {
    max_vol: incoming_data.g4_alcohol_max_vol,
    interval_vol: incoming_data.g4_alcohol_interval_vol,
  };

  const g1_mix = await prisma.G1_measurement_particulars_mix.create({
    data: obj_G1_measurement_particulars_mix,
  });

  console.log("g1_mix added to the database:", g1_mix);

  const g1_alcohol = await prisma.G1_measurement_particulars_alcohol.create({
    data: obj_G1_measurement_particulars_alcohol,
  });
  console.log("g1_alcohol added to the database:", g1_alcohol);

  const g2_mix = await prisma.G2_measurement_particulars_mix.create({
    data: obj_G2_measurement_particulars_mix,
  });
  console.log("g2_mix added to the database:", g2_mix);

  const g2_alcohol = await prisma.G2_measurement_particulars_alcohol.create({
    data: obj_G2_measurement_particulars_alcohol,
  });

  console.log("g2_alcohol added to the database:", g2_alcohol);

  const g3_mix = await prisma.G3_measurement_particulars_alcohol.create({
    data: obj_G3_measurement_particulars_mix,
  });

  console.log("g3_mix added to the database:", g3_mix);

  const g3_alcohol = await prisma.G3_measurement_particulars_alcohol.create({
    data: obj_G3_measurement_particulars_alcohol,
  });

  console.log("g3_alcohol added to the database:", g3_alcohol);

  const g4_mix = await prisma.G4_measurement_particulars_alcohol.create({
    data: obj_G4_measurement_particulars_mix,
  });

  console.log("g4_alcohol added to the database:", g4_mix);

  const g4_alcohol = await prisma.G4_measurement_particulars_alcohol.create({
    data: obj_G4_measurement_particulars_alcohol,
  });

  console.log("g4_alcohol added to the database:", g4_alcohol);

  // console.log("Sample data added to the database:", newUser);
}

// async function saveGroup_1_particulars_alcohol(sampleData) {
//   const newUser = await prisma.calibration.create({
//     data: sampleData,
//   });

//   console.log("Sample data added to the database:", newUser);
// }

async function getCalibration() {
  // const calibration_json = await prisma.calibration.findMany();

  const calibration_mix_json_G1 = await prisma.G1_calibration_mix.findMany();
  const calibration_alcohol_json_G1 =
    await prisma.G1_calibration_alcohol.findMany();

  const calibration_mix_json_G2 = await prisma.G2_calibration_mix.findMany();
  const calibration_alcohol_json_G2 =
    await prisma.G2_calibration_alcohol.findMany();

  const calibration_mix_json_G3 = await prisma.G3_calibration_mix.findMany();
  const calibration_alcohol_json_G3 =
    await prisma.G3_calibration_alcohol.findMany();

  const calibration_mix_json_G4 = await prisma.G4_calibration_mix.findMany();
  const calibration_alcohol_json_G4 =
    await prisma.G4_calibration_alcohol.findMany();

  // console.log("calibration, G1", calibration_json_G1);
  // console.log("calibration, G2", calibration_json_G2);
  // console.log("calibration, G3", calibration_json_G3);
  // console.log("calibration, G4", calibration_json_G4);

  const G1_mix_particulars_json =
    await prisma.G1_measurement_particulars_mix.findMany();

  // console.log("g1_mix", G1_mix_particulars_json);

  const G1_alcohol_particulars_json =
    await prisma.G1_measurement_particulars_alcohol.findMany();

  const G2_mix_particulars_json =
    await prisma.G2_measurement_particulars_mix.findMany();
  const G2_alcohol_particulars_json =
    await prisma.G2_measurement_particulars_alcohol.findMany();

  const G3_mix_particulars_json =
    await prisma.G3_measurement_particulars_mix.findMany();
  const G3_alcohol_particulars_json =
    await prisma.G3_measurement_particulars_alcohol.findMany();

  const G4_mix_particulars_json =
    await prisma.G4_measurement_particulars_mix.findMany();
  const G4_alcohol_particulars_json =
    await prisma.G4_measurement_particulars_alcohol.findMany();

  sendJsonData_initialization(
    calibration_mix_json_G1[calibration_mix_json_G1.length - 1],
    calibration_alcohol_json_G1[calibration_alcohol_json_G1.length - 1],

    calibration_mix_json_G2[calibration_mix_json_G2.length - 1],
    calibration_alcohol_json_G2[calibration_alcohol_json_G2.length - 1],

    calibration_mix_json_G3[calibration_mix_json_G3.length - 1],
    calibration_alcohol_json_G3[calibration_alcohol_json_G3.length - 1],

    calibration_mix_json_G4[calibration_mix_json_G4.length - 1],
    calibration_alcohol_json_G4[calibration_alcohol_json_G4.length - 1],

    G1_mix_particulars_json[G1_mix_particulars_json.length - 1],
    G1_alcohol_particulars_json[G1_alcohol_particulars_json.length - 1],

    G2_mix_particulars_json[G2_mix_particulars_json.length - 1],
    G2_alcohol_particulars_json[G2_alcohol_particulars_json.length - 1],

    G3_mix_particulars_json[G3_mix_particulars_json.length - 1],
    G3_alcohol_particulars_json[G3_alcohol_particulars_json.length - 1],

    G4_mix_particulars_json[G4_mix_particulars_json.length - 1],
    G4_alcohol_particulars_json[G4_alcohol_particulars_json.length - 1]
  );

  //return users;
}

async function start_measurement_getCalibration() {
  const calibration_json = await prisma.calibration.findMany();
  // console.log(calibration_json[calibration_json.length - 1])

  let command = "main_measurement";

  sendJsonData_initialization(
    calibration_json[calibration_json.length - 1],
    command
  );

  // const data_to_send = {
  //   status: "main_measure",
  //   // tap_type: init_data.tap_type,
  //   // calibration_factor_alcohol: init_data.calibration_factor_alcohol,
  //   // calibration_factor_mix: init_data.calibration_factor_mix,
  // };

  // const jsonToSend = JSON.stringify(data_to_send);

  // console.log("json object", jsonToSend);

  // port.write(jsonToSend, (error) => {
  //   if (error) {
  //     console.error("Error sending JSON data:", error);
  //   } else {
  //     console.log("JSON data sent:", jsonToSend);
  //   }
  // });

  //return users;
}

async function getMeasurementParticulars(socket) {
  const G1_m_particulars_mix =
    await prisma.G1_measurement_particulars_mix.findMany();

  const G1_m_particulars_alcohol =
    await prisma.G1_measurement_particulars_alcohol.findMany();

  const G2_m_particulars_mix =
    await prisma.G2_measurement_particulars_mix.findMany();

  const G2_m_particulars_alcohol =
    await prisma.G2_measurement_particulars_alcohol.findMany();

  const G3_m_particulars_mix =
    await prisma.G3_measurement_particulars_mix.findMany();

  const G3_m_particulars_alcohol =
    await prisma.G3_measurement_particulars_alcohol.findMany();

  const G4_m_particulars_mix =
    await prisma.G4_measurement_particulars_mix.findMany();

  const G4_m_particulars_alcohol =
    await prisma.G4_measurement_particulars_alcohol.findMany();

  const particulars = {
    G1__mix_particulars: G1_m_particulars_mix[G1_m_particulars_mix.length - 1],
    G1__alcohol_particulars:
      G1_m_particulars_alcohol[G1_m_particulars_alcohol.length - 1],

    G2__mix_particulars: G2_m_particulars_mix[G2_m_particulars_mix.length - 1],
    G2__alcohol_particulars:
      G2_m_particulars_alcohol[G2_m_particulars_alcohol.length - 1],

    G3__mix_particulars: G3_m_particulars_mix[G3_m_particulars_mix.length - 1],
    G3__alcohol_particulars:
      G3_m_particulars_alcohol[G3_m_particulars_alcohol.length - 1],

    G4__mix_particulars: G4_m_particulars_mix[G4_m_particulars_mix.length - 1],
    G4__alcohol_particulars:
      G4_m_particulars_alcohol[G4_m_particulars_alcohol.length - 1],
  };

  console.log("part", particulars);

  socket.emit("refresh", {
    message: particulars,
  });

  // sendJsonData_initialization(calibration_json[calibration_json.length - 1]);

  return particulars;
}

async function updateCalibrationData(sampleData) {
  const calibration_json = await prisma.calibration.findMany();

  last_record = calibration_json[calibration_json.length - 1];

  console.log("last record", last_record.id);

  if (sampleData.tap_type == "keg") {
    console.log(
      "updatng for keg, new calibration factor",
      sampleData.calibration_factor
    );
    // updateCalFunction_mix(last_record.id, sampleData.calibration_factor);
  } else if (sampleData.tap_type == "alcohol") {
    console.log("updatng for alcohol");
    // updateCalFunction_alcohol(last_record.id, sampleData.calibration_factor);
  }

  //updateCalFunction(last_record.id);

  //sendJsonData_initialization(calibration_json[calibration_json.length - 1]);

  //return users;
}

async function saveNewCalibration(cal_data) {
  const obj = {
    calibration_factor_mix: cal_data.current_multiplier,
    error: cal_data.error,
  };
  const obj_alcohol = {
    calibration_factor_alcohol: cal_data.current_multiplier,
    error: cal_data.error,
  };
  console.log("cal_data", cal_data);

  if (cal_data.tap_number == "1" && cal_data.tap_type == "keg") {
    console.log("tap 1 calibration", obj);

    const newUser = await prisma.G1_calibration_mix.create({
      data: obj,
    });

    duplicate_remover();
  } else if (cal_data.tap_number == "2" && cal_data.tap_type == "keg") {
    const newUser = await prisma.G2_calibration_mix.create({
      data: obj,
    });
  } else if (cal_data.tap_number == "3" && cal_data.tap_type == "keg") {
    const newUser = await prisma.G3_calibration_mix.create({
      data: obj,
    });
  } else if (cal_data.tap_number == "4" && cal_data.tap_type == "keg") {
    const newUser = await prisma.G4_calibration_mix.create({
      data: obj,
    });
    /******************ALCOHOL************************************/
  } else if (cal_data.tap_number == "1" && cal_data.tap_type == "alcohol") {
    console.log("ALCOHOL HERE!!!!");
    const newUser = await prisma.G1_calibration_alcohol.create({
      data: obj_alcohol,
    });
  } else if (cal_data.tap_number == "2" && cal_data.tap_type == "alcohol") {
    const newUser = await prisma.G2_calibration_alcohol.create({
      data: obj_alcohol,
    });
  } else if (cal_data.tap_number == "3" && cal_data.tap_type == "alcohol") {
    const newUser = await prisma.G3_calibration_alcohol.create({
      data: obj_alcohol,
    });
  } else if (cal_data.tap_number == "4" && cal_data.tap_type == "alcohol") {
    const newUser = await prisma.G4_calibration_alcohol.create({
      data: obj_alcohol,
    });
  } else if (cal_data.tap_number == "3") {
  } else if (cal_data.tap_number == "4") {
  }

  // const newUser = await prisma.calibration.create({
  //   data: sampleData,
  // });
}

async function saveCalibation(data) {
  const newUser = await prisma.calibration.create({
    data: sampleData,
  });

  console.log("Sample data added to the database:", newUser);
}

// Use getUsers() and other Prisma functions as needed in your application.

var connectedClients = {}; // Track connected clients

http.listen(4001, function () {
  console.log("Server is started....");

  socketIO.on("connection", function (socket) {
    console.log("New client connected:", socket.id);

    socket.on("measure", (data) => {
      console.log("measure data", data);

      sendMeasureStatus(data);
    });

    socket.on("init", (data) => {
      //console.log("data", data);
      getCalibration(data);

      parser.on("data", (data) => {
        console.log("Received:", data);

        try {
          const jsonObject = JSON.parse(data);
          console.log("Received from serial:", jsonObject);

          if (jsonObject != null) {
            //console.log(jsonObject.status);
            stat = jsonObject.status;
            calibration_fac = jsonObject.current_multiplier;
            tap_type = jsonObject.tap_type;
            error_ = jsonObject.error;

            switch (stat) {
              case "new_calibration":
                console.log("NEW CALIBRATION RECEIVED");

                const sampleData = {
                  calibration_factor: calibration_fac,
                  tap_type: tap_type,
                  error: error_,
                };

                //console.log("data about to be saved", jsonObject);

                // postData(sampleData);
                saveNewCalibration(jsonObject);

                // updateCalibrationData(sampleData);

                break;

              case "initialized":
                console.log("INTIALIZED...");
                socket.emit("initialized", {
                  message: "connected to controller.",
                });

                // console.log("from intialized", jsonObject);
                // save_initializers(jsonObject);

                break;

              case "tap1: measuring":
                console.log("TAP 1! is measuring...");
                socket.emit("measuring", {
                  message: "tap1: measuring...",
                });

                break;

              case "tap1: measuring stopped":
                console.log("TAP 1 stopped measurement");
                socket.emit("measurement_stopped", {
                  message: "tap1: measurement stopped...",
                });

                break;

              case "tap2: measuring":
                console.log("TAP2 ! is measuring...");
                socket.emit("measuring", {
                  message: "tap2: measuring...",
                });

                break;

              case "tap2: measuring stopped":
                console.log("TAP 2 stopped measurement");
                socket.emit("measurement_stopped", {
                  message: "tap2: measurement stopped...",
                });

                break;

              case "tap3: measuring":
                console.log("TAP3 ! is measuring...");
                socket.emit("measuring", {
                  message: "tap3: measuring...",
                });

                break;

              case "tap3: measuring stopped...":
                console.log("TAP 3 stopped measurement");
                socket.emit("measurement_stopped", {
                  message: "tap3: measurement stopped...",
                });

              case "tap4: measuring stopped..":
                console.log("TAP 4 stopped measurement");
                socket.emit("measurement_stopped", {
                  message: "tap4: measurement stopped...",
                });

              case "measuring_volume_for_G1":
                console.log("measured volume for g1");
                socket.emit("G1_measured_volume_data", {
                  message: jsonObject,
                });

              case "measuring_volume_for_G2":
                console.log("measured volume for g2");
                socket.emit("G2_measured_volume_data", {
                  message: jsonObject,
                });

              case "measuring_volume_for_G3":
                console.log("measured volume for g3");
                socket.emit("G3_measured_volume_data", {
                  message: jsonObject,
                });

              case "measuring_volume_for_G4":
                console.log("measured volume for g3");
                socket.emit("G4_measured_volume_data", {
                  message: jsonObject,
                });

                break;
            }
          }

          //console.log(jsonObject.status);
        } catch (error) {
          console.error("Error parsing JSON:", error);
        }
      });
    });

    socket.on("measured_volume", (data) => {
      const jsonToInsert = JSON.stringify(data); // Convert the data object to a JSON string

      console.log("measured_data:", data);

      // After replacing data, you can send it to the serial port
      send_measured_volume(data);
    });

    socket.on("G1_form_data_mix", (data_mix) => {
      const jsonToInsert = JSON.stringify(data_mix); // Convert the data object to a JSON string
      console.log("G1 mix data:", data_mix);
      socket.emit("G1_mix_data_event", {
        message: data_mix,
      });
      console.log("emmitted...");

      saveGroup_1_particulars_mix(data_mix);
    });

    socket.on("G1_form_data_alcohol", (data_alcohol) => {
      const jsonToInsert = JSON.stringify(data_alcohol); // Convert the data object to a JSON string

      console.log("G1_alcohol_data:", data_alcohol);
      if (socket.connected) {
        socket.emit("G1_alcohol_data_event", {
          message: data_alcohol,
        });
      } else {
        console.error(
          "Socket is not connected. Event 'alcohol_data' not emitted."
        );
      }

      saveGroup_1_particulars_alcohol(data_alcohol);
    });

    socket.on("G2_form_data_mix", (data_mix) => {
      const jsonToInsert = JSON.stringify(data_mix); // Convert the data object to a JSON string
      console.log("G2 mix data:", data_mix);
      socket.emit("G2_mix_data_event", {
        message: data_mix,
      });
      console.log("emmitted...");

      saveGroup_2_particulars_mix(data_mix);
    });

    socket.on("G2_form_data_alcohol", (data_alcohol) => {
      const jsonToInsert = JSON.stringify(data_alcohol); // Convert the data object to a JSON string

      console.log("G2_alcohol_data:", data_alcohol);
      if (socket.connected) {
        socket.emit("G2_alcohol_data_event", {
          message: data_alcohol,
        });
      } else {
        console.error(
          "Socket is not connected. Event 'alcohol_data' not emitted."
        );
      }

      saveGroup_2_particulars_alcohol(data_alcohol);
    });

    socket.on("G3_form_data_mix", (data_mix) => {
      const jsonToInsert = JSON.stringify(data_mix); // Convert the data object to a JSON string
      console.log("G3 mix data:", data_mix);
      socket.emit("G3_mix_data_event", {
        message: data_mix,
      });
      console.log("emmitted...");

      saveGroup_3_particulars_mix(data_mix);
    });

    socket.on("G3_form_data_alcohol", (data_alcohol) => {
      const jsonToInsert = JSON.stringify(data_alcohol); // Convert the data object to a JSON string

      console.log("G3_alcohol_data:", data_alcohol);
      if (socket.connected) {
        socket.emit("G3_alcohol_data_event", {
          message: data_alcohol,
        });
      } else {
        console.error(
          "Socket is not connected. Event 'alcohol_data' not emitted."
        );
      }

      saveGroup_3_particulars_alcohol(data_alcohol);
    });

    socket.on("G4_form_data_mix", (data_mix) => {
      const jsonToInsert = JSON.stringify(data_mix); // Convert the data object to a JSON string
      console.log("G4 mix data:", data_mix);
      socket.emit("G4_mix_data_event", {
        message: data_mix,
      });
      console.log("emmitted...");

      saveGroup_4_particulars_mix(data_mix);
    });

    socket.on("G4_form_data_alcohol", (data_alcohol) => {
      const jsonToInsert = JSON.stringify(data_alcohol); // Convert the data object to a JSON string

      console.log("G4_alcohol_data:", data_alcohol);
      if (socket.connected) {
        socket.emit("G4_alcohol_data_event", {
          message: data_alcohol,
        });
      } else {
        console.error(
          "Socket is not connected. Event 'alcohol_data' not emitted."
        );
      }

      saveGroup_4_particulars_alcohol(data_alcohol);
    });

    socket.on("start_gauge_measurement", (data_refresh) => {
      console.log("received a ping", data_refresh);
      sendGaugeMeasurement()
      // msg = getMeasurementParticulars()
      // console.log("message", msg)
      // getMeasurementParticulars(socket);
    });

    socket.on("refresh_data", (data_refresh) => {
      console.log("received a ping", data_refresh);
      // msg = getMeasurementParticulars()
      // console.log("message", msg)
      getMeasurementParticulars(socket);
    });

    socket.on("main_switch", (data_switch) => {
      console.log("received a switch_status", data_switch);

      if (data_switch.msg == "activate") {
        start_measurement_getCalibration(data_switch);
        console.log("receive activation command");
      } else if (data_switch.msg == "deactivate") {
        //start_measurement_getCalibration(data_switch);
        console.log("received deactivattion command..");
      }

      //start_measurement_getCalibration(data_switch)
      // sendJsonData_initialization()

      // sendJsonData_start_main_measurement(data_switch)
      // start_measurement_getCalibration(data_switch);

      // msg = getMeasurementParticulars()
      // console.log("message", msg)
      // getMeasurementParticulars(socket);
    });

    socket.on("disconnect", function () {
      //clearInterval(intervalId); // Stop sending events when client disconnects
      delete connectedClients[socket.id];
      //console.log("Client disconnected:", socket.id);
    });
  });
});
