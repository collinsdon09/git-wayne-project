// app.js

const  sequelize  = require('sequelize');
const User = require('./User');

async function createUser() {
  try {
    // Connect to the database
    await sequelize.authenticate();
    console.log('Connected to the database.');

    // Synchronize the database (create tables based on models)
    await sequelize.sync();
    console.log('Database synchronized.');

    // Create a new user
    const newUser = await User.create({
      name: 'John Doe',
      phone: '123-456-7890',
      address: '123 Main Street',
    });

    console.log('User created:', newUser.toJSON());
  } catch (error) {
    console.error('Error:', error);
  } finally {
    // Close the database connection
    await sequelize.close();
    console.log('Database connection closed.');
  }
}

createUser();
