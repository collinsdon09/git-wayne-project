const { Sequelize, DataTypes } = require('sequelize');
module.exports = (Sequelize, DataTypes) => {
const User = Sequelize.define('user', {
  id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
  name: {
    type: DataTypes.STRING
  },
  email: {
    type: DataTypes.STRING
  },
 password: {
    type: DataTypes.STRING
  }
});
return User;
}