#include <ArduinoJson.h>
// keg
int solenoid_relay_1_keg = 5;
int solenoid_relay_2_keg = 6;
int solenoid_relay_3_keg = 7;
int solenoid_relay_4_keg = 8;

// alcohol
int solenoid_relay_1_alcohol = 8;
int solenoid_relay_2_alcohol = 9;
int solenoid_relay_3_alcohol = 10;
int solenoid_relay_4_alcohol = 11;

int flow_meter_1_keg = 2;
int flow_meter_2_keg = 3;
int flow_meter_3_keg = 21;
int flow_meter_4_keg = 22;

int flow_meter_1_alcohol = 18;
int flow_meter_2_alcohol = 19;
int flow_meter_3_alcohol = 20;
int flow_meter_4_alcohol = 22;

volatile long pulse_1;
volatile long pulse_2;
volatile long pulse_3;
volatile long pulse_4;

volatile long main_pulse_keg_1;
volatile long main_pulse_alcohol_1;

volatile long main_pulse_keg_2;
volatile long main_pulse_alcohol_2;

volatile long main_pulse_keg_3;
volatile long main_pulse_alcohol_3;

volatile long main_pulse_keg_4;
volatile long main_pulse_alcohol_4;

volatile long main_pulse;
float volume_1;
float volume_2;

float vol_mix_1;
float vol_alcohol_1;

float vol_mix_2;
float vol_alcohol_2;

float vol_mix_3;
float vol_alcohol_3;

float vol_mix_4;
float vol_alcohol_4;

float g1_mix_max_vol;
float g1_mix_interval_vol;
float g1_alcohol_max_vol;
float g1_alcohol_interval_vol;

float g2_mix_max_vol;
float g2_mix_interval_vol;
float g2_alcohol_max_vol;
float g2_alcohol_interval_vol;

float g3_mix_max_vol;
float g3_mix_interval_vol;
float g3_alcohol_max_vol;
float g3_alcohol_interval_vol;

float g4_mix_max_vol;
float g4_mix_interval_vol;
float g4_alcohol_max_vol;
float g4_alcohol_interval_vol;

float maxA = 0;
float maxB = 0;
float interval_vol = 0;
float calibration = 0;

// // keg
// float calibration_multiplier_flow_1_keg = 0.48;
// float calibration_multiplier_flow_2_keg = 0.48;
// float calibration_multiplier_flow_3_keg = 0.48;

// // alcohol
// float calibration_multiplier_flow_1_alcohol = 0.4523;
// float calibration_multiplier_flow_2_alcohol = 0.4523;
// float calibration_multiplier_flow_3_alcohol = 0.4523;

// keg
float calibration_multiplier_flow_1_keg = 0.48;
float calibration_multiplier_flow_2_keg = 0.48;
float calibration_multiplier_flow_3_keg = 0.48;
float calibration_multiplier_flow_4_keg = 0.48;

// alcohol
float calibration_multiplier_flow_1_alcohol = 0.4523;
float calibration_multiplier_flow_2_alcohol = 0.4523;
float calibration_multiplier_flow_3_alcohol = 0.4523;
float calibration_multiplier_flow_4_alcohol = 0.4523;

// Define the initial threshold and the threshold increment
float initialThreshold = 50.0;
float thresholdIncrement = 50.0;

String status;

// keg
// bool measure_switcher = false;
bool measure_switcher_1_keg = false;
bool measure_switcher_2_keg = false;
bool measure_switcher_3_keg = false;
bool measure_switcher_4_keg = false;

// alcohol
// bool measure_switcher = false;
bool measure_switcher_1_alcohol = false;
bool measure_switcher_2_alcohol = false;
bool measure_switcher_3_alcohol = false;
bool measure_switcher_4_alcohol = false;

bool calibration_position_over = false;
bool calibration_position_under = false;

bool start_main_measure = false;

float vol;
float vol2;
float difference;

// Function to handle adjustments
void handleAdjustments_over_cal(float measured_vol, float actual_vol, float &calibration_multiplier, String tap_type)
{

    StaticJsonDocument<500> doc;
    DynamicJsonDocument outgoing_json(500);
    // Define a threshold for making adjustments
    float adjustment_threshold = 0.1; // Adjust as needed

    // Calculate the error
    // float error = actual_vol - measured_vol;
    float error = 500 - actual_vol;

    String json_string;
    // //outgoing_json["calibration_adjusted_by"] = adjustment_factor;
    // outgoing_json["current multiplier"] = calibration_multiplier;
    // outgoing_json["error"] = error;
    // outgoing_json["actual_vol"] = actual_vol;
    // //outgoing_json["initial_multiplier"] = initial_multiplier;
    // outgoing_json["measured_vol"] = measured_vol;
    // outgoing_json["OVER"] ="over";

    serializeJson(outgoing_json, json_string);
    Serial.println(json_string);

    // Check if the error is beyond the adjustment threshold
    if (abs(error) > adjustment_threshold)
    {
        // Calculate the adjustment factor
        float adjustment_factor = abs(error) / actual_vol;

        float initial_multiplier = calibration_multiplier;

        // Apply the adjustment to the calibration multiplier
        calibration_multiplier -= adjustment_factor;

        // Reset any flags or variables related to calibration
        // Add your code to reset flags or variables as needed

        // Log the adjustment for debugging or monitoring
        // Serial.print("Calibration adjusted by: ");
        // Serial.println(adjustment_factor);

        String json_string;
        outgoing_json["status"] = "new_calibration";
        outgoing_json["under/over"] = "OVER";
        outgoing_json["calibration_adjusted_by"] = adjustment_factor;
        outgoing_json["initial_multiplier"] = initial_multiplier;
        outgoing_json["current_multiplier"] = calibration_multiplier;
        outgoing_json["error"] = error;
        outgoing_json["actual_vol"] = actual_vol;
        outgoing_json["measured_vol"] = measured_vol;
        outgoing_json["tap_type"] = tap_type;
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);

        // You can add more logic here if necessary, such as limiting the adjustment range
    }
}

// Function to handle adjustments
void handleAdjustments_under_cal(float measured_vol, float actual_vol, float &calibration_multiplier, String tap_type)
{

    StaticJsonDocument<200> doc;
    DynamicJsonDocument outgoing_json(500);
    // Define a threshold for making adjustments
    float adjustment_threshold = 0.1; // Adjust as needed

    // Calculate the error
    // float error = actual_vol - measured_vol;
    float error = 500 - actual_vol;

    // Check if the error is beyond the adjustment threshold
    if (abs(error) > adjustment_threshold)
    {
        // Calculate the adjustment factor
        float adjustment_factor = error / actual_vol;

        float initial_multiplier = calibration_multiplier;

        // Apply the adjustment to the calibration multiplier
        calibration_multiplier += adjustment_factor;
        String json_string;
        outgoing_json["status"] = "new_calibration";
        outgoing_json["under/over"] = "UNDER";
        outgoing_json["calibration_adjusted_by"] = adjustment_factor;
        outgoing_json["initial_multiplier"] = initial_multiplier;
        outgoing_json["current_multiplier"] = calibration_multiplier;
        outgoing_json["error"] = error;
        outgoing_json["actual_vol"] = actual_vol;
        outgoing_json["measured_vol"] = measured_vol;
        outgoing_json["tap_type"] = tap_type;
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);

        // You can add more logic here if necessary, such as limiting the adjustment range
    }
}

void handleAdjustments_under_cal_TAP_1(float measured_vol, float actual_vol, float &calibration_multiplier, String tap_type)
{

    StaticJsonDocument<200> doc;
    DynamicJsonDocument outgoing_json(500);
    // Define a threshold for making adjustments
    float adjustment_threshold = 0.1; // Adjust as needed

    // Calculate the error
    // float error = actual_vol - measured_vol;
    float error = 500 - actual_vol;

    // Check if the error is beyond the adjustment threshold
    if (abs(error) > adjustment_threshold)
    {
        // Calculate the adjustment factor
        float adjustment_factor = error / actual_vol;

        float initial_multiplier = calibration_multiplier;

        // Apply the adjustment to the calibration multiplier
        calibration_multiplier += adjustment_factor;
        String json_string;

        outgoing_json["status"] = "new_calibration";
        outgoing_json["under/over"] = "UNDER";
        outgoing_json["calibration_adjusted_by"] = adjustment_factor;
        outgoing_json["initial_multiplier"] = initial_multiplier;
        outgoing_json["current_multiplier"] = calibration_multiplier;
        outgoing_json["error"] = error;
        outgoing_json["actual_vol"] = actual_vol;
        outgoing_json["measured_vol"] = measured_vol;
        outgoing_json["tap_type"] = tap_type;
        outgoing_json["tap_number"] = "1";
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);

        // You can add more logic here if necessary, such as limiting the adjustment range
    }
}

void handleAdjustments_over_cal_TAP_1(float measured_vol, float actual_vol, float &calibration_multiplier, String tap_type)
{

    StaticJsonDocument<500> doc;
    DynamicJsonDocument outgoing_json(500);
    // Define a threshold for making adjustments
    float adjustment_threshold = 0.1; // Adjust as needed

    // Calculate the error
    // float error = actual_vol - measured_vol;
    float error = 500 - actual_vol;

    String json_string;
    // //outgoing_json["calibration_adjusted_by"] = adjustment_factor;
    // outgoing_json["current multiplier"] = calibration_multiplier;
    // outgoing_json["error"] = error;
    // outgoing_json["actual_vol"] = actual_vol;
    // //outgoing_json["initial_multiplier"] = initial_multiplier;
    // outgoing_json["measured_vol"] = measured_vol;
    // outgoing_json["OVER"] ="over";

    serializeJson(outgoing_json, json_string);
    Serial.println(json_string);

    // Check if the error is beyond the adjustment threshold
    if (abs(error) > adjustment_threshold)
    {
        // Calculate the adjustment factor
        float adjustment_factor = abs(error) / actual_vol;

        float initial_multiplier = calibration_multiplier;

        // Apply the adjustment to the calibration multiplier
        calibration_multiplier -= adjustment_factor;

        // Reset any flags or variables related to calibration
        // Add your code to reset flags or variables as needed

        // Log the adjustment for debugging or monitoring
        // Serial.print("Calibration adjusted by: ");
        // Serial.println(adjustment_factor);

        String json_string;
        outgoing_json["status"] = "new_calibration";
        outgoing_json["under/over"] = "OVER";
        outgoing_json["calibration_adjusted_by"] = adjustment_factor;
        outgoing_json["initial_multiplier"] = initial_multiplier;
        outgoing_json["current_multiplier"] = abs(calibration_multiplier);
        outgoing_json["error"] = error;
        outgoing_json["actual_vol"] = actual_vol;
        outgoing_json["measured_vol"] = measured_vol;
        outgoing_json["tap_type"] = tap_type;
        outgoing_json["tap_number"] = 1;
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);

        // You can add more logic here if necessary, such as limiting the adjustment range
    }
}

void handleAdjustments_under_cal_TAP_2(float measured_vol, float actual_vol, float &calibration_multiplier, String tap_type)
{

    StaticJsonDocument<200> doc;
    DynamicJsonDocument outgoing_json(500);
    // Define a threshold for making adjustments
    float adjustment_threshold = 0.1; // Adjust as needed

    // Calculate the error
    // float error = actual_vol - measured_vol;
    float error = 500 - actual_vol;

    // Check if the error is beyond the adjustment threshold
    if (abs(error) > adjustment_threshold)
    {
        // Calculate the adjustment factor
        float adjustment_factor = error / actual_vol;

        float initial_multiplier = calibration_multiplier;

        // Apply the adjustment to the calibration multiplier
        calibration_multiplier += adjustment_factor;
        String json_string;

        outgoing_json["status"] = "new_calibration";
        outgoing_json["under/over"] = "UNDER";
        outgoing_json["calibration_adjusted_by"] = adjustment_factor;
        outgoing_json["initial_multiplier"] = initial_multiplier;
        outgoing_json["current_multiplier"] = abs(calibration_multiplier);
        outgoing_json["error"] = error;
        outgoing_json["actual_vol"] = actual_vol;
        outgoing_json["measured_vol"] = measured_vol;
        outgoing_json["tap_type"] = tap_type;
        outgoing_json["tap_number"] = "2";
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);

        // You can add more logic here if necessary, such as limiting the adjustment range
    }
}

void handleAdjustments_over_cal_TAP_2(float measured_vol, float actual_vol, float &calibration_multiplier, String tap_type)
{

    StaticJsonDocument<500> doc;
    DynamicJsonDocument outgoing_json(500);
    // Define a threshold for making adjustments
    float adjustment_threshold = 0.1; // Adjust as needed

    // Calculate the error
    // float error = actual_vol - measured_vol;
    float error = 500 - actual_vol;

    String json_string;
    // //outgoing_json["calibration_adjusted_by"] = adjustment_factor;
    // outgoing_json["current multiplier"] = calibration_multiplier;
    // outgoing_json["error"] = error;
    // outgoing_json["actual_vol"] = actual_vol;
    // //outgoing_json["initial_multiplier"] = initial_multiplier;
    // outgoing_json["measured_vol"] = measured_vol;
    // outgoing_json["OVER"] ="over";

    serializeJson(outgoing_json, json_string);
    Serial.println(json_string);

    // Check if the error is beyond the adjustment threshold
    if (abs(error) > adjustment_threshold)
    {
        // Calculate the adjustment factor
        float adjustment_factor = abs(error) / actual_vol;

        float initial_multiplier = calibration_multiplier;

        // Apply the adjustment to the calibration multiplier
        calibration_multiplier -= adjustment_factor;

        // Reset any flags or variables related to calibration
        // Add your code to reset flags or variables as needed

        // Log the adjustment for debugging or monitoring
        // Serial.print("Calibration adjusted by: ");
        // Serial.println(adjustment_factor);

        String json_string;
        outgoing_json["status"] = "new_calibration";
        outgoing_json["under/over"] = "OVER";
        outgoing_json["calibration_adjusted_by"] = adjustment_factor;
        outgoing_json["initial_multiplier"] = initial_multiplier;
        outgoing_json["current_multiplier"] = abs(calibration_multiplier);
        outgoing_json["error"] = error;
        outgoing_json["actual_vol"] = actual_vol;
        outgoing_json["measured_vol"] = measured_vol;
        outgoing_json["tap_type"] = tap_type;
        outgoing_json["tap_number"] = 2;
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);

        // You can add more logic here if necessary, such as limiting the adjustment range
    }
}

void handleAdjustments_under_cal_TAP_3(float measured_vol, float actual_vol, float &calibration_multiplier, String tap_type)
{

    StaticJsonDocument<200> doc;
    DynamicJsonDocument outgoing_json(500);
    // Define a threshold for making adjustments
    float adjustment_threshold = 0.1; // Adjust as needed

    // Calculate the error
    // float error = actual_vol - measured_vol;
    float error = 500 - actual_vol;

    // Check if the error is beyond the adjustment threshold
    if (abs(error) > adjustment_threshold)
    {
        // Calculate the adjustment factor
        float adjustment_factor = error / actual_vol;

        float initial_multiplier = calibration_multiplier;

        // Apply the adjustment to the calibration multiplier
        calibration_multiplier += adjustment_factor;
        String json_string;

        outgoing_json["status"] = "new_calibration";
        outgoing_json["under/over"] = "UNDER";
        outgoing_json["calibration_adjusted_by"] = adjustment_factor;
        outgoing_json["initial_multiplier"] = initial_multiplier;
        outgoing_json["current_multiplier"] = abs(calibration_multiplier);
        outgoing_json["error"] = error;
        outgoing_json["actual_vol"] = actual_vol;
        outgoing_json["measured_vol"] = measured_vol;
        outgoing_json["tap_type"] = tap_type;
        outgoing_json["tap_number"] = "3";
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);

        // You can add more logic here if necessary, such as limiting the adjustment range
    }
}

void handleAdjustments_over_cal_TAP_3(float measured_vol, float actual_vol, float &calibration_multiplier, String tap_type)
{

    StaticJsonDocument<500> doc;
    DynamicJsonDocument outgoing_json(500);
    // Define a threshold for making adjustments
    float adjustment_threshold = 0.1; // Adjust as needed

    // Calculate the error
    // float error = actual_vol - measured_vol;
    float error = 500 - actual_vol;

    String json_string;
    // //outgoing_json["calibration_adjusted_by"] = adjustment_factor;
    // outgoing_json["current multiplier"] = calibration_multiplier;
    // outgoing_json["error"] = error;
    // outgoing_json["actual_vol"] = actual_vol;
    // //outgoing_json["initial_multiplier"] = initial_multiplier;
    // outgoing_json["measured_vol"] = measured_vol;
    // outgoing_json["OVER"] ="over";

    serializeJson(outgoing_json, json_string);
    Serial.println(json_string);

    // Check if the error is beyond the adjustment threshold
    if (abs(error) > adjustment_threshold)
    {
        // Calculate the adjustment factor
        float adjustment_factor = abs(error) / actual_vol;

        float initial_multiplier = calibration_multiplier;

        // Apply the adjustment to the calibration multiplier
        calibration_multiplier -= adjustment_factor;

        // Reset any flags or variables related to calibration
        // Add your code to reset flags or variables as needed

        // Log the adjustment for debugging or monitoring
        // Serial.print("Calibration adjusted by: ");
        // Serial.println(adjustment_factor);

        String json_string;
        outgoing_json["status"] = "new_calibration";
        outgoing_json["under/over"] = "OVER";
        outgoing_json["calibration_adjusted_by"] = adjustment_factor;
        outgoing_json["initial_multiplier"] = initial_multiplier;
        outgoing_json["current_multiplier"] = abs(calibration_multiplier);
        outgoing_json["error"] = error;
        outgoing_json["actual_vol"] = actual_vol;
        outgoing_json["measured_vol"] = measured_vol;
        outgoing_json["tap_type"] = tap_type;
        outgoing_json["tap_number"] = 3;
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);

        // You can add more logic here if necessary, such as limiting the adjustment range
    }
}

void handleAdjustments_under_cal_TAP_4(float measured_vol, float actual_vol, float &calibration_multiplier, String tap_type)
{

    StaticJsonDocument<200> doc;
    DynamicJsonDocument outgoing_json(500);
    // Define a threshold for making adjustments
    float adjustment_threshold = 0.1; // Adjust as needed

    // Calculate the error
    // float error = actual_vol - measured_vol;
    float error = 500 - actual_vol;

    // Check if the error is beyond the adjustment threshold
    if (abs(error) > adjustment_threshold)
    {
        // Calculate the adjustment factor
        float adjustment_factor = error / actual_vol;

        float initial_multiplier = calibration_multiplier;

        // Apply the adjustment to the calibration multiplier
        calibration_multiplier += adjustment_factor;
        String json_string;

        outgoing_json["status"] = "new_calibration";
        outgoing_json["under/over"] = "UNDER";
        outgoing_json["calibration_adjusted_by"] = adjustment_factor;
        outgoing_json["initial_multiplier"] = initial_multiplier;
        outgoing_json["current_multiplier"] = abs(calibration_multiplier);
        outgoing_json["error"] = error;
        outgoing_json["actual_vol"] = actual_vol;
        outgoing_json["measured_vol"] = measured_vol;
        outgoing_json["tap_type"] = tap_type;
        outgoing_json["tap_number"] = "4";
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);

        // You can add more logic here if necessary, such as limiting the adjustment range
    }
}

void handleAdjustments_over_cal_TAP_4(float measured_vol, float actual_vol, float &calibration_multiplier, String tap_type)
{

    StaticJsonDocument<500> doc;
    DynamicJsonDocument outgoing_json(500);
    // Define a threshold for making adjustments
    float adjustment_threshold = 0.1; // Adjust as needed

    // Calculate the error
    // float error = actual_vol - measured_vol;
    float error = 500 - actual_vol;

    String json_string;
    serializeJson(outgoing_json, json_string);
    Serial.println(json_string);

    // Check if the error is beyond the adjustment threshold
    if (abs(error) > adjustment_threshold)
    {
        // Calculate the adjustment factor
        float adjustment_factor = abs(error) / actual_vol;

        float initial_multiplier = calibration_multiplier;

        // Apply the adjustment to the calibration multiplier
        calibration_multiplier -= adjustment_factor;
        String json_string;
        outgoing_json["status"] = "new_calibration";
        outgoing_json["under/over"] = "OVER";
        outgoing_json["calibration_adjusted_by"] = adjustment_factor;
        outgoing_json["initial_multiplier"] = initial_multiplier;
        outgoing_json["current_multiplier"] = abs(calibration_multiplier);
        outgoing_json["error"] = error;
        outgoing_json["actual_vol"] = actual_vol;
        outgoing_json["measured_vol"] = measured_vol;
        outgoing_json["tap_type"] = tap_type;
        outgoing_json["tap_number"] = 4;
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);

        // You can add more logic here if necessary, such as limiting the adjustment range
    }
}

// Alcohol_handleAdjustments_over_cal_TAP_1(vol, actual_volume, calibration_multiplier_flow_1_alcohol, "alcohol");

// Alcohol_handleAdjustments_under_cal_TAP_1(vol, actual_volume, calibration_multiplier_flow_1_alcohol, "alcohol");

void Alcohol_handleAdjustments_under_cal_TAP_1(float measured_vol, float actual_vol, float &calibration_multiplier, String tap_type)
{

    StaticJsonDocument<200> doc;
    DynamicJsonDocument outgoing_json(500);
    // Define a threshold for making adjustments
    float adjustment_threshold = 0.1; // Adjust as needed

    // Calculate the error
    // float error = actual_vol - measured_vol;
    float error = 500 - actual_vol;

    // Check if the error is beyond the adjustment threshold
    if (abs(error) > adjustment_threshold)
    {
        // Calculate the adjustment factor
        float adjustment_factor = error / actual_vol;

        float initial_multiplier = calibration_multiplier;

        // Apply the adjustment to the calibration multiplier
        calibration_multiplier += adjustment_factor;
        String json_string;

        outgoing_json["status"] = "new_calibration";
        outgoing_json["under/over"] = "UNDER";
        outgoing_json["calibration_adjusted_by"] = adjustment_factor;
        outgoing_json["initial_multiplier"] = initial_multiplier;
        outgoing_json["current_multiplier"] = abs(calibration_multiplier);
        outgoing_json["error"] = error;
        outgoing_json["actual_vol"] = actual_vol;
        outgoing_json["measured_vol"] = measured_vol;
        outgoing_json["tap_type"] = tap_type;
        outgoing_json["tap_number"] = "1";
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);

        // You can add more logic here if necessary, such as limiting the adjustment range
    }
}

void Alcohol_handleAdjustments_over_cal_TAP_1(float measured_vol, float actual_vol, float &calibration_multiplier, String tap_type)
{

    StaticJsonDocument<500> doc;
    DynamicJsonDocument outgoing_json(500);
    // Define a threshold for making adjustments
    float adjustment_threshold = 0.1; // Adjust as needed

    // Calculate the error
    // float error = actual_vol - measured_vol;
    float error = 500 - actual_vol;

    String json_string;
    serializeJson(outgoing_json, json_string);
    Serial.println(json_string);

    // Check if the error is beyond the adjustment threshold
    if (abs(error) > adjustment_threshold)
    {
        // Calculate the adjustment factor
        float adjustment_factor = abs(error) / actual_vol;

        float initial_multiplier = calibration_multiplier;

        // Apply the adjustment to the calibration multiplier
        calibration_multiplier -= adjustment_factor;
        String json_string;
        outgoing_json["status"] = "new_calibration";
        outgoing_json["under/over"] = "OVER";
        outgoing_json["calibration_adjusted_by"] = adjustment_factor;
        outgoing_json["initial_multiplier"] = initial_multiplier;
        outgoing_json["current_multiplier"] = abs(calibration_multiplier);
        outgoing_json["error"] = error;
        outgoing_json["actual_vol"] = actual_vol;
        outgoing_json["measured_vol"] = measured_vol;
        outgoing_json["tap_type"] = tap_type;
        outgoing_json["tap_number"] = 1;
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);

        // You can add more logic here if necessary, such as limiting the adjustment range
    }
}

void Alcohol_handleAdjustments_under_cal_TAP_2(float measured_vol, float actual_vol, float &calibration_multiplier, String tap_type)
{

    StaticJsonDocument<200> doc;
    DynamicJsonDocument outgoing_json(500);
    // Define a threshold for making adjustments
    float adjustment_threshold = 0.1; // Adjust as needed

    // Calculate the error
    // float error = actual_vol - measured_vol;
    float error = 500 - actual_vol;

    // Check if the error is beyond the adjustment threshold
    if (abs(error) > adjustment_threshold)
    {
        // Calculate the adjustment factor
        float adjustment_factor = error / actual_vol;

        float initial_multiplier = calibration_multiplier;

        // Apply the adjustment to the calibration multiplier
        calibration_multiplier += adjustment_factor;
        String json_string;

        outgoing_json["status"] = "new_calibration";
        outgoing_json["under/over"] = "UNDER";
        outgoing_json["calibration_adjusted_by"] = adjustment_factor;
        outgoing_json["initial_multiplier"] = initial_multiplier;
        outgoing_json["current_multiplier"] = abs(calibration_multiplier);
        outgoing_json["error"] = error;
        outgoing_json["actual_vol"] = actual_vol;
        outgoing_json["measured_vol"] = measured_vol;
        outgoing_json["tap_type"] = tap_type;
        outgoing_json["tap_number"] = "2";
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);

        // You can add more logic here if necessary, such as limiting the adjustment range
    }
}

void Alcohol_handleAdjustments_over_cal_TAP_2(float measured_vol, float actual_vol, float &calibration_multiplier, String tap_type)
{

    StaticJsonDocument<500> doc;
    DynamicJsonDocument outgoing_json(500);
    // Define a threshold for making adjustments
    float adjustment_threshold = 0.1; // Adjust as needed

    // Calculate the error
    // float error = actual_vol - measured_vol;
    float error = 500 - actual_vol;

    String json_string;
    serializeJson(outgoing_json, json_string);
    Serial.println(json_string);

    // Check if the error is beyond the adjustment threshold
    if (abs(error) > adjustment_threshold)
    {
        // Calculate the adjustment factor
        float adjustment_factor = abs(error) / actual_vol;

        float initial_multiplier = calibration_multiplier;

        // Apply the adjustment to the calibration multiplier
        calibration_multiplier -= adjustment_factor;
        String json_string;
        outgoing_json["status"] = "new_calibration";
        outgoing_json["under/over"] = "OVER";
        outgoing_json["calibration_adjusted_by"] = adjustment_factor;
        outgoing_json["initial_multiplier"] = initial_multiplier;
        outgoing_json["current_multiplier"] = abs(calibration_multiplier);
        outgoing_json["error"] = error;
        outgoing_json["actual_vol"] = actual_vol;
        outgoing_json["measured_vol"] = measured_vol;
        outgoing_json["tap_type"] = tap_type;
        outgoing_json["tap_number"] = 2;
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);

        // You can add more logic here if necessary, such as limiting the adjustment range
    }
}

void Alcohol_handleAdjustments_under_cal_TAP_3(float measured_vol, float actual_vol, float &calibration_multiplier, String tap_type)
{

    StaticJsonDocument<200> doc;
    DynamicJsonDocument outgoing_json(500);
    // Define a threshold for making adjustments
    float adjustment_threshold = 0.1; // Adjust as needed

    // Calculate the error
    // float error = actual_vol - measured_vol;
    float error = 500 - actual_vol;

    // Check if the error is beyond the adjustment threshold
    if (abs(error) > adjustment_threshold)
    {
        // Calculate the adjustment factor
        float adjustment_factor = error / actual_vol;

        float initial_multiplier = calibration_multiplier;

        // Apply the adjustment to the calibration multiplier
        calibration_multiplier += adjustment_factor;
        String json_string;

        outgoing_json["status"] = "new_calibration";
        outgoing_json["under/over"] = "UNDER";
        outgoing_json["calibration_adjusted_by"] = adjustment_factor;
        outgoing_json["initial_multiplier"] = initial_multiplier;
        outgoing_json["current_multiplier"] = abs(calibration_multiplier);
        outgoing_json["error"] = error;
        outgoing_json["actual_vol"] = actual_vol;
        outgoing_json["measured_vol"] = measured_vol;
        outgoing_json["tap_type"] = tap_type;
        outgoing_json["tap_number"] = "3";
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);

        // You can add more logic here if necessary, such as limiting the adjustment range
    }
}

void Alcohol_handleAdjustments_over_cal_TAP_3(float measured_vol, float actual_vol, float &calibration_multiplier, String tap_type)
{

    StaticJsonDocument<500> doc;
    DynamicJsonDocument outgoing_json(500);
    // Define a threshold for making adjustments
    float adjustment_threshold = 0.1; // Adjust as needed

    // Calculate the error
    // float error = actual_vol - measured_vol;
    float error = 500 - actual_vol;

    String json_string;
    serializeJson(outgoing_json, json_string);
    Serial.println(json_string);

    // Check if the error is beyond the adjustment threshold
    if (abs(error) > adjustment_threshold)
    {
        // Calculate the adjustment factor
        float adjustment_factor = abs(error) / actual_vol;

        float initial_multiplier = calibration_multiplier;

        // Apply the adjustment to the calibration multiplier
        calibration_multiplier -= adjustment_factor;
        String json_string;
        outgoing_json["status"] = "new_calibration";
        outgoing_json["under/over"] = "OVER";
        outgoing_json["calibration_adjusted_by"] = adjustment_factor;
        outgoing_json["initial_multiplier"] = initial_multiplier;
        outgoing_json["current_multiplier"] = abs(calibration_multiplier);
        outgoing_json["error"] = error;
        outgoing_json["actual_vol"] = actual_vol;
        outgoing_json["measured_vol"] = measured_vol;
        outgoing_json["tap_type"] = tap_type;
        outgoing_json["tap_number"] = 3;
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);

        // You can add more logic here if necessary, such as limiting the adjustment range
    }
}

void Alcohol_handleAdjustments_under_cal_TAP_4(float measured_vol, float actual_vol, float &calibration_multiplier, String tap_type)
{

    StaticJsonDocument<200> doc;
    DynamicJsonDocument outgoing_json(500);
    // Define a threshold for making adjustments
    float adjustment_threshold = 0.1; // Adjust as needed

    // Calculate the error
    // float error = actual_vol - measured_vol;
    float error = 500 - actual_vol;

    // Check if the error is beyond the adjustment threshold
    if (abs(error) > adjustment_threshold)
    {
        // Calculate the adjustment factor
        float adjustment_factor = error / actual_vol;

        float initial_multiplier = calibration_multiplier;

        // Apply the adjustment to the calibration multiplier
        calibration_multiplier += adjustment_factor;
        String json_string;

        outgoing_json["status"] = "new_calibration";
        outgoing_json["under/over"] = "UNDER";
        outgoing_json["calibration_adjusted_by"] = adjustment_factor;
        outgoing_json["initial_multiplier"] = initial_multiplier;
        outgoing_json["current_multiplier"] = abs(calibration_multiplier);
        outgoing_json["error"] = error;
        outgoing_json["actual_vol"] = actual_vol;
        outgoing_json["measured_vol"] = measured_vol;
        outgoing_json["tap_type"] = tap_type;
        outgoing_json["tap_number"] = "4";
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);

        // You can add more logic here if necessary, such as limiting the adjustment range
    }
}

void Alcohol_handleAdjustments_over_cal_TAP_4(float measured_vol, float actual_vol, float &calibration_multiplier, String tap_type)
{

    StaticJsonDocument<500> doc;
    DynamicJsonDocument outgoing_json(500);
    // Define a threshold for making adjustments
    float adjustment_threshold = 0.1; // Adjust as needed

    // Calculate the error
    // float error = actual_vol - measured_vol;
    float error = 500 - actual_vol;

    String json_string;
    serializeJson(outgoing_json, json_string);
    Serial.println(json_string);

    // Check if the error is beyond the adjustment threshold
    if (abs(error) > adjustment_threshold)
    {
        // Calculate the adjustment factor
        float adjustment_factor = abs(error) / actual_vol;

        float initial_multiplier = calibration_multiplier;

        // Apply the adjustment to the calibration multiplier
        calibration_multiplier -= adjustment_factor;
        String json_string;
        outgoing_json["status"] = "new_calibration";
        outgoing_json["under/over"] = "OVER";
        outgoing_json["calibration_adjusted_by"] = adjustment_factor;
        outgoing_json["initial_multiplier"] = initial_multiplier;
        outgoing_json["current_multiplier"] = abs(calibration_multiplier);
        outgoing_json["error"] = error;
        outgoing_json["actual_vol"] = actual_vol;
        outgoing_json["measured_vol"] = measured_vol;
        outgoing_json["tap_type"] = tap_type;
        outgoing_json["tap_number"] = 4;
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);

        // You can add more logic here if necessary, such as limiting the adjustment range
    }
}

void setup()
{
    Serial.begin(9600);
    attachInterrupt(digitalPinToInterrupt(flow_meter_1_keg), main_increase_keg_1, RISING);
    attachInterrupt(digitalPinToInterrupt(flow_meter_2_keg), main_increase_keg_2, RISING);
    attachInterrupt(digitalPinToInterrupt(flow_meter_3_keg), main_increase_keg_3, RISING);
    attachInterrupt(digitalPinToInterrupt(flow_meter_4_keg), main_increase_keg_4, RISING);

    attachInterrupt(digitalPinToInterrupt(flow_meter_1_alcohol), main_increase_alcohol_1, RISING);
    attachInterrupt(digitalPinToInterrupt(flow_meter_2_alcohol), main_increase_alcohol_2, RISING);
    attachInterrupt(digitalPinToInterrupt(flow_meter_3_alcohol), main_increase_alcohol_3, RISING);
    attachInterrupt(digitalPinToInterrupt(flow_meter_4_alcohol), main_increase_alcohol_4, RISING);
}

void loop()
{
    StaticJsonDocument<1000> doc;
    DynamicJsonDocument outgoing_json(1000);

    if (Serial.available() > 0)
    {
        String incomingChar = Serial.readString();
        DeserializationError error = deserializeJson(doc, incomingChar);

        if (error)
        {
            Serial.print(F("deserializeJson() failed: "));
            Serial.println(error.f_str());
            return;
        }

        String json_string;

        if (doc["status"] == "start_gauge_measurement")
        {
            outgoing_json["status"] = "started_gauge_measurement!";
            // outgoing_json["max_vol_A"] = 1000;
            // outgoing_json["max_vol_B"] = maxB;
            // outgoing_json["interval_vol"] = interval_vol;
            switch_gauge_measurement = true;
            serializeJson(outgoing_json, json_string);
            Serial.println(json_string);
        }

        else if (doc["status"] == "intialization")
        {
            // outgoing_json["calibration_factor_mix"] = doc["calibration_factor_mix"];
            // outgoing_json["calibration_factor_alcohol"] = doc["calibration_factor_alcohol"];

            calibration_multiplier_flow_1_keg = doc["G1_calibration_factor_mix"];
            calibration_multiplier_flow_2_keg = doc["G2_calibration_factor_mix"];
            calibration_multiplier_flow_3_keg = doc["G3_calibration_factor_mix"];
            calibration_multiplier_flow_4_keg = doc["G4_calibration_factor_mix"];

            calibration_multiplier_flow_1_alcohol = doc["G1_calibration_factor_alcohol"];
            calibration_multiplier_flow_2_alcohol = doc["G2_calibration_factor_alcohol"];
            calibration_multiplier_flow_3_alcohol = doc["G3_calibration_factor_alcohol"];
            calibration_multiplier_flow_4_alcohol = doc["G4_calibration_factor_alcohol"];

            // float g3_mix_max_vol;
            // float g3_mix_interval_vol;
            // float g3_alcohol_max_vol;
            // float g3_alcohol_interval_vol;

            // float g4_mix_max_vol;
            // float g4_mix_interval_vol;
            // float g4_alcohol_max_vol;
            // float g4_alcohol_interval_vol;

            g1_mix_max_vol = doc["g1_mix_max_vol"];
            g1_mix_interval_vol = doc["g1_mix_interval_vol"];
            g1_alcohol_max_vol = doc["g1_alcohol_max_vol"];
            g1_alcohol_interval_vol = doc["g1_alcohol_interval_vol"];

            g2_mix_max_vol = doc["g2_mix_max_vol"];
            g2_mix_interval_vol = doc["g2_mix_interval_vol"];
            g2_alcohol_max_vol = doc["g2_alcohol_max_vol"];
            g2_alcohol_interval_vol = doc["g2_alcohol_interval_vol"];

            g3_mix_max_vol = doc["g3_mix_max_vol"];
            g3_mix_interval_vol = doc["g3_mix_interval_vol"];
            g3_alcohol_max_vol = doc["g3_alcohol_max_vol"];
            g3_alcohol_interval_vol = doc["g3_alcohol_interval_vol"];

            g4_mix_max_vol = doc["g4_mix_max_vol"];
            g4_mix_interval_vol = doc["g4_mix_interval_vol"];
            g4_alcohol_max_vol = doc["g3_alcohol_max_vol"];
            g3_alcohol_interval_vol = doc["g4_alcohol_interval_vol"];

            outgoing_json["status"] = "initialized";
            outgoing_json["G1_calibration_factor_mix"] = calibration_multiplier_flow_1_keg;
            outgoing_json["G2_calibration_factor_mix"] = calibration_multiplier_flow_2_keg;
            outgoing_json["G3_calibration_factor_mix"] = calibration_multiplier_flow_3_keg;
            outgoing_json["G4_calibration_factor_mix"] = calibration_multiplier_flow_4_keg;

            outgoing_json["G1_calibration_factor_alcohol"] = calibration_multiplier_flow_1_alcohol;
            outgoing_json["G2_calibration_factor_alcohol"] = calibration_multiplier_flow_2_alcohol;
            outgoing_json["G3_calibration_factor_alcohol"] = calibration_multiplier_flow_3_alcohol;
            outgoing_json["G4_calibration_factor_alcohol"] = calibration_multiplier_flow_4_alcohol;

            // outgoing_json["g1_mix_max_vol"] =  g1_mix_max_vol;
            // outgoing_json["g1_mix_interval_vol"] =  g1_mix_interval_vol;
            // outgoing_json["g1_alcohol_max_vol"] =  g1_alcohol_max_vol;
            // outgoing_json["g1_alcohol_interval_vol"] =  g1_alcohol_max_vol;

            // outgoing_json["g2_mix_max_vol"] =  g2_mix_max_vol;
            // outgoing_json["g2_mix_interval_vol"] =  g2_mix_interval_vol;
            // outgoing_json["g2_alcohol_max_vol"] =  g2_alcohol_max_vol;
            // outgoing_json["g2_alcohol_interval_vol"] =  g2_alcohol_interval_vol;

            // outgoing_json["g3_mix_max_vol"] =  g3_mix_max_vol;
            // outgoing_json["g3_mix_interval_vol"] = g3_mix_interval_vol;
            // outgoing_json["g3_alcohol_max_vol"] =  g3_alcohol_max_vol;
            // outgoing_json["g3_alcohol_interval_vol"] =  g3_alcohol_interval_vol;

            // outgoing_json["g4_mix_max_vol"] =   g4_mix_max_vol;
            // outgoing_json["g4_mix_interval_vol"] =   g4_mix_interval_vol;
            // outgoing_json["g4_alcohol_max_vol"] =   g4_alcohol_max_vol;
            // outgoing_json["g4_alcohol_interval_vol"] =   g4_alcohol_interval_vol;

            serializeJson(outgoing_json, json_string);
            Serial.println(json_string);

            // if(doc["tap_type"] == "keg"){
            //   outgoing_json["status"] = "initialized";
            //   outgoing_json["tap_type"] = "keg";
            //   outgoing_json["calibration_factor_mix"] = doc["calibration_factor_mix"];
            //   serializeJson(outgoing_json, json_string);
            //   Serial.println(json_string);

            //  calibration_multiplier_flow_1_keg = doc["calibration_factor_mix"];
            //  calibration_multiplier_flow_2_keg = doc["calibration_factor_mix"];
            //  calibration_multiplier_flow_3_keg = doc["calibration_factor_mix"];

            // }else if(doc["tap_type"] == "alcohol"){

            //   outgoing_json["status"] = "initialized";
            //   outgoing_json["tap_type"] = "alcohol";
            //   outgoing_json["calibration_factor_alcohol"] = doc["calibration_factor_alcohol"];
            //   serializeJson(outgoing_json, json_string);
            //   Serial.println(json_string);

            //  calibration_multiplier_flow_1_alcohol = doc["calibration_factor_alcohol"];
            //  calibration_multiplier_flow_2_alcohol = doc["calibration_factor_alcohol"];
            //  calibration_multiplier_flow_3_alcohol = doc["calibration_factor_alcohol"];

            // }
        }
        else if (doc["status"] == "start_measurement")
        {

            if (doc["tap_type"] == "keg")
            {

                if (doc["tap_number"] == "1")
                {
                    String json_string;
                    outgoing_json["tap_type"] = "keg!";
                    outgoing_json["tap_number"] = 1;
                    outgoing_json["volume"] = vol;
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                    measure_switcher_1_keg = true;
                }
                else if (doc["tap_number"] == "2")
                {
                    String json_string;
                    outgoing_json["tap_type"] = "keg!";
                    outgoing_json["tap_number"] = 2;
                    outgoing_json["volume"] = vol;
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                    measure_switcher_2_keg = true;
                }
                else if (doc["tap_number"] == "3")
                {

                    String json_string;
                    outgoing_json["tap_type"] = "keg!";
                    outgoing_json["tap_number"] = 3;
                    outgoing_json["volume"] = vol;
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                    measure_switcher_3_keg = true;
                }
                else if (doc["tap_number"] == "4")
                {

                    String json_string;
                    outgoing_json["tap_type"] = "keg!";
                    outgoing_json["tap_number"] = 4;
                    outgoing_json["volume"] = vol;
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                    measure_switcher_4_keg = true;
                }
            }
            else if (doc["tap_type"] == "alcohol")
            {

                if (doc["tap_number"] == "1")
                {
                    String json_string;
                    outgoing_json["tap_type"] = "alcohol!";
                    outgoing_json["tap_number"] = 1;
                    outgoing_json["volume"] = vol;
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                    measure_switcher_1_alcohol = true;
                }
                else if (doc["tap_number"] == "2")
                {
                    String json_string;
                    outgoing_json["tap_type"] = "alcohol!";
                    outgoing_json["tap_number"] = 2;
                    outgoing_json["volume"] = vol;
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                    measure_switcher_2_alcohol = true;
                }
                else if (doc["tap_number"] == "3")
                {

                    String json_string;
                    outgoing_json["tap_type"] = "alcohol!";
                    outgoing_json["tap_number"] = 3;
                    outgoing_json["volume"] = vol;
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                    measure_switcher_3_alcohol = true;
                }

                else if (doc["tap_number"] == "4")
                {

                    String json_string;
                    outgoing_json["tap_type"] = "alcohol!";
                    outgoing_json["tap_number"] = 4;
                    outgoing_json["volume"] = vol;
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                    measure_switcher_4_alcohol = true;
                }
            }

            // outgoing_json["status"] = "start_measurement!";
            // serializeJson(outgoing_json, json_string);
            // Serial.println(json_string);
            // measure_switcher = true;
        }
        else if (doc["status"] == "calibration")
        {
            if (doc["tap_type"] == "keg" && doc["tap_number"] == "1")
            {

                String actual_str = doc["measured_vol"];
                int actual_int = actual_str.toInt();
                if (actual_int > 500)
                {
                    calibration_position_over = true;
                    outgoing_json["calibration_status"] = "over";
                    outgoing_json["calibrating for"] = "keg";
                    outgoing_json["current cal factor"] = calibration_multiplier_flow_1_keg;
                    difference = actual_int - 500;
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                    float actual_volume = actual_int;
                    handleAdjustments_over_cal_TAP_1(vol, actual_volume, calibration_multiplier_flow_1_keg, "keg");
                }
                else if (actual_int < 500)
                {

                    calibration_position_over = true;
                    outgoing_json["calibration_status"] = "under";
                    outgoing_json["calibrating for"] = "keg";
                    outgoing_json["current cal factor"] = calibration_multiplier_flow_1_keg;
                    difference = actual_int - 500;
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                    float actual_volume = actual_int;
                    handleAdjustments_under_cal_TAP_1(vol, actual_volume, calibration_multiplier_flow_1_keg, "keg");
                }
                else if (actual_int >= 500 && actual_int <= 550)
                {
                    // calibration_position_under = true;
                    // difference = actual_int - 500;
                    outgoing_json["calibration_status"] = "okay";
                    outgoing_json["calibrating for"] = "keg";
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);

                    // Calculate the adjustment based on measured and actual volumes
                    // float actual_volume = actual_int; // Replace with your actual volume
                    // handleAdjustments(vol, actual_volume, calibration_multiplier_flow_1_keg);
                }
            }
            else if (doc["tap_type"] == "keg" && doc["tap_number"] == "2")
            {

                String actual_str = doc["measured_vol"];
                int actual_int = actual_str.toInt();
                if (actual_int > 500)
                {
                    calibration_position_over = true;
                    outgoing_json["calibration_status"] = "over";
                    outgoing_json["calibrating for"] = "keg";
                    outgoing_json["current cal factor"] = calibration_multiplier_flow_2_keg;
                    difference = actual_int - 500;
                    serializeJson(outgoing_json, json_string);
                    // Serial.println(json_string);
                    float actual_volume = actual_int;
                    handleAdjustments_over_cal_TAP_2(vol, actual_volume, calibration_multiplier_flow_2_keg, "keg");
                }
                else if (actual_int < 500)
                {

                    calibration_position_over = true;
                    outgoing_json["calibration_status"] = "under";
                    outgoing_json["calibrating for"] = "keg";
                    outgoing_json["current cal factor"] = calibration_multiplier_flow_2_keg;
                    difference = actual_int - 500;
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                    float actual_volume = actual_int;
                    handleAdjustments_under_cal_TAP_2(vol, actual_volume, calibration_multiplier_flow_2_keg, "keg");
                }
            }
            else if (doc["tap_type"] == "keg" && doc["tap_number"] == "3")
            {

                String actual_str = doc["measured_vol"];
                int actual_int = actual_str.toInt();
                if (actual_int > 500)
                {
                    calibration_position_over = true;
                    outgoing_json["calibration_status"] = "over";
                    outgoing_json["calibrating for"] = "keg";
                    outgoing_json["current cal factor"] = calibration_multiplier_flow_3_keg;
                    difference = actual_int - 500;
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                    float actual_volume = actual_int;
                    handleAdjustments_over_cal_TAP_3(vol, actual_volume, calibration_multiplier_flow_3_keg, "keg");
                }
                else if (actual_int < 500)
                {

                    calibration_position_over = true;
                    outgoing_json["calibration_status"] = "under";
                    outgoing_json["calibrating for"] = "keg";
                    outgoing_json["current cal factor"] = calibration_multiplier_flow_3_keg;
                    difference = actual_int - 500;
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                    float actual_volume = actual_int;
                    handleAdjustments_under_cal_TAP_3(vol, actual_volume, calibration_multiplier_flow_3_keg, "keg");
                }
                else if (actual_int >= 500 && actual_int <= 550)
                {
                    // calibration_position_under = true;
                    // difference = actual_int - 500;
                    outgoing_json["calibration_status"] = "okay";
                    outgoing_json["calibrating for"] = "keg";
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                }
            }
            else if (doc["tap_type"] == "keg" && doc["tap_number"] == "4")
            {
                String actual_str = doc["measured_vol"];
                int actual_int = actual_str.toInt();
                if (actual_int > 500)
                {
                    calibration_position_over = true;
                    outgoing_json["calibration_status"] = "over";
                    outgoing_json["calibrating for"] = "keg";
                    outgoing_json["current cal factor"] = calibration_multiplier_flow_4_keg;
                    difference = actual_int - 500;
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                    float actual_volume = actual_int;
                    handleAdjustments_over_cal_TAP_4(vol, actual_volume, calibration_multiplier_flow_4_keg, "keg");
                }
                else if (actual_int < 500)
                {

                    calibration_position_over = true;
                    outgoing_json["calibration_status"] = "under";
                    outgoing_json["calibrating for"] = "keg";
                    outgoing_json["current cal factor"] = calibration_multiplier_flow_4_keg;
                    difference = actual_int - 500;
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                    float actual_volume = actual_int;
                    handleAdjustments_under_cal_TAP_4(vol, actual_volume, calibration_multiplier_flow_4_keg, "keg");
                }
                else if (actual_int >= 500 && actual_int <= 550)
                {
                    // calibration_position_under = true;
                    // difference = actual_int - 500;
                    outgoing_json["calibration_status"] = "okay";
                    outgoing_json["calibrating for"] = "keg";
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                }

                /*********************ALCOHOL************************************/
            }
            else if (doc["tap_type"] == "alcohol" && doc["tap_number"] == "1")
            {
                String actual_str = doc["measured_vol"];
                int actual_int = actual_str.toInt();
                if (actual_int > 500)
                {
                    calibration_position_over = true;
                    outgoing_json["calibration_status"] = "over";
                    outgoing_json["calibrating for"] = "alcohol";
                    outgoing_json["current cal factor"] = calibration_multiplier_flow_1_alcohol;
                    difference = actual_int - 500;
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                    float actual_volume = actual_int;
                    Alcohol_handleAdjustments_over_cal_TAP_1(vol, actual_volume, calibration_multiplier_flow_1_alcohol, "alcohol");
                }
                else if (actual_int < 500)
                {

                    calibration_position_over = true;
                    outgoing_json["calibration_status"] = "under";
                    outgoing_json["calibrating for"] = "alcohol";
                    outgoing_json["current cal factor"] = calibration_multiplier_flow_1_alcohol;
                    difference = actual_int - 500;
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                    float actual_volume = actual_int;
                    Alcohol_handleAdjustments_under_cal_TAP_1(vol, actual_volume, calibration_multiplier_flow_1_alcohol, "alcohol");
                }
                else if (actual_int >= 500 && actual_int <= 550)
                {
                    // calibration_position_under = true;
                    // difference = actual_int - 500;
                    outgoing_json["calibration_status"] = "okay";
                    outgoing_json["calibrating for"] = "alcohol";
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                }
            }
            else if (doc["tap_type"] == "alcohol" && doc["tap_number"] == "3")
            {
                String actual_str = doc["measured_vol"];
                int actual_int = actual_str.toInt();
                if (actual_int > 500)
                {
                    calibration_position_over = true;
                    outgoing_json["calibration_status"] = "over";
                    outgoing_json["calibrating for"] = "alcohol";
                    outgoing_json["current cal factor"] = calibration_multiplier_flow_3_alcohol;
                    difference = actual_int - 500;
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                    float actual_volume = actual_int;
                    Alcohol_handleAdjustments_over_cal_TAP_3(vol, actual_volume, calibration_multiplier_flow_3_alcohol, "alcohol");
                }
                else if (actual_int < 500)
                {

                    calibration_position_over = true;
                    outgoing_json["calibration_status"] = "under";
                    outgoing_json["calibrating for"] = "alcohol";
                    outgoing_json["current cal factor"] = calibration_multiplier_flow_3_alcohol;
                    difference = actual_int - 500;
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                    float actual_volume = actual_int;
                    Alcohol_handleAdjustments_under_cal_TAP_3(vol, actual_volume, calibration_multiplier_flow_3_alcohol, "alcohol");
                }
                else if (actual_int >= 500 && actual_int <= 550)
                {
                    // calibration_position_under = true;
                    // difference = actual_int - 500;
                    outgoing_json["calibration_status"] = "okay";
                    outgoing_json["calibrating for"] = "alcohol";
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                }
            } // end of  tap3 alcohol

            else if (doc["tap_type"] == "alcohol" && doc["tap_number"] == "4")
            {
                String actual_str = doc["measured_vol"];
                int actual_int = actual_str.toInt();
                if (actual_int > 500)
                {
                    calibration_position_over = true;
                    outgoing_json["calibration_status"] = "over";
                    outgoing_json["calibrating for"] = "alcohol";
                    outgoing_json["current cal factor"] = calibration_multiplier_flow_4_alcohol;
                    difference = actual_int - 500;
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                    float actual_volume = actual_int;
                    Alcohol_handleAdjustments_over_cal_TAP_4(vol, actual_volume, calibration_multiplier_flow_4_alcohol, "alcohol");
                }
                else if (actual_int < 500)
                {

                    calibration_position_over = true;
                    outgoing_json["calibration_status"] = "under";
                    outgoing_json["calibrating for"] = "alcohol";
                    outgoing_json["current cal factor"] = calibration_multiplier_flow_4_alcohol;
                    difference = actual_int - 500;
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                    float actual_volume = actual_int;
                    Alcohol_handleAdjustments_under_cal_TAP_4(vol, actual_volume, calibration_multiplier_flow_4_alcohol, "alcohol");
                }
                else if (actual_int >= 500 && actual_int <= 550)
                {
                    // calibration_position_under = true;
                    // difference = actual_int - 500;
                    outgoing_json["calibration_status"] = "okay";
                    outgoing_json["calibrating for"] = "alcohol";
                    serializeJson(outgoing_json, json_string);
                    Serial.println(json_string);
                }
            }
            // else if (doc["tap_type"] == "alcohol")
            // {

            //     String actual_str = doc["measured_vol"];
            //     int actual_int = actual_str.toInt();
            //     if (actual_int > 500)
            //     {
            //         calibration_position_over = true;
            //         outgoing_json["calibration_status"] = "over";
            //         outgoing_json["calibrating for"] = "alcohol";
            //         outgoing_json["current cal factor"] = calibration_multiplier_flow_1_alcohol;
            //         difference = actual_int - 500;
            //         serializeJson(outgoing_json, json_string);
            //         Serial.println(json_string);
            //         float actual_volume = actual_int;
            //         handleAdjustments_over_cal(vol, actual_volume, calibration_multiplier_flow_1_alcohol, "alcohol");
            //     }
            //     else if (actual_int < 500)
            //     {
            //         calibration_position_under = true;
            //         difference = actual_int - 500;
            //         outgoing_json["calibration_status"] = "under";
            //         outgoing_json["calibrating for"] = "alcohol";
            //         serializeJson(outgoing_json, json_string);
            //         Serial.println(json_string);

            //         // Calculate the adjustment based on measured and actual volumes
            //         float actual_volume = actual_int; // Replace with your actual volume
            //         handleAdjustments_under_cal(vol, actual_volume, calibration_multiplier_flow_1_alcohol, "alcohol");
            //     }
            //     else if (actual_int >= 500 && actual_int <= 550)
            //     {
            //         // calibration_position_under = true;
            //         // difference = actual_int - 500;
            //         outgoing_json["calibration_status"] = "okay";
            //         outgoing_json["calibrating for"] = "alcohol";
            //         serializeJson(outgoing_json, json_string);
            //         Serial.println(json_string);
            //     }
            // }
            // else if (doc["tap_number"] == "1" && doc["tap_type"] == "keg")
            // {
            //     //  calibration_position_over = true;
            //     // outgoing_json["calibration_status"] = "over";
            //     outgoing_json["calibrating for"] = "TEST CALIBRATION";
            //     outgoing_json["tap number"] = doc["tap_number"];
            //     outgoing_json["current cal factor"] = calibration_multiplier_flow_1_keg;
            //     // difference = actual_int - 500;
            //     serializeJson(outgoing_json, json_string);
            //     Serial.println(json_string);
            //     // float actual_volume = actual_int;
            //     // handleAdjustments_over_cal(vol, actual_volume, calibration_multiplier_flow_1_keg, "keg");
            // }
        }

        else if (doc["status"] == "main_measure")
        {
            String json_string;

            start_main_measure = true;
            // outgoing_json["calibration_factor_mix"] = doc["calibration_factor_mix"];
            // outgoing_json["calibration_factor_alcohol"] = doc["calibration_factor_alcohol"];
            calibration_multiplier_flow_1_keg = doc["calibration_factor_mix"];
            calibration_multiplier_flow_2_keg = doc["calibration_factor_mix"];
            calibration_multiplier_flow_3_keg = doc["calibration_factor_mix"];

            calibration_multiplier_flow_1_alcohol = doc["calibration_factor_alcohol"];
            calibration_multiplier_flow_2_alcohol = doc["calibration_factor_alcohol"];
            calibration_multiplier_flow_3_alcohol = doc["calibration_factor_alcohol"];

            outgoing_json["status"] = "ready for measurement";
            outgoing_json["calibration_factor_mix"] = calibration_multiplier_flow_1_keg;
            outgoing_json["calibration_factor_alcohol"] = calibration_multiplier_flow_1_alcohol;

            serializeJson(outgoing_json, json_string);
            Serial.println(json_string);
        }
    }

    if (measure_switcher_1_keg == true)
    {
        String json_string;
        digitalWrite(solenoid_relay_1_keg, HIGH);
        vol = 0;
        vol = measure(calibration_multiplier_flow_1_keg, main_pulse_keg_1, calibration_position_over, calibration_position_under, difference);
        outgoing_json["status"] = "tap1: measuring";
        outgoing_json["volume"] = vol;
        outgoing_json["tap_type"] = "mix";
        outgoing_json["current_multiplier"] = calibration_multiplier_flow_1_keg;
        outgoing_json["solenoid_1_status"] = "ON";

        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);
        main_pulse_keg_1++;

        if (vol >= 5)
        {
            main_pulse_keg_1 = 0;
            String json_string;
            digitalWrite(solenoid_relay_1_keg, LOW);
            // digitalWrite(solenoid_relay_2, LOW);
            outgoing_json["status"] = "tap1: measuring stopped";
            outgoing_json["tap_type"] = "mix";
            outgoing_json["current_vol"] = vol;
            outgoing_json["solenoid_1_status"] = "OFF";
            serializeJson(outgoing_json, json_string);
            Serial.println(json_string);
            measure_switcher_1_keg = false;
        }
    }
    else if (measure_switcher_2_keg == true)
    {
        String json_string;
        digitalWrite(solenoid_relay_2_keg, HIGH);
        vol = 0;
        vol = measure(calibration_multiplier_flow_2_keg, main_pulse_keg_2, calibration_position_over, calibration_position_under, difference);
        outgoing_json["status"] = "tap2: measuring";
        outgoing_json["tap_type"] = "mix";
        outgoing_json["volume"] = vol;
        outgoing_json["current_multiplier"] = calibration_multiplier_flow_2_keg;
        outgoing_json["solenoid_2_status"] = "ON";
        outgoing_json["pulse_2"] = pulse_2;
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);
        main_pulse_keg_2++;

        if (vol >= 5)
        {
            main_pulse_keg_2 = 0;
            String json_string;
            digitalWrite(solenoid_relay_2_keg, LOW);
            // digitalWrite(solenoid_relay_2, LOW);
            outgoing_json["status"] = "tap2: measuring stopped";
            outgoing_json["tap_type"] = "mix";
            outgoing_json["current_vol"] = vol;
            outgoing_json["solenoid_2_status"] = "OFF";
            serializeJson(outgoing_json, json_string);
            Serial.println(json_string);
            measure_switcher_2_keg = false;
        }
    }
    else if (measure_switcher_3_keg == true)
    {

        String json_string;
        digitalWrite(solenoid_relay_2_keg, HIGH);
        vol = 0;
        vol = measure(calibration_multiplier_flow_3_keg, main_pulse_keg_3, calibration_position_over, calibration_position_under, difference);
        outgoing_json["status"] = "tap3: measuring";
        outgoing_json["tap_type"] = "mix";
        outgoing_json["volume"] = vol;
        outgoing_json["current_multiplier"] = calibration_multiplier_flow_3_keg;
        outgoing_json["solenoid_3_status"] = "ON";
        outgoing_json["pulse_3"] = main_pulse_keg_3;
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);
        main_pulse_keg_3++;

        if (vol >= 5)
        {
            main_pulse_keg_3 = 0;
            String json_string;
            digitalWrite(solenoid_relay_3_keg, LOW);
            // digitalWrite(solenoid_relay_2, LOW);
            outgoing_json["status"] = "tap3: measuring stopped";
            outgoing_json["tap_type"] = "mix";
            outgoing_json["current_vol"] = vol;
            outgoing_json["solenoid_2_status"] = "OFF";
            serializeJson(outgoing_json, json_string);
            Serial.println(json_string);
            measure_switcher_3_keg = false;
        }
    }

    else if (measure_switcher_4_keg == true)
    {

        String json_string;
        digitalWrite(solenoid_relay_4_keg, HIGH);
        vol = 0;
        vol = measure(calibration_multiplier_flow_4_keg, main_pulse_keg_4, calibration_position_over, calibration_position_under, difference);
        outgoing_json["status"] = "tap4: measuring";
        outgoing_json["tap_type"] = "mix";
        outgoing_json["volume"] = vol;
        outgoing_json["current_multiplier"] = calibration_multiplier_flow_4_keg;
        outgoing_json["solenoid_4_status"] = "ON";
        outgoing_json["pulse_4"] = main_pulse_keg_4;
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);
        main_pulse_keg_4++;

        if (vol >= 5)
        {
            main_pulse_keg_4 = 0;
            String json_string;
            digitalWrite(solenoid_relay_4_keg, LOW);
            // digitalWrite(solenoid_relay_2, LOW);
            outgoing_json["status"] = "tap4: measuring stopped";
            outgoing_json["tap_type"] = "mix";
            outgoing_json["current_vol"] = vol;
            outgoing_json["solenoid_4_status"] = "OFF";
            serializeJson(outgoing_json, json_string);
            Serial.println(json_string);
            measure_switcher_4_keg = false;
        }
    }
    else if (switch_gauge_measurement == true)
    {

        // String json_string;
        //   digitalWrite(solenoid_relay_4_keg, HIGH);
        //   vol = 0;
        //   vol = measure(calibration_multiplier_flow_4_keg, main_pulse_keg_4, calibration_position_over, calibration_position_under, difference);
        //   outgoing_json["status"] = "tap4: measuring";
        //   outgoing_json["tap_type"] = "mix";
        //   outgoing_json["volume"] = vol;
        //   outgoing_json["current_multiplier"] = calibration_multiplier_flow_4_keg;
        //   outgoing_json["solenoid_4_status"] = "ON";
        //   outgoing_json["pulse_4"] = main_pulse_keg_4;
        //   serializeJson(outgoing_json, json_string);
        //   Serial.println(json_string);
        //   main_pulse_keg_4++;

        //   if (vol >= 5)
        //   {
        //       main_pulse_keg_4 = 0;
        //       String json_string;
        //       digitalWrite(solenoid_relay_4_keg, LOW);
        //       // digitalWrite(solenoid_relay_2, LOW);
        //       outgoing_json["status"] = "tap4: measuring stopped";
        //       outgoing_json["tap_type"] = "mix";
        //       outgoing_json["current_vol"] = vol;
        //       outgoing_json["solenoid_4_status"] = "OFF";
        //       serializeJson(outgoing_json, json_string);
        //       Serial.println(json_string);
        //       measure_switcher_4_keg = false;
        //   }
    }

    // else if (measure_switcher_4_keg == true)
    // {

    //    String json_string;
    //     digitalWrite(solenoid_relay_4_keg, HIGH);
    //     vol = 0;
    //     vol = measure(calibration_multiplier_flow_4_keg, main_pulse_keg_4, calibration_position_over, calibration_position_under, difference);
    //     outgoing_json["status"] = "tap4: measuring";
    //     outgoing_json["tap_type"] = "mix";
    //     outgoing_json["volume"] = vol;
    //     outgoing_json["current_multiplier"] = calibration_multiplier_flow_4_keg;
    //     outgoing_json["solenoid_4_status"] = "ON";
    //     outgoing_json["pulse_4"] = main_pulse_keg_4;
    //     serializeJson(outgoing_json, json_string);
    //     Serial.println(json_string);
    //     main_pulse_keg_4++;

    //     if (vol >= 5)
    //     {
    //         main_pulse_keg_4 = 0;
    //         String json_string;
    //         digitalWrite(solenoid_relay_4_keg, LOW);
    //         // digitalWrite(solenoid_relay_2, LOW);
    //         outgoing_json["status"] = "tap4: measuring stopped";
    //         outgoing_json["tap_type"] = "mix";
    //         outgoing_json["current_vol"] = vol;
    //         outgoing_json["solenoid_2_status"] = "OFF";
    //         serializeJson(outgoing_json, json_string);
    //         Serial.println(json_string);
    //         measure_switcher_4_keg = false;
    //     }

    // }

    if (measure_switcher_1_alcohol == true)
    {
        String json_string;
        digitalWrite(solenoid_relay_1_alcohol, HIGH);
        vol = 0;
        vol = measure(calibration_multiplier_flow_1_alcohol, pulse_1, calibration_position_over, calibration_position_under, difference);
        outgoing_json["status"] = "tap1: measuring";
        outgoing_json["tap_type"] = "alcohol";
        outgoing_json["volume"] = vol;
        outgoing_json["current_multiplier"] = calibration_multiplier_flow_1_alcohol;
        outgoing_json["solenoid_1_status"] = "ON";
        outgoing_json["tap_type"] = "alcohol";

        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);
        pulse_1++;

        if (vol >= 5)
        {
            pulse_1 = 0;
            String json_string;
            digitalWrite(solenoid_relay_1_alcohol, LOW);
            // digitalWrite(solenoid_relay_2, LOW);
            outgoing_json["status"] = "tap1: measuring stopped";
            outgoing_json["tap_type"] = "alcohol";
            outgoing_json["current_vol"] = vol;
            outgoing_json["solenoid_1_status"] = "OFF";
            outgoing_json["tap_type"] = "alcohol";
            serializeJson(outgoing_json, json_string);
            Serial.println(json_string);
            measure_switcher_1_alcohol = false;
        }
    }
    else if (measure_switcher_2_alcohol == true)
    {
        String json_string;
        digitalWrite(solenoid_relay_2_alcohol, HIGH);
        vol = 0;
        vol = measure(calibration_multiplier_flow_2_alcohol, pulse_2, calibration_position_over, calibration_position_under, difference);
        outgoing_json["status"] = "tap2: measuring";
        outgoing_json["tap_type"] = "alcohol";
        outgoing_json["volume"] = vol;
        outgoing_json["current_multiplier"] = calibration_multiplier_flow_2_alcohol;
        outgoing_json["solenoid_2_status"] = "ON";
        outgoing_json["pulse_2"] = pulse_2;
        outgoing_json["tap_type"] = "alcohol";
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);
        pulse_2++;

        if (vol >= 5)
        {
            pulse_2 = 0;
            String json_string;
            digitalWrite(solenoid_relay_2_alcohol, LOW);
            // digitalWrite(solenoid_relay_2, LOW);
            outgoing_json["status"] = "tap2: measuring stopped";
            outgoing_json["tap_type"] = "alcohol";
            outgoing_json["current_vol"] = vol;
            outgoing_json["solenoid_2_status"] = "OFF";
            outgoing_json["tap_type"] = "alcohol";
            serializeJson(outgoing_json, json_string);
            Serial.println(json_string);
            measure_switcher_2_alcohol = false;
        }
    }
    else if (measure_switcher_3_alcohol == true)
    {
        String json_string;
        digitalWrite(solenoid_relay_3_alcohol, HIGH);
        vol = 0;
        vol = measure(calibration_multiplier_flow_3_alcohol, pulse_3, calibration_position_over, calibration_position_under, difference);
        outgoing_json["status"] = "tap3: measuring";
        outgoing_json["tap_type"] = "alcohol";
        outgoing_json["volume"] = vol;
        outgoing_json["current_multiplier"] = calibration_multiplier_flow_3_alcohol;
        outgoing_json["solenoid_3_status"] = "ON";
        outgoing_json["pulse_3"] = pulse_3;
        outgoing_json["tap_type"] = "alcohol";
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);
        pulse_3++;

        if (vol >= 5)
        {
            pulse_3 = 0;
            String json_string;
            digitalWrite(solenoid_relay_3_alcohol, LOW);
            // digitalWrite(solenoid_relay_2, LOW);
            outgoing_json["status"] = "tap3: measuring stopped...";
            outgoing_json["tap_type"] = "alcohol";
            outgoing_json["current_vol"] = vol;
            outgoing_json["solenoid_3_status"] = "OFF";
            outgoing_json["tap_type"] = "alcohol";
            serializeJson(outgoing_json, json_string);
            Serial.println(json_string);
            measure_switcher_3_alcohol = false;
        }
    }

    else if (measure_switcher_4_alcohol == true)
    {
        String json_string;
        digitalWrite(solenoid_relay_4_alcohol, HIGH);
        vol = 0;
        vol = measure(calibration_multiplier_flow_4_alcohol, pulse_4, calibration_position_over, calibration_position_under, difference);
        outgoing_json["status"] = "tap4: measuring";
        outgoing_json["tap_type"] = "alcohol";
        outgoing_json["volume"] = vol;
        outgoing_json["current_multiplier"] = calibration_multiplier_flow_4_alcohol;
        outgoing_json["solenoid_4_status"] = "ON";
        outgoing_json["pulse_4"] = pulse_4;
        outgoing_json["tap_type"] = "alcohol";
        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);
        pulse_4++;

        if (vol >= 5)
        {
            pulse_4 = 0;
            String json_string;
            digitalWrite(solenoid_relay_4_alcohol, LOW);
            // digitalWrite(solenoid_relay_2, LOW);
            outgoing_json["status"] = "tap3: measuring stopped...";
            outgoing_json["tap_type"] = "alcohol";
            outgoing_json["current_vol"] = vol;
            outgoing_json["solenoid_3_status"] = "OFF";
            outgoing_json["tap_type"] = "alcohol";
            serializeJson(outgoing_json, json_string);
            Serial.println(json_string);
            measure_switcher_4_alcohol = false;
        }
    }

    else if (start_main_measure == true)
    {
        String json_string;
        digitalWrite(solenoid_relay_1_keg, HIGH);
        digitalWrite(solenoid_relay_1_alcohol, HIGH);

        digitalWrite(solenoid_relay_2_keg, HIGH);
        digitalWrite(solenoid_relay_2_alcohol, HIGH);

        digitalWrite(solenoid_relay_3_keg, HIGH);
        digitalWrite(solenoid_relay_3_alcohol, HIGH);

        vol_mix_1 = 0;
        vol_alcohol_1 = 0;

        vol_mix_2 = 0;
        vol_alcohol_2 = 0;

        vol_mix_3 = 0;
        vol_alcohol_3 = 0;

        vol_mix_1 = measure(calibration_multiplier_flow_1_keg, main_pulse_keg_1, calibration_position_over, calibration_position_under, difference);
        vol_alcohol_1 = measure(calibration_multiplier_flow_1_alcohol, main_pulse_alcohol_1, calibration_position_over, calibration_position_under, difference);

        vol_mix_2 = measure(calibration_multiplier_flow_2_keg, main_pulse_keg_2, calibration_position_over, calibration_position_under, difference);
        vol_alcohol_2 = measure(calibration_multiplier_flow_2_alcohol, main_pulse_alcohol_2, calibration_position_over, calibration_position_under, difference);

        vol_mix_3 = measure(calibration_multiplier_flow_3_keg, main_pulse_keg_3, calibration_position_over, calibration_position_under, difference);
        vol_alcohol_3 = measure(calibration_multiplier_flow_3_alcohol, main_pulse_alcohol_3, calibration_position_over, calibration_position_under, difference);

        vol_mix_4 = measure(calibration_multiplier_flow_3_keg, main_pulse_keg_4, calibration_position_over, calibration_position_under, difference);
        vol_alcohol_4 = measure(calibration_multiplier_flow_3_alcohol, main_pulse_alcohol_4, calibration_position_over, calibration_position_under, difference);

        // outgoing_json["status"] = "main measuring";
        // outgoing_json["volume_mix_1"] = vol_mix_1;
        // outgoing_json["vol_alcohol_1"] = vol_alcohol_1;

        // outgoing_json["vol_mix_2"] = vol_mix_2;
        // outgoing_json["vol_alcohol_2"] = vol_alcohol_2;

        // outgoing_json["vol_mix_3"] = vol_mix_3;
        // outgoing_json["vol_alcohol_3"] = vol_alcohol_3;

        outgoing_json["status"] = "main measuring";
        outgoing_json["volume_mix_1"] = 2;
        outgoing_json["vol_alcohol_1"] = 2;

        outgoing_json["vol_mix_2"] = 2;
        outgoing_json["vol_alcohol_2"] = 2;

        outgoing_json["vol_mix_3"] = 2;
        outgoing_json["vol_alcohol_3"] = 2;

        // outgoing_json["tap_type"] ="mix";
        // outgoing_json["current_multiplier"] = calibration_multiplier_flow_1_keg;
        // outgoing_json["solenoid_1_status"] = "ON";

        serializeJson(outgoing_json, json_string);
        Serial.println(json_string);

        main_pulse++;
    }

    delay(500);
}

void increase_1()
{
    pulse_1++;
}

void increase_2()
{
    pulse_2++;
}

void increase_3()
{
    pulse_3++;
}

void increase_4()
{
    pulse_4++;
}

void gauge_measurement_G1(long pulse_mix, long pulse_alcohol float calibration_mix, float calibration_alcohol, float interval_vol_mix, float interval_vol_alcohol, float max_vol_mix, float max_vol_alcohol)
{
}

void main_increase_keg_1()
{
    main_pulse_keg_1;
}

void main_increase_alcohol_1()
{
    main_pulse_alcohol_1;
}

void main_increase_keg_2()
{
    main_pulse_keg_2;
}

void main_increase_alcohol_2()
{
    main_pulse_alcohol_2;
}

void main_increase_keg_3()
{
    main_pulse_keg_3;
}

void main_increase_alcohol_3()
{
    main_pulse_alcohol_3;
}

void main_increase_keg_4()
{
    main_pulse_keg_4;
}

void main_increase_alcohol_4()
{
    main_pulse_alcohol_4;
}

float measure(float calibration_multiplier, int pulse, bool calibration_over, bool calibration_under, float diff)
{
    float volume;
    return volume = calibration_multiplier * pulse;
}

void ajust_calibration(float measured_vol, float actual_vol, float calibration_multiplier)
{
    if (measured_vol > actual_vol)
    {
        // Perform adjustments if needed
    }
}
