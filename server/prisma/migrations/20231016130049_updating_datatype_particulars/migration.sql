-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_measurement_particulars" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "liquid_type" TEXT NOT NULL,
    "max_vol" TEXT NOT NULL DEFAULT '0',
    "group" TEXT NOT NULL DEFAULT '0',
    "interval_vol" TEXT NOT NULL DEFAULT '0'
);
INSERT INTO "new_measurement_particulars" ("group", "id", "interval_vol", "liquid_type", "max_vol") SELECT "group", "id", "interval_vol", "liquid_type", "max_vol" FROM "measurement_particulars";
DROP TABLE "measurement_particulars";
ALTER TABLE "new_measurement_particulars" RENAME TO "measurement_particulars";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
