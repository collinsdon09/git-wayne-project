/*
  Warnings:

  - You are about to drop the `measurement_particulars` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
PRAGMA foreign_keys=off;
DROP TABLE "measurement_particulars";
PRAGMA foreign_keys=on;

-- CreateTable
CREATE TABLE "measurement_particulars_mix" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "liquid_type" TEXT NOT NULL,
    "max_vol" TEXT NOT NULL DEFAULT '0',
    "group" TEXT NOT NULL DEFAULT '0',
    "interval_vol" TEXT NOT NULL DEFAULT '0'
);

-- CreateTable
CREATE TABLE "measurement_particulars_alcohol" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "liquid_type" TEXT NOT NULL,
    "max_vol" TEXT NOT NULL DEFAULT '0',
    "group" TEXT NOT NULL DEFAULT '0',
    "interval_vol" TEXT NOT NULL DEFAULT '0'
);
