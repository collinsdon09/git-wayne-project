/*
  Warnings:

  - Added the required column `tap_type` to the `calibration` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_calibration" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "calibration_factor" REAL NOT NULL DEFAULT 0,
    "tap_type" TEXT NOT NULL,
    "createDate" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updateDate" DATETIME NOT NULL
);
INSERT INTO "new_calibration" ("calibration_factor", "createDate", "id", "updateDate") SELECT "calibration_factor", "createDate", "id", "updateDate" FROM "calibration";
DROP TABLE "calibration";
ALTER TABLE "new_calibration" RENAME TO "calibration";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
