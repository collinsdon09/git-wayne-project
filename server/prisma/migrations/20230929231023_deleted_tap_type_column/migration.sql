/*
  Warnings:

  - You are about to drop the column `tap_type` on the `calibration` table. All the data in the column will be lost.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_calibration" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "calibration_factor_alcohol" REAL NOT NULL DEFAULT 0,
    "calibration_factor_mix" REAL NOT NULL DEFAULT 0,
    "error" INTEGER NOT NULL DEFAULT 0,
    "createDate" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updateDate" DATETIME NOT NULL
);
INSERT INTO "new_calibration" ("calibration_factor_alcohol", "calibration_factor_mix", "createDate", "error", "id", "updateDate") SELECT "calibration_factor_alcohol", "calibration_factor_mix", "createDate", "error", "id", "updateDate" FROM "calibration";
DROP TABLE "calibration";
ALTER TABLE "new_calibration" RENAME TO "calibration";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
