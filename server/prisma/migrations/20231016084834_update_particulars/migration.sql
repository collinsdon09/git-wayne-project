/*
  Warnings:

  - You are about to drop the `volume_particulars` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
PRAGMA foreign_keys=off;
DROP TABLE "volume_particulars";
PRAGMA foreign_keys=on;

-- CreateTable
CREATE TABLE "measurement_particulars" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "liquid_type" TEXT NOT NULL,
    "max_vol" INTEGER NOT NULL DEFAULT 0,
    "interval_vol" INTEGER NOT NULL DEFAULT 0
);
