/*
  Warnings:

  - You are about to alter the column `interval_vol` on the `G1_measurement_particulars_mix` table. The data in that column could be lost. The data in that column will be cast from `String` to `Float`.
  - You are about to alter the column `max_vol` on the `G1_measurement_particulars_mix` table. The data in that column could be lost. The data in that column will be cast from `String` to `Float`.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_G1_measurement_particulars_mix" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "max_vol" REAL NOT NULL DEFAULT 0,
    "interval_vol" REAL NOT NULL DEFAULT 0
);
INSERT INTO "new_G1_measurement_particulars_mix" ("id", "interval_vol", "max_vol") SELECT "id", "interval_vol", "max_vol" FROM "G1_measurement_particulars_mix";
DROP TABLE "G1_measurement_particulars_mix";
ALTER TABLE "new_G1_measurement_particulars_mix" RENAME TO "G1_measurement_particulars_mix";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
