/*
  Warnings:

  - You are about to drop the `G1_calibration_tap1` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `G1_calibration_tap2` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `G2_calibration_tap1` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `G3_calibration_tap2` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `G4_calibration_tap1` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `G4_calibration_tap2` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
PRAGMA foreign_keys=off;
DROP TABLE "G1_calibration_tap1";
PRAGMA foreign_keys=on;

-- DropTable
PRAGMA foreign_keys=off;
DROP TABLE "G1_calibration_tap2";
PRAGMA foreign_keys=on;

-- DropTable
PRAGMA foreign_keys=off;
DROP TABLE "G2_calibration_tap1";
PRAGMA foreign_keys=on;

-- DropTable
PRAGMA foreign_keys=off;
DROP TABLE "G3_calibration_tap2";
PRAGMA foreign_keys=on;

-- DropTable
PRAGMA foreign_keys=off;
DROP TABLE "G4_calibration_tap1";
PRAGMA foreign_keys=on;

-- DropTable
PRAGMA foreign_keys=off;
DROP TABLE "G4_calibration_tap2";
PRAGMA foreign_keys=on;

-- CreateTable
CREATE TABLE "G1_calibration" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "calibration_factor_alcohol" REAL NOT NULL DEFAULT 0,
    "calibration_factor_mix" REAL NOT NULL DEFAULT 0,
    "error" INTEGER NOT NULL DEFAULT 0,
    "createDate" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updateDate" DATETIME NOT NULL,
    "active_connection" BOOLEAN NOT NULL DEFAULT false
);

-- CreateTable
CREATE TABLE "G2_calibration" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "calibration_factor_alcohol" REAL NOT NULL DEFAULT 0,
    "calibration_factor_mix" REAL NOT NULL DEFAULT 0,
    "error" INTEGER NOT NULL DEFAULT 0,
    "createDate" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updateDate" DATETIME NOT NULL,
    "active_connection" BOOLEAN NOT NULL DEFAULT false
);

-- CreateTable
CREATE TABLE "G3_calibration" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "calibration_factor_alcohol" REAL NOT NULL DEFAULT 0,
    "calibration_factor_mix" REAL NOT NULL DEFAULT 0,
    "error" INTEGER NOT NULL DEFAULT 0,
    "createDate" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updateDate" DATETIME NOT NULL,
    "active_connection" BOOLEAN NOT NULL DEFAULT false
);

-- CreateTable
CREATE TABLE "G4_calibration" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "calibration_factor_alcohol" REAL NOT NULL DEFAULT 0,
    "calibration_factor_mix" REAL NOT NULL DEFAULT 0,
    "error" INTEGER NOT NULL DEFAULT 0,
    "createDate" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updateDate" DATETIME NOT NULL,
    "active_connection" BOOLEAN NOT NULL DEFAULT false
);
