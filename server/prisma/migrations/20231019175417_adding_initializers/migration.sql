/*
  Warnings:

  - You are about to drop the column `group` on the `G1_measurement_particulars_mix` table. All the data in the column will be lost.
  - You are about to drop the column `liquid_type` on the `G1_measurement_particulars_mix` table. All the data in the column will be lost.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_G1_measurement_particulars_mix" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "max_vol" TEXT NOT NULL DEFAULT '0',
    "interval_vol" TEXT NOT NULL DEFAULT '0'
);
INSERT INTO "new_G1_measurement_particulars_mix" ("id", "interval_vol", "max_vol") SELECT "id", "interval_vol", "max_vol" FROM "G1_measurement_particulars_mix";
DROP TABLE "G1_measurement_particulars_mix";
ALTER TABLE "new_G1_measurement_particulars_mix" RENAME TO "G1_measurement_particulars_mix";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
