-- CreateTable
CREATE TABLE "volume_particulars" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "liquid_type" TEXT NOT NULL,
    "max_vol" INTEGER NOT NULL DEFAULT 0,
    "interval_vol" INTEGER NOT NULL DEFAULT 0
);
