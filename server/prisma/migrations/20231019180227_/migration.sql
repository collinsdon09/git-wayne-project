/*
  Warnings:

  - You are about to drop the column `group` on the `G2_measurement_particulars_mix` table. All the data in the column will be lost.
  - You are about to drop the column `liquid_type` on the `G2_measurement_particulars_mix` table. All the data in the column will be lost.
  - You are about to alter the column `interval_vol` on the `G2_measurement_particulars_mix` table. The data in that column could be lost. The data in that column will be cast from `String` to `Float`.
  - You are about to alter the column `max_vol` on the `G2_measurement_particulars_mix` table. The data in that column could be lost. The data in that column will be cast from `String` to `Float`.
  - You are about to drop the column `group` on the `G3_measurement_particulars_mix` table. All the data in the column will be lost.
  - You are about to drop the column `liquid_type` on the `G3_measurement_particulars_mix` table. All the data in the column will be lost.
  - You are about to alter the column `interval_vol` on the `G3_measurement_particulars_mix` table. The data in that column could be lost. The data in that column will be cast from `String` to `Float`.
  - You are about to alter the column `max_vol` on the `G3_measurement_particulars_mix` table. The data in that column could be lost. The data in that column will be cast from `String` to `Float`.
  - You are about to drop the column `group` on the `G3_measurement_particulars_alcohol` table. All the data in the column will be lost.
  - You are about to drop the column `liquid_type` on the `G3_measurement_particulars_alcohol` table. All the data in the column will be lost.
  - You are about to alter the column `interval_vol` on the `G3_measurement_particulars_alcohol` table. The data in that column could be lost. The data in that column will be cast from `String` to `Float`.
  - You are about to alter the column `max_vol` on the `G3_measurement_particulars_alcohol` table. The data in that column could be lost. The data in that column will be cast from `String` to `Float`.
  - You are about to drop the column `group` on the `G1_measurement_particulars_alcohol` table. All the data in the column will be lost.
  - You are about to drop the column `liquid_type` on the `G1_measurement_particulars_alcohol` table. All the data in the column will be lost.
  - You are about to alter the column `interval_vol` on the `G1_measurement_particulars_alcohol` table. The data in that column could be lost. The data in that column will be cast from `String` to `Float`.
  - You are about to alter the column `max_vol` on the `G1_measurement_particulars_alcohol` table. The data in that column could be lost. The data in that column will be cast from `String` to `Float`.
  - You are about to drop the column `group` on the `G4_measurement_particulars_alcohol` table. All the data in the column will be lost.
  - You are about to drop the column `liquid_type` on the `G4_measurement_particulars_alcohol` table. All the data in the column will be lost.
  - You are about to alter the column `interval_vol` on the `G4_measurement_particulars_alcohol` table. The data in that column could be lost. The data in that column will be cast from `String` to `Float`.
  - You are about to alter the column `max_vol` on the `G4_measurement_particulars_alcohol` table. The data in that column could be lost. The data in that column will be cast from `String` to `Float`.
  - You are about to drop the column `group` on the `G2_measurement_particulars_alcohol` table. All the data in the column will be lost.
  - You are about to drop the column `liquid_type` on the `G2_measurement_particulars_alcohol` table. All the data in the column will be lost.
  - You are about to alter the column `interval_vol` on the `G2_measurement_particulars_alcohol` table. The data in that column could be lost. The data in that column will be cast from `String` to `Float`.
  - You are about to alter the column `max_vol` on the `G2_measurement_particulars_alcohol` table. The data in that column could be lost. The data in that column will be cast from `String` to `Float`.
  - You are about to drop the column `group` on the `G4_measurement_particulars_mix` table. All the data in the column will be lost.
  - You are about to drop the column `liquid_type` on the `G4_measurement_particulars_mix` table. All the data in the column will be lost.
  - You are about to alter the column `interval_vol` on the `G4_measurement_particulars_mix` table. The data in that column could be lost. The data in that column will be cast from `String` to `Float`.
  - You are about to alter the column `max_vol` on the `G4_measurement_particulars_mix` table. The data in that column could be lost. The data in that column will be cast from `String` to `Float`.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_G2_measurement_particulars_mix" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "max_vol" REAL NOT NULL DEFAULT 0,
    "interval_vol" REAL NOT NULL DEFAULT 0
);
INSERT INTO "new_G2_measurement_particulars_mix" ("id", "interval_vol", "max_vol") SELECT "id", "interval_vol", "max_vol" FROM "G2_measurement_particulars_mix";
DROP TABLE "G2_measurement_particulars_mix";
ALTER TABLE "new_G2_measurement_particulars_mix" RENAME TO "G2_measurement_particulars_mix";
CREATE TABLE "new_G3_measurement_particulars_mix" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "max_vol" REAL NOT NULL DEFAULT 0,
    "interval_vol" REAL NOT NULL DEFAULT 0
);
INSERT INTO "new_G3_measurement_particulars_mix" ("id", "interval_vol", "max_vol") SELECT "id", "interval_vol", "max_vol" FROM "G3_measurement_particulars_mix";
DROP TABLE "G3_measurement_particulars_mix";
ALTER TABLE "new_G3_measurement_particulars_mix" RENAME TO "G3_measurement_particulars_mix";
CREATE TABLE "new_G3_measurement_particulars_alcohol" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "max_vol" REAL NOT NULL DEFAULT 0,
    "interval_vol" REAL NOT NULL DEFAULT 0
);
INSERT INTO "new_G3_measurement_particulars_alcohol" ("id", "interval_vol", "max_vol") SELECT "id", "interval_vol", "max_vol" FROM "G3_measurement_particulars_alcohol";
DROP TABLE "G3_measurement_particulars_alcohol";
ALTER TABLE "new_G3_measurement_particulars_alcohol" RENAME TO "G3_measurement_particulars_alcohol";
CREATE TABLE "new_G1_measurement_particulars_alcohol" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "max_vol" REAL NOT NULL DEFAULT 0,
    "interval_vol" REAL NOT NULL DEFAULT 0
);
INSERT INTO "new_G1_measurement_particulars_alcohol" ("id", "interval_vol", "max_vol") SELECT "id", "interval_vol", "max_vol" FROM "G1_measurement_particulars_alcohol";
DROP TABLE "G1_measurement_particulars_alcohol";
ALTER TABLE "new_G1_measurement_particulars_alcohol" RENAME TO "G1_measurement_particulars_alcohol";
CREATE TABLE "new_G4_measurement_particulars_alcohol" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "max_vol" REAL NOT NULL DEFAULT 0,
    "interval_vol" REAL NOT NULL DEFAULT 0
);
INSERT INTO "new_G4_measurement_particulars_alcohol" ("id", "interval_vol", "max_vol") SELECT "id", "interval_vol", "max_vol" FROM "G4_measurement_particulars_alcohol";
DROP TABLE "G4_measurement_particulars_alcohol";
ALTER TABLE "new_G4_measurement_particulars_alcohol" RENAME TO "G4_measurement_particulars_alcohol";
CREATE TABLE "new_G2_measurement_particulars_alcohol" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "max_vol" REAL NOT NULL DEFAULT 0,
    "interval_vol" REAL NOT NULL DEFAULT 0
);
INSERT INTO "new_G2_measurement_particulars_alcohol" ("id", "interval_vol", "max_vol") SELECT "id", "interval_vol", "max_vol" FROM "G2_measurement_particulars_alcohol";
DROP TABLE "G2_measurement_particulars_alcohol";
ALTER TABLE "new_G2_measurement_particulars_alcohol" RENAME TO "G2_measurement_particulars_alcohol";
CREATE TABLE "new_G4_measurement_particulars_mix" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "max_vol" REAL NOT NULL DEFAULT 0,
    "interval_vol" REAL NOT NULL DEFAULT 0
);
INSERT INTO "new_G4_measurement_particulars_mix" ("id", "interval_vol", "max_vol") SELECT "id", "interval_vol", "max_vol" FROM "G4_measurement_particulars_mix";
DROP TABLE "G4_measurement_particulars_mix";
ALTER TABLE "new_G4_measurement_particulars_mix" RENAME TO "G4_measurement_particulars_mix";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
