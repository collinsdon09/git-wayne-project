-- CreateTable
CREATE TABLE "calibration" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "calibration_factor" REAL NOT NULL DEFAULT 0,
    "createDate" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updateDate" DATETIME NOT NULL
);
