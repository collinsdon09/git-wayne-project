-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_calibration" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "calibration_factor" REAL NOT NULL DEFAULT 0,
    "calibration_factor_alcohol" REAL NOT NULL DEFAULT 0,
    "calibration_factor_mix" REAL NOT NULL DEFAULT 0,
    "tap_type" TEXT NOT NULL,
    "error" INTEGER NOT NULL DEFAULT 0,
    "createDate" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updateDate" DATETIME NOT NULL
);
INSERT INTO "new_calibration" ("calibration_factor", "createDate", "error", "id", "tap_type", "updateDate") SELECT "calibration_factor", "createDate", "error", "id", "tap_type", "updateDate" FROM "calibration";
DROP TABLE "calibration";
ALTER TABLE "new_calibration" RENAME TO "calibration";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
