import React from "react";
import "../styles/Overlay.css"; // You can create a CSS file to style the overlay

const Overlay = ({ show }) => {
  if (!show) return null;

  return (
    <div className="overlay">
      <div className="overlay-content">
        {/* Place your LoaderComp or any other content here */}
      </div>
    </div>
  );
};

export default Overlay;
