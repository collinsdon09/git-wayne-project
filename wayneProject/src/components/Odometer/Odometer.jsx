
import React, { Component } from "react";
import DigitRoll from "digit-roll-react";
import "../styles/odometer-style.css"

class OdoComponent extends Component {
  constructor(props) {
    super(props);
    this.state = { num: 0 };
  }

  componentDidMount() {
    this.startCounting();
  }

  startCounting() {
    const incrementInterval = 1000; // Time in milliseconds between each increment
    const targetNum = 1000;

    const updateNum = () => {
      if (this.state.num < targetNum) {
        this.setState((prevState) => ({
          num: prevState.num + 1,
        }));
        setTimeout(updateNum, incrementInterval);
      }
    };

    setTimeout(updateNum, incrementInterval);
  }

  render() {
    return <DigitRoll num={this.state.num} length={6} divider="" />;
  }
}

export default OdoComponent;
