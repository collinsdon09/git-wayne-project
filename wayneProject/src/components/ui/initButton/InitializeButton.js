import { Button, Center, ButtonGroup } from "@chakra-ui/react";

const InitializeButtonComponent = ({initFunction, init_status}) => {
  const handleClick = () => {
    // Call the function from the parent component
    initFunction();
  };
  return (
    <>
      <Center h="100px" color="white">
        {/* This is the Center */}

        <Button
          //colorScheme="teal"
          colorScheme={init_status ? "blue" : "red"}
          // variant="outline"
          onClick={handleClick}
        >
          {/* connect to controller{" "} */}
          {init_status
            ? "Connected"
            : "Connect to Microcontroller!"}
        </Button>
      </Center>
    </>
  );
};

export default InitializeButtonComponent;
