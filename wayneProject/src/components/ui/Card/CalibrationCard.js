import { Card, CardHeader, CardBody, CardFooter } from "@chakra-ui/react";

const CardComponent = () => {
  return (
    <>
      <Card
        // bgGradient="linear(to-l, #9AE6B4, #38B2AC)"
        bgColor={"#93c572"}
        className="drop-shadow-2xl"
      >
        <CardHeader>
          <Heading mb="10px" size="lg" color={"gray.700"}>
            {" "}
            Mix
          </Heading>

          <Alert status="warning">
            <AlertIcon />
            {/* <AlertTitle>Calibration</AlertTitle> */}
            <AlertDescription color={"gray.700"} className="font-bold">
              Measure 500ml
            </AlertDescription>
          </Alert>

          <Formik
            // initialValues={{ firstName: "" }}
            initialValues={{
              tap_number: "",

              // Add this field with an initial value
            }}
            onSubmit={handleStartMeasurement_Keg}
            validationSchema={validationSchema2}
          >
            {(formikProps) => (
              <>
                <Form>
                  <Box mt={6} w="100%">
                    <Text
                      bgGradient="linear(to-r, teal.700, green.700)"
                      bgClip="text"
                      fontSize="l"
                      fontWeight="extrabold"
                    >
                      {" "}
                      Tap Number:
                    </Text>
                    <Field name="tap_number">
                      {({ field, form }) => (
                        <FormControl
                          isInvalid={
                            form.errors.tap_number && form.touched.tap_number
                          }
                        >
                          <Select
                            {...field}
                            placeholder="-- Select Tap --"
                            onChange={(e) =>
                              form.setFieldValue("tap_number", e.target.value)
                            }
                          >
                            {/* Add options for the Select here */}
                            <option value="1">1</option>
                            <option value="2">2 </option>
                            <option value="3">3</option>
                          </Select>
                          <FormErrorMessage>
                            {form.errors.tap_number}
                          </FormErrorMessage>
                        </FormControl>
                      )}
                    </Field>
                  </Box>

                  <Button
                    mt={6}
                    colorScheme="orange"
                    //isLoading={formikProps.isSubmitting}
                    type="submit"
                    disabled={formikProps.isValid} // Disable the button if the form is not valid
                  >
                    Start Measurement
                  </Button>
                </Form>
              </>
            )}
          </Formik>
        </CardHeader>
        <CardBody>
          <Formik
            // initialValues={{ firstName: "" }}
            initialValues={{
              volume: "",

              // Add this field with an initial value
            }}
            onSubmit={handleSubmit_Keg}
            validationSchema={validationSchema}
          >
            {(formikProps) => (
              <>
                <Form>
                  <Box>
                    <Field name="volume">
                      {({ field, form }) => (
                        <FormControl
                          isInvalid={form.errors.volume && form.touched.volume}
                        >
                          <FormLabel htmlFor="volume" colorScheme="brand">
                            How much did you measure?
                          </FormLabel>
                          <Input
                            {...field}
                            id="volume"
                            placeholder="Volume"
                            _placeholder={{ opacity: 1, color: "gray.600" }}
                            // value={formfirstName}
                          />{" "}
                          <FormErrorMessage>
                            {form.errors.volume}
                          </FormErrorMessage>
                        </FormControl>
                      )}
                    </Field>
                  </Box>

                  <HStack>
                    <Button
                      mt={6}
                      colorScheme="red"
                      isLoading={formikProps.isSubmitting}
                      type="submit"
                      disabled={formikProps.isValid} // Disable the button if the form is not valid
                    >
                      Submit
                    </Button>
                  </HStack>
                </Form>
              </>
            )}
          </Formik>
        </CardBody>
        {/* <CardFooter>
            <Button>View here</Button>
          </CardFooter> */}
      </Card>
    </>
  );
};

export default CardComponent;
