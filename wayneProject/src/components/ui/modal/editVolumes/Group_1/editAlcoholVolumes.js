import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  Box,
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
  HStack,
} from "@chakra-ui/react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import useSocket from "../../../../../hooks/sendData";
import { useEffect } from "react";

const EditVolumesModal_alcohol = ({ activate, deactivate }) => {
  const { socket, sendMessage, subscribeToEvent } = useSocket(
    "http://localhost:4001"
  );
  function handleSubmitData(values, actions) {
    console.log("submitting edit data", values);
    sendMessage("form_data_alcohol", {
      liquid_type: "alcohol",
      group: "1",
      max_vol: values.volume,
      interval_vol: values.interval_volume,
    });
  }

  const validationSchema = Yup.object().shape({
    volume: Yup.number().required("You need to provide a number.."),
    interval_volume: Yup.number().required("You need to provide a number.."),
  });



  useEffect(() => {
    console.log("use effect..");

    if (socket && subscribeToEvent) {
      // subscribeToEvent("initialized", (data) => {
      //   // Handle incoming data here
      //   console.log("Received data:", data.message);
      //   // setInitializationComplete(data.message);
      // });

      subscribeToEvent("alcohol_data", (data) => {
        // console.log("receiving mix data", data);
        // Handle incoming data here
        // console.log("message:", data.message);
        // setLoader(true);
        //setInitializationComplete(data.message);
      });

      subscribeToEvent("measurement_stopped", (data) => {
        // Handle incoming data here
        console.log("message:", data.message);
        // setLoader(false);
        // setModalOpen(true);
        //setInitializationComplete(data.message);
      });

      console.log("use effect end..");

      // Don't forget to unsubscribe when the component unmounts to avoid memory leaks
      return () => {
        socket.off("initialized");
      };
    }
  }, [socket, subscribeToEvent]);




  return (
    <>
      <Modal isOpen={activate} onClose={deactivate}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Edit Volumes for Alcohol</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Formik
              // initialValues={{ firstName: "" }}
              initialValues={{
                volume: "",

                // Add this field with an initial value
              }}
              onSubmit={handleSubmitData}
              validationSchema={validationSchema}
            >
              {(formikProps) => (
                <>
                  <Form>
                    <Box>
                      <Field name="volume">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.volume && form.touched.volume
                            }
                          >
                            <FormLabel htmlFor="volume" colorScheme="brand">
                              Maximum Volume: [Alcohol]
                            </FormLabel>
                            <Input
                              {...field}
                              id="interval_volume"
                              placeholder="interval volume"
                              _placeholder={{ opacity: 1, color: "gray.600" }}
                              // value={formfirstName}
                            />{" "}
                            <FormErrorMessage>
                              {form.errors.volume}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>

                      <Field name="interval_volume">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.volume && form.touched.volume
                            }
                          >
                            <FormLabel htmlFor="volume" colorScheme="brand">
                              Interval Volume: [Alcohol]
                            </FormLabel>
                            <Input
                              {...field}
                              id="volume"
                              placeholder="Volume"
                              _placeholder={{ opacity: 1, color: "gray.600" }}
                              // value={formfirstName}
                            />{" "}
                            <FormErrorMessage>
                              {form.errors.volume}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>
                    </Box>

                    <HStack>
                      <Button
                        mt={6}
                        colorScheme="red"
                        isLoading={formikProps.isSubmitting}
                        type="submit"
                        disabled={formikProps.isValid} // Disable the button if the form is not valid
                      >
                        Submit
                      </Button>
                    </HStack>
                  </Form>
                </>
              )}
            </Formik>
          </ModalBody>

          {/* <ModalFooter>
              <Button colorScheme="blue" mr={3} onClick={deactivate}>
                Close
              </Button>
              <Button variant="ghost">Secondary Action</Button>
            </ModalFooter> */}
        </ModalContent>
      </Modal>
    </>
  );
};

export default EditVolumesModal_alcohol;
