import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    Button,
    Box,
    FormControl,
    FormLabel,
    Input,
    FormErrorMessage,
    HStack
  } from "@chakra-ui/react";
  import { Formik, Form, Field } from "formik";
  import * as Yup from "yup";
  
  
  const MeasuredVol = ({ activate, deactivate }) => {
    function handleSubmitData() {
      console.log("submitting edit data");
    }
  
    const validationSchema = Yup.object().shape({
      volume: Yup.number().required("You need to provide a number.."),
    });
    return (
      <>
        <Modal isOpen={activate} onClose={deactivate}>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>Modal Title</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <Formik
                // initialValues={{ firstName: "" }}
                initialValues={{
                  volume: "",
  
                  // Add this field with an initial value
                }}
                onSubmit={handleSubmitData}
                validationSchema={validationSchema}
              >
                {(formikProps) => (
                  <>
                    <Form>
                      <Box>
                        <Field name="volume">
                          {({ field, form }) => (
                            <FormControl
                              isInvalid={
                                form.errors.volume && form.touched.volume
                              }
                            >
                              <FormLabel htmlFor="volume" colorScheme="brand">
                                How much did you measure?
                              </FormLabel>
                              <Input
                                {...field}
                                id="volume"
                                placeholder="Volume"
                                _placeholder={{ opacity: 1, color: "gray.600" }}
                                // value={formfirstName}
                              />{" "}
                              <FormErrorMessage>
                                {form.errors.volume}
                              </FormErrorMessage>
                            </FormControl>
                          )}
                        </Field>
                      </Box>
  
                      <HStack>
                        <Button
                          mt={6}
                          colorScheme="red"
                          isLoading={formikProps.isSubmitting}
                          type="submit"
                          disabled={formikProps.isValid} // Disable the button if the form is not valid
                        >
                          Submit
                        </Button>
                      </HStack>
                    </Form>
                  </>
                )}
              </Formik>
            </ModalBody>
  
            <ModalFooter>
              <Button colorScheme="blue" mr={3} onClick={deactivate}>
                Close
              </Button>
              <Button variant="ghost">Secondary Action</Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      </>
    );
  };
  
  export default MeasuredVol;
  