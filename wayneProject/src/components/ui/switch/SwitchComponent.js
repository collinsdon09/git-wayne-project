import { useState } from "react";
import Switch from "react-switch";
import useSocket from "../../../hooks/sendData";

const SwitchComp = () => {
  const { socket, sendMessage, subscribeToEvent } = useSocket(
    "http://localhost:4001"
  );
  function handleChange(checked_status) {
    setChecked_status(checked_status);

    if (checked_status == true) {
      console.log("activate");
      sendMessage("main_switch", {
        msg: "activate",
      });
    } else if (checked_status == false) {
      console.log("deactivated");
      sendMessage("main_switch", {
        msg: "deactivate",
      });
    }
  }

  const [checked_status, setChecked_status] = useState(false);

  return (
    <>
      <div>
        <label>
          <span>START:</span>
          <Switch onChange={handleChange} checked={checked_status} />
        </label>
      </div>
    </>
  );
};

export default SwitchComp;
