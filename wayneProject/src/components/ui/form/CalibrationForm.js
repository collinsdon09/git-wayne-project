import { Formik, Form, Field } from "formik";
import * as Yup from "yup";

const CalibrationForm = () => {
  return (
    <>
      <Formik
        // initialValues={{ firstName: "" }}
        initialValues={{
          tap_number: "",

          // Add this field with an initial value
        }}
        onSubmit={handleStartMeasurement_Keg}
        validationSchema={validationSchema2}
      >
        {(formikProps) => (
          <>
            <Form>
              <Box mt={6} w="100%">
                <Text
                  bgGradient="linear(to-r, teal.700, green.700)"
                  bgClip="text"
                  fontSize="l"
                  fontWeight="extrabold"
                >
                  {" "}
                  Tap Number:
                </Text>
                <Field name="tap_number">
                  {({ field, form }) => (
                    <FormControl
                      isInvalid={
                        form.errors.tap_number && form.touched.tap_number
                      }
                    >
                      <Select
                        {...field}
                        placeholder="-- Select Tap --"
                        onChange={(e) =>
                          form.setFieldValue("tap_number", e.target.value)
                        }
                      >
                        {/* Add options for the Select here */}
                        <option value="1">1</option>
                        <option value="2">2 </option>
                        <option value="3">3</option>
                      </Select>
                      <FormErrorMessage>
                        {form.errors.tap_number}
                      </FormErrorMessage>
                    </FormControl>
                  )}
                </Field>
              </Box>

              <Button
                mt={6}
                colorScheme="orange"
                //isLoading={formikProps.isSubmitting}
                type="submit"
                disabled={formikProps.isValid} // Disable the button if the form is not valid
              >
                Start Measurement
              </Button>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
};

export default CalibrationForm;
