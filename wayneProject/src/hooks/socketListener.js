import { useEffect, useState } from 'react';

const useSocketListener = (socket, eventName) => {
  const [data, setData] = useState(null);

  useEffect(() => {
    // Check if the socket is available
    if (socket) {
      // Define a callback function to handle incoming data
      const handleSocketEvent = (eventData) => {
        setData(eventData);
      };

      // Listen for the specified event
      socket.on(eventName, handleSocketEvent);

      // Clean up the event listener when the component unmounts
      return () => {
        socket.off(eventName, handleSocketEvent);
      };
    }
  }, [socket, eventName]);

  return data;
};

export default useSocketListener;
