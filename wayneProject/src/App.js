import logo from "./logo.svg";
import "./App.css";
import { ChakraProvider } from "@chakra-ui/react";
import theme from "./theme.js";
import { BrowserRouter, Routes, Route, Link, NavLink } from "react-router-dom";
// import DashBoardComponent from "./components/Dashboard";
import DashBoardComponent from "./pages/Dashboard";
import CalibrationComponent from "./pages/Calibration";
import SidebarComponent from "./pages/sidebarcomponents/Sidebar";
import { Menu } from "react-pro-sidebar";
import MenuComponent from "./pages/Menu";
// import { SidebarComponent } from "./pages/Sidebar";
// import SidebarComponent from "./pages/Sidebar";

// function App() {
//   return (
//     <ChakraProvider >
//       <MainComponent />
//     </ChakraProvider>

//   );
// }

function App() {
  return (
    <ChakraProvider theme={theme}>
      <BrowserRouter>
        <Routes>
          MainLoader
          <Route path="/calibration" element={<CalibrationComponent />} />
          <Route path="/dash" element={<DashBoardComponent />} />
          <Route path="/" element={<DashBoardComponent />} />
          <Route path="/menu" element={<MenuComponent />} />


          {/* <Route path="/" element={<SidebarComponent />} /> */}
        </Routes>
      </BrowserRouter>
    </ChakraProvider>
  );
}
export default App;
