import React, { useRef, useState, CSSProperties } from "react";
import {
  Button,
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
  Radio,
  RadioGroup,
  Stack,
  Text,
  Select,
  Box,
  ButtonSpinner,
  Heading,
  SimpleGrid,
  HStack,
  Center,
} from "@chakra-ui/react";

import { Card, CardHeader, CardBody, CardFooter } from "@chakra-ui/react";

import {
  Alert,
  AlertIcon,
  AlertTitle,
  AlertDescription,
} from "@chakra-ui/react";

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from "@chakra-ui/react";

import LoadingOverlay from "react-loading-overlay";

import { Formik, Form, Field } from "formik";
import * as Yup from "yup";

import { useToast } from "@chakra-ui/react";
import { color } from "framer-motion";
import useSocket from "../hooks/sendData";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { useDisclosure } from "@chakra-ui/react";
// import MeasuredVolumeModalComponent from "../components/modal/modal/modal";
import InitializeButtonComponent from "../components/ui/initButton/InitializeButton";
import MeasuredVolumeModalComponent from "../components/ui/modal/measured_voume_modal";
// import InitializeButtonComponent from "../components/ui/InitializeButton";
// import InitializeButtonComponent from "../components/ui/Button";

// import usePostRequest from "../hook/usePostRequest";
// import ErrorAlertComponent from "../components2/ErrorAlert";
// import SuccessAlertComponent from "../components2/SuccessAlert";

const CalibrationComponent = () => {
  const [formfirstName, setFirstName] = useState("");
  const navigate = useNavigate();

  let mix_tap_ref = useRef(0);
  let alcohol_tap_ref = useRef(0);

  // const [value, setValue] = useState("Calibration Form");
  const [showExtraFields, setShowExtraFields] = useState(false);
  const toast = useToast();
  //const { loading, error, data, post } = usePostRequest();
  const [message, setMessage] = useState("");

  const [loader, setLoader] = useState(false);
  const [openModal, setModalOpen] = useState(false);
  const { isOpen, onOpen, onClose } = useDisclosure();

  // const [intilization_complete, setInitialization_message] = useState(
  //   "connect to controller"
  // );

  const [initialization_complete, setInitializationComplete] = useState("");

  const { socket, sendMessage, subscribeToEvent } = useSocket(
    "http://localhost:4001"
  );

  // console.log("websocket message:--->", receivedData);
  // setInitialization_message(receivedData);

  useEffect(() => {
    console.log("initialize everything...");
    sendMessage("init", {
      start: "intialize",
    });
    // setMessage("");

    if (socket && subscribeToEvent) {
      subscribeToEvent("initialized", (data) => {
        // Handle incoming data here
        console.log("Received data:", data.message);
        setInitializationComplete(data.message);
      });

      subscribeToEvent("measuring", (data) => {
        // Handle incoming data here
        console.log("message:", data.message);
        setLoader(true);
        //setInitializationComplete(data.message);
      });

      subscribeToEvent("measurement_stopped", (data) => {
        // Handle incoming data here
        console.log("message:", data.message);
        setLoader(false);
        setModalOpen(true);
        //setInitializationComplete(data.message);
      });

      // Don't forget to unsubscribe when the component unmounts to avoid memory leaks
      return () => {
        socket.off("initialized");
      };
    }
  }, [socket, subscribeToEvent]);

  const signupInformationRef = useRef({
    first_name: "",
    last_name: "",
    username: "",
    email: "",
    disabled: false,
    driver_category: "",
    role: "Admin",
    hashed_pwd: "",
  });

  function moveToMain() {
    console.log("move to main");
    navigate("/dash");
  }

  function intializeFunction() {
    console.log("initialize everything...");
    sendMessage("init", {
      start: "intialize",
    });
    setMessage("");
  }

  const handleSubmit_Keg = (values, actions) => {
    //   console.log("reg", values.firstName);

    console.log("values", values.volume);

    console.log("tap_number during submission", mix_tap_ref.current);

    sendMessage("measured_volume", {
      volume: values.volume,
      tap_type: "keg",
      tap_number: mix_tap_ref.current,
    });
    setMessage("");

    setMessage(values.volume);

    actions.resetForm();
  };

  const handleSubmit_alcohol = (values, actions) => {
    //   console.log("reg", values.firstName);
    console.log("tap_number during submission", mix_tap_ref.current);


    console.log("values", values.volume);

    sendMessage("measured_volume", {
      volume: values.volume,
      tap_type: "alcohol",
      tap_number: alcohol_tap_ref.current,

    });
    setMessage("");

    setMessage(values.volume);

    actions.resetForm();
  };

  function handleStartMeasurement_Keg(values, actions) {
    console.log("values", values);
    mix_tap_ref.current = values.tap_number;
    sendMessage("measure", {
      start: "start_measurement",
      tap_type: "keg",
      tap: values.tap_number,
    });
    setMessage("");
  }

  function handleStartMeasurement_Alcohol(values, actions) {
    console.log("values", values);
    alcohol_tap_ref.current = values.tap_number;

    sendMessage("measure", {
      start: "start_measurement",
      tap_type: "alcohol",
      tap: values.tap_number,
    });
    setMessage("");
  }

  const validationSchema = Yup.object().shape({
    volume: Yup.number().required("You need to provide a number.."),
  });

  const validationSchema2 = Yup.object().shape({
    tap_number: Yup.number().required(""),
  });

  function closeModal() {
    setModalOpen(false);
  }

  return (
    <>
      {/* <MeasuredVolumeModalComponent
        activate={openModal}
        deactivate={closeModal}
      /> */}

      <LoadingOverlay active={loader} spinner text="Measuring...">
        <InitializeButtonComponent
          initFunction={intializeFunction}
          init_status={initialization_complete}
        />

        <SimpleGrid
          spacing={16}
          templateColumns="repeat(auto-fill, minmax(400px, 1fr))"
          padding={8}
        >
          <Card
            // bgGradient="linear(to-l, #9AE6B4, #38B2AC)"
            bgColor={"#93c572"}
            className="drop-shadow-2xl"
          >
            <CardHeader>
              <Heading mb="10px" size="lg" color={"gray.700"}>
                {" "}
                Mix
              </Heading>

              <Alert status="warning">
                <AlertIcon />
                {/* <AlertTitle>Calibration</AlertTitle> */}
                <AlertDescription color={"gray.700"} className="font-bold">
                  Measure 500ml
                </AlertDescription>
              </Alert>

              <Formik
                // initialValues={{ firstName: "" }}
                initialValues={{
                  tap_number: "",

                  // Add this field with an initial value
                }}
                onSubmit={handleStartMeasurement_Keg}
                validationSchema={validationSchema2}
              >
                {(formikProps) => (
                  <>
                    <Form>
                      <Box mt={6} w="100%">
                        <Text
                          bgGradient="linear(to-r, teal.700, green.700)"
                          bgClip="text"
                          fontSize="l"
                          fontWeight="extrabold"
                        >
                          {" "}
                          Tap Number:
                        </Text>
                        <Field name="tap_number">
                          {({ field, form }) => (
                            <FormControl
                              isInvalid={
                                form.errors.tap_number &&
                                form.touched.tap_number
                              }
                            >
                              <Select
                                {...field}
                                placeholder="-- Select Tap --"
                                onChange={(e) =>
                                  form.setFieldValue(
                                    "tap_number",
                                    e.target.value
                                  )
                                }
                              >
                                {/* Add options for the Select here */}
                                <option value="1">1</option>
                                <option value="2">2 </option>
                                <option value="3">3</option>
                                <option value="4">4</option>

                              </Select>
                              <FormErrorMessage>
                                {form.errors.tap_number}
                              </FormErrorMessage>
                            </FormControl>
                          )}
                        </Field>
                      </Box>

                      <Button
                        mt={6}
                        colorScheme="orange"
                        //isLoading={formikProps.isSubmitting}
                        type="submit"
                        disabled={formikProps.isValid} // Disable the button if the form is not valid
                      >
                        Start Measurement
                      </Button>
                    </Form>
                  </>
                )}
              </Formik>
            </CardHeader>
            <CardBody>
              <Formik
                // initialValues={{ firstName: "" }}
                initialValues={{
                  volume: "",

                  // Add this field with an initial value
                }}
                onSubmit={handleSubmit_Keg}
                validationSchema={validationSchema}
              >
                {(formikProps) => (
                  <>
                    <Form>
                      <Box>
                        <Field name="volume">
                          {({ field, form }) => (
                            <FormControl
                              isInvalid={
                                form.errors.volume && form.touched.volume
                              }
                            >
                              <FormLabel htmlFor="volume" colorScheme="brand">
                                How much did you measure?
                              </FormLabel>
                              <Input
                                {...field}
                                id="volume"
                                placeholder="Volume"
                                _placeholder={{ opacity: 1, color: "gray.600" }}
                                // value={formfirstName}
                              />{" "}
                              <FormErrorMessage>
                                {form.errors.volume}
                              </FormErrorMessage>
                            </FormControl>
                          )}
                        </Field>
                      </Box>

                      <HStack>
                        <Button
                          mt={6}
                          colorScheme="red"
                          isLoading={formikProps.isSubmitting}
                          type="submit"
                          disabled={formikProps.isValid} // Disable the button if the form is not valid
                        >
                          Submit
                        </Button>
                      </HStack>
                    </Form>
                  </>
                )}
              </Formik>
            </CardBody>
            {/* <CardFooter>
            <Button>View here</Button>
          </CardFooter> */}
          </Card>
          {/* ************************************************************************** */}
          <Card
            // bgGradient="linear(to-l, #9AE6B4, #38B2AC)"
            bgColor={"#93c572"}
            className="drop-shadow-2xl"
          >
            <CardHeader>
              <Heading mb="10px" size="lg" color={"gray.700"}>
                {" "}
                Alcohol
              </Heading>

              <Alert status="warning">
                <AlertIcon />
                {/* <AlertTitle>Calibration</AlertTitle> */}
                <AlertDescription color={"gray.800"} className="font-bold">
                  Measure 500ml
                </AlertDescription>
              </Alert>
              <Formik
                initialValues={{
                  tap_number: "",

                  // Add this field with an initial value
                }}
                onSubmit={handleStartMeasurement_Alcohol}
                validationSchema={validationSchema2}
              >
                {(formikProps) => (
                  <>
                    <Form>
                      <Box mt={6} w="100%">
                        <Text
                          bgGradient="linear(to-r, teal.700, green.700)"
                          bgClip="text"
                          fontSize="l"
                          fontWeight="extrabold"
                        >
                          {" "}
                          Tap Number:
                        </Text>
                        <Field name="tap_number">
                          {({ field, form }) => (
                            <FormControl
                              isInvalid={
                                form.errors.tap_number &&
                                form.touched.tap_number
                              }
                            >
                              <Select
                                {...field}
                                placeholder="-- Select Tap --"
                                onChange={(e) =>
                                  form.setFieldValue(
                                    "tap_number",
                                    e.target.value
                                  )
                                }
                              >
                                {/* Add options for the Select here */}
                                <option value="1">1</option>
                                <option value="2">2 </option>
                                <option value="3">3</option>
                                <option value="4">4</option>

                              </Select>
                              <FormErrorMessage>
                                {form.errors.tap_number}
                              </FormErrorMessage>
                            </FormControl>
                          )}
                        </Field>
                      </Box>

                      <Button
                        mt={6}
                        colorScheme="orange"
                        //isLoading={formikProps.isSubmitting}
                        type="submit"
                        disabled={formikProps.isValid} // Disable the button if the form is not valid
                      >
                        Start Measurement
                      </Button>
                    </Form>
                  </>
                )}
              </Formik>
            </CardHeader>
            <CardBody>
              <Formik
                // initialValues={{ firstName: "" }}
                initialValues={{
                  volume: "",

                  // Add this field with an initial value
                }}
                onSubmit={handleSubmit_alcohol}
                validationSchema={validationSchema}
              >
                {(formikProps) => (
                  <>
                    <Form>
                      <Box>
                        <Field name="volume">
                          {({ field, form }) => (
                            <FormControl
                              isInvalid={
                                form.errors.volume && form.touched.volume
                              }
                            >
                              <FormLabel htmlFor="volume">
                                How much did you measure?
                              </FormLabel>
                              <Input
                                {...field}
                                id="volume"
                                placeholder="Volume"
                                _placeholder={{ opacity: 1, color: "gray.600" }}
                                // value={formfirstName}
                              />{" "}
                              <FormErrorMessage>
                                {form.errors.volume}
                              </FormErrorMessage>
                            </FormControl>
                          )}
                        </Field>
                      </Box>

                      <HStack>
                        <Button
                          mt={6}
                          colorScheme="red"
                          isLoading={formikProps.isSubmitting}
                          type="submit"
                          disabled={formikProps.isValid} // Disable the button if the form is not valid
                        >
                          Submit
                        </Button>
                      </HStack>
                    </Form>
                  </>
                )}
              </Formik>
            </CardBody>
            {/* <CardFooter>
            <Button>View here</Button>
          </CardFooter> */}
          </Card>
        </SimpleGrid>

        <Box padding={10} alignContent="right">
          <Button
            colorScheme="teal"
            width="250px"
            className="drop-shadow-xl"
            color={"yellow.300"}
            onClick={moveToMain}
          >
            Proceed Without Calibration
          </Button>
        </Box>
      </LoadingOverlay>
    </>
  );
};

export default CalibrationComponent;
