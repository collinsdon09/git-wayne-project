import { useState } from "react";
import { Sidebar, Menu, MenuItem } from "react-pro-sidebar";
import { Link } from "react-router-dom";
import DashBoardComponent from "../Dashboard";

const SidebarComponent = () => {
  const [rtl, setRtl] = useState(false);

  // const handleRTLChange = (e: React.ChangeEvent<HTMLInputElement>) => {
  //   setRtl(e.target.checked);
  // };

  return (
    <>
      <div
        style={{
          display: "flex",
          height: "100%",
          direction: rtl ? "rtl" : "ltr",
        }}
      >
        <Sidebar rtl={rtl}>
          <Menu
            menuItemStyles={{
              button: {
                // the active class will be added automatically by react router
                // so we can use it to style the active menu item
                [`&.active`]: {
                  backgroundColor: "#13395e",
                  color: "#b6c8d9",
                },
              },
            }}
          >
            <MenuItem component={<Link to="/dash" />}> Documentation</MenuItem>
            <MenuItem component={<Link to="/calendar" />}> Calendar</MenuItem>
            <MenuItem component={<Link to="/e-commerce" />}>
              {" "}
              E-commerce
            </MenuItem>
          </Menu>
        </Sidebar>

        {/* <main> */}
        <div style={{ padding: "16px 24px", color: "#44596e" }}>
          <div style={{ marginBottom: "16px" }}>
            {/* {broken && (
              <button
                className="sb-button"
                onClick={() => setToggled(!toggled)}
              >
                Toggle
              </button>
            )} */}
          </div>
       

          {/* <DashBoardComponent /> */}
          {/* 
            <div style={{ padding: "0 8px" }}>
              <div style={{ marginBottom: 16 }}>
                <Switch
                  id="collapse"
                  checked={collapsed}
                  onChange={() => setCollapsed(!collapsed)}
                  label="Collapse"
                />
              </div>

              <div style={{ marginBottom: 16 }}>
                <Switch
                  id="rtl"
                  checked={rtl}
                  onChange={handleRTLChange}
                  label="RTL"
                />
              </div>

              <div style={{ marginBottom: 16 }}>
                <Switch
                  id="theme"
                  checked={theme === "dark"}
                  onChange={handleThemeChange}
                  label="Dark theme"
                />
              </div>

              <div style={{ marginBottom: 16 }}>
                <Switch
                  id="image"
                  checked={hasImage}
                  onChange={handleImageChange}
                  label="Image"
                /> */}
        </div>
      </div>
    </>
  );
};

export default SidebarComponent;
