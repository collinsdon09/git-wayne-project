import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  SimpleGrid,
  Heading,
  Text,
  Button,
  Box,
} from "@chakra-ui/react";
import { useState } from "react";

import Chart from "react-google-charts";
// import OdoComponent from "./Odometer";
import { BiEditAlt } from "react-icons/bi";

const gaugeData = [
  ["Label", "Value"],
  ["Mix", 100],
  ["Alcohol", 75],
  // ['Network', 68],
];

const DashBoardComponent = () => {
  const [selectedMenuItem, setSelectedMenuItem] = useState(null);

  const handleMenuItemClick = (menuItem) => {
    setSelectedMenuItem(menuItem);

  };
  return (
    <>
      {/* <GaugeChart id="gauge-chart2" nrOfLevels={1} percent={1} /> */}
      <div className="flex flex-row bg-gray-200">
        <div className="w-[200px] border-2 mt-2 flex flex-col bg-gray-100 rounded-xl m-3 ">
          <div className="m-3 ">
            {" "}
            {/*Side bar div */}
            <p className="font-bold text-4xl">Options</p>
          </div>

          <div className="m-5 mt-8 gap-8 flex flex-col">
            {" "}
            {/*Menu options div */}
            {/* <p>Calibration</p> */}
            <div
              onClick={() => handleMenuItemClick("Calibration")}
              className={`menu-item p-2 rounded-md cursor-pointer ${
                selectedMenuItem === "Calibration"
                  ? "bg-blue-500 text-white"
                  : "bg-gray-100"
              }`}

            >
              <p>Calibration</p>
            </div>
            <div
              onClick={() => handleMenuItemClick("Analytics")}
              className={`menu-item p-2 rounded-md cursor-pointer ${
                selectedMenuItem === "Analytics"
                  ? "bg-blue-500 text-white"
                  : "bg-gray-100"
              }`}
            >
              <p>Analytics</p>
            </div>
          </div>
        </div>

        <div className="m-4 flex flex-col gap-2">
          <div className="border-2 shadow-md w-[800px] h-[250px] rounded-xl bg-gray-100 ">
            <h2 className="ml-3">Group 1</h2>

            <div className="flex flex-col">
              <div className="flex flex-row">
                <Chart
                  width={400}
                  height={200}
                  chartType="Gauge"
                  loader={<div>Loading Chart</div>}
                  data={gaugeData}
                  options={{
                    redFrom: 450,
                    redTo: 500,
                    yellowFrom: 400,
                    yellowTo: 500,
                    minorTicks: 5,
                    max: 500,
                  }}
                  rootProps={{ "data-testid": "1" }}
                />

                <div className=" flex flex-col w-[200px] justify-center  h-[100px] bg-gray-0 border-2 shadow-lg m-4 rounded-lg">
                  <div className="ml-4 text-sm">
                    {" "}
                    <p className=" font-bold-md text-lg">Mix</p>
                    <div className="flex flex-row gap-1">
                      <p className="mb-1">
                        max vol:{" "}
                        <span className="font-bold text-xl">{19000}</span>
                      </p>
                      <BiEditAlt />
                    </div>
                    {/* <p className="font-bold text-2xl ">{19000}</p> */}
                    <div className="flex flex-row gap-1">
                      <p className="mb-1">
                        int vol:{" "}
                        <span className="font-bold text-xl">{19000}</span>
                      </p>

                      <BiEditAlt />
                    </div>
                  </div>
                </div>

                <div className=" flex flex-col w-[200px] justify-center  h-[100px] bg-gray-0 border-2 shadow-lg m-4 rounded-lg">
                  <div className="ml-4 text-sm">
                    {" "}
                    <p className=" font-bold-md text-lg">Alcohol</p>
                    <p className="mb-1">
                      max vol:{" "}
                      <span className="font-bold text-xl">{19000}</span>
                    </p>
                    {/* <p className="font-bold text-2xl ">{19000}</p> */}
                    <p className="mb-1">
                      interval vol:{" "}
                      <span className="font-bold text-xl">{19000}</span>
                    </p>
                    {/* <p className="font-bold text-2xl ">{19000}</p> */}
                    {/* <div className="flex flex-row text-center gap-1">
                    {" "}
                    <p className="">Relay Status: </p>
                    <div className="h-4 w-4 bg-green-500 rounded-lg mt-1" />
                  </div> */}
                  </div>
                </div>
              </div>

              <div className="flex flex-row gap-48 ml-20 ">
                <div className="h-4 w-4 rounded-lg bg-green-500" />
                <div className="h-4 w-4 rounded-lg bg-red-500" />
                {/* <p className="">TAP:ON</p>
                <p className="">TAP:ON</p> */}
              </div>
            </div>
          </div>

          <div className="border-2 shadow-md w-[700px] h-[250px] bg-gray-100 rounded-xl">
            <h2>Group 2</h2>
            <Chart
              width={400}
              height={300}
              chartType="Gauge"
              loader={<div>Loading Chart</div>}
              data={gaugeData}
              options={{
                redFrom: 450,
                redTo: 500,
                yellowFrom: 400,
                yellowTo: 500,
                minorTicks: 5,
                max: 500,
              }}
              rootProps={{ "data-testid": "1" }}
            />
          </div>

          <div className="border-2 shadow-md w-[700px] h-[250px] rounded-xl bg-gray-100">
            <h2 className="p-1">Group 3</h2>
            <Chart
              width={400}
              height={300}
              chartType="Gauge"
              loader={<div>Loading Chart</div>}
              data={gaugeData}
              options={{
                redFrom: 450,
                redTo: 500,
                yellowFrom: 400,
                yellowTo: 500,
                minorTicks: 5,
                max: 500,
              }}
              rootProps={{ "data-testid": "1" }}
            />
          </div>
        </div>
      </div>

      {/* <OdoComponent /> */}
    </>
  );
};

export default DashBoardComponent;