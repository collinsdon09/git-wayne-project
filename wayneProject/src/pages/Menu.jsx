import React, { useState } from "react";


const MenuComponent = () => {
  // Set the initial selected menu item to "Calibration"
  const [selectedMenuItem, setSelectedMenuItem] = useState("Calibration");

  const handleMenuItemClick = (menuItem) => {
    setSelectedMenuItem(menuItem);
  };
  return (
    <>
      <div className="flex flex-row bg-gray-200">
        {/* ... (sidebar code remains the same) */}

        <div className="m-4 flex flex-col gap-2">
          {selectedMenuItem === "Calibration" && (
            <div className="border-2 shadow-md w-[800px] h-[250px] rounded-xl bg-gray-100 ">
              {/* Content for "Calibration" menu item */}
            </div>
          )}

          {selectedMenuItem === "Analytics" && (
            <div className="border-2 shadow-md w-[700px] h-[250px] bg-gray-100 rounded-xl">
              {/* Content for "Analytics" menu item */}
            </div>
          )}

          {/* ... (other menu items and their respective content) */}
        </div>
      </div>
    </>
  );
};

export default MenuComponent;
