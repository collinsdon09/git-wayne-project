import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  Box,
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
  HStack,
} from "@chakra-ui/react";

import { Formik, Form, Field } from "formik";
import * as Yup from "yup";

import React, { useEffect, useState } from "react";
import Chart from "react-google-charts";
import { BiEditAlt } from "react-icons/bi";
import EditVolumesModal from "../components/ui/modal/editVolumes/Group_1/editMixVolumes";
import EditVolumesModal_alcohol from "../components/ui/modal/editVolumes/Group_1/editAlcoholVolumes";
import EditVolumesModal_mix from "../components/ui/modal/editVolumes/Group_1/editMixVolumes";
import useSocket from "../hooks/sendData";
import { io } from "socket.io-client";
import { useRef } from "react";
import SwitchComp from "../components/ui/switch/SwitchComponent";
import CalibrationComponent from "./Calibration";
import { Navigate, useNavigate } from "react-router-dom";
import InitializeButtonComponent from "../components/ui/initButton/InitializeButton";

// import EditVolumesModal from "../components/ui/modal/editVolumes/Group_1/editMixVolumes";
// import EditVolumesModal_alcohol from "../components/ui/modal/Group_1/";

const gaugeData = [
  ["Label", "Value"],
  ["Mix", 100],
  ["Alcohol", 75],
];

const DashBoardComponent = () => {
  const navigate = useNavigate();

  // Set the initial selected menu item to "Calibration"
  const [selectedMenuItem, setSelectedMenuItem] = useState("Calibration");
  const [openeditVolumeModal_G1_mix, setEditVolumeModal_G1_mix] =
    useState(false);
  const [openeditVolumeModal_G1_alcohol, setEditVolumeModal_G1_alcohol] =
    useState(false);

  const [openeditVolumeModal_G2_mix, setEditVolumeModal_G2_mix] =
    useState(false);
  const [openeditVolumeModal_G2_alcohol, setEditVolumeModal_G2_alcohol] =
    useState(false);

  const [openeditVolumeModal_G3_mix, setEditVolumeModal_G3_mix] =
    useState(false);
  const [openeditVolumeModal_G3_alcohol, setEditVolumeModal_G3_alcohol] =
    useState(false);

  const [openeditVolumeModal_G4_mix, setEditVolumeModal_G4_mix] =
    useState(false);
  const [openeditVolumeModal_G4_alcohol, setEditVolumeModal_G4_alcohol] =
    useState(false);

  const [initialization_complete, setInitializationComplete] = useState("");

  let G1_ref_mix = useRef(0);
  let G1_ref_alcohol = useRef(0);

  let G1_ref_mix_vol = useRef(0);
  let G1_ref_alcohol_vol = useRef(0);

  let G2_ref_mix = useRef(0);
  let G2_ref_alcohol = useRef(0);

  let G2_ref_mix_vol = useRef(0);
  let G2_ref_alcohol_vol = useRef(0);

  let G3_ref_mix = useRef(0);
  let G3_ref_alcohol = useRef(0);

  let G3_ref_mix_vol = useRef(0);
  let G3_ref_alcohol_vol = useRef(0);

  let G4_ref_mix = useRef(0);
  let G4_ref_alcohol = useRef(0);

  let G4_ref_mix_vol = useRef(0);
  let G4_ref_alcohol_vol = useRef(0);

  let G1_measured_volume_ref = useRef(0);
  let G2_measured_volume_ref = useRef(0);
  let G3_measured_volume_ref = useRef(0);
  let G4_measured_volume_ref = useRef(0);

  // G1_ref_mix_vol.current = 100

  const [MixData, setMixData] = useState({});

  const { socket, sendMessage, subscribeToEvent } = useSocket(
    "http://localhost:4001"
  );

  const validationSchema = Yup.object().shape({
    volume: Yup.number().required("You need to provide a number.."),
    interval_volume: Yup.number().required("You need to provide a number.."),
  });

  const handleMenuItemClick = (menuItem) => {
    setSelectedMenuItem(menuItem);
  };

  function handleConfigurationModal_G1_mix() {
    console.log("configuration modal g1 mix");
    setEditVolumeModal_G1_mix(true);
  }

  function handleConfigurationModal_G1_alcohol() {
    console.log("configuration modal g1 alcohol");
    setEditVolumeModal_G1_alcohol(true);
  }

  function handleConfigurationModal_G2_mix() {
    console.log("configuration modal g2 mix");
    setEditVolumeModal_G2_mix(true);
  }

  function handleConfigurationModal_G2_alcohol() {
    console.log("configuration modal g2 alcohol");
    setEditVolumeModal_G2_alcohol(true);
  }

  function handleConfigurationModal_G3_mix() {
    console.log("configuration modal g3 mix");
    setEditVolumeModal_G3_mix(true);
  }

  function handleConfigurationModal_G3_alcohol() {
    console.log("configuration modal g3 alcohol");
    setEditVolumeModal_G3_alcohol(true);
  }

  function handleConfigurationModal_G4_mix() {
    console.log("configuration modal g4 mix");
    setEditVolumeModal_G4_mix(true);
  }

  function handleConfigurationModal_G4_alcohol() {
    console.log("configuration modal alcohol g4");
    setEditVolumeModal_G4_alcohol(true);
  }

  function closeModal_G1_mix() {
    setEditVolumeModal_G1_mix(false);
  }

  function closeModal_G1_alcohol() {
    setEditVolumeModal_G1_alcohol(false);
  }

  function closeModal_G2_mix() {
    setEditVolumeModal_G2_mix(false);
  }

  function closeModal_G2_alcohol() {
    setEditVolumeModal_G2_alcohol(false);
  }

  function closeModal_G3_mix() {
    setEditVolumeModal_G3_mix(false);
  }

  function closeModal_G3_alcohol() {
    setEditVolumeModal_G3_alcohol(false);
  }

  function closeModal_G4_mix() {
    setEditVolumeModal_G4_mix(false);
  }

  function closeModal_G4_alcohol() {
    setEditVolumeModal_G4_alcohol(false);
  }

  function G1_handleSubmitData_mix(values, actions) {
    console.log("submitting edit data g1 mix", values);
    sendMessage("G1_form_data_mix", {
      // liquid_type: "mix",
      // group: "1",
      max_vol: parseFloat(values.volume),
      interval_vol: parseFloat(values.interval_volume),
    });
    actions.resetForm();
  }

  function G1_handleSubmitData_alcohol(values, actions) {
    console.log("submitting edit data g1 alcohol", values);
    sendMessage("G1_form_data_alcohol", {
      // liquid_type: "alcohol",
      // group: "1",
      max_vol: parseFloat(values.volume),
      interval_vol: parseFloat(values.interval_volume),
    });
  }

  function G2_handleSubmitData_mix(values, actions) {
    console.log("submitting edit data g2 mix", values);
    sendMessage("G2_form_data_mix", {
      // liquid_type: "mix",
      // group: "1",
      max_vol: parseFloat(values.volume),
      interval_vol: parseFloat(values.interval_volume),
    });
    actions.resetForm();
  }

  function G2_handleSubmitData_alcohol(values, actions) {
    console.log("submitting edit data g2 alcohol", values);
    sendMessage("G2_form_data_alcohol", {
      // liquid_type: "alcohol",
      // group: "1",
      max_vol: parseFloat(values.volume),
      interval_vol: parseFloat(values.interval_volume),
    });
  }

  function G3_handleSubmitData_mix(values, actions) {
    console.log("submitting edit data g3 mix", values);
    sendMessage("G3_form_data_mix", {
      // liquid_type: "mix",
      // group: "1",
      max_vol: parseFloat(values.volume),
      interval_vol: parseFloat(values.interval_volume),
    });
    actions.resetForm();
  }

  function G3_handleSubmitData_alcohol(values, actions) {
    console.log("submitting edit data g3 alcohol", values);
    sendMessage("G3_form_data_alcohol", {
      // liquid_type: "alcohol",
      // group: "1",
      max_vol: parseFloat(values.volume),
      interval_vol: parseFloat(values.interval_volume),
    });
  }

  function G4_handleSubmitData_mix(values, actions) {
    console.log("submitting edit data g4 mix", values);
    sendMessage("G4_form_data_mix", {
      // liquid_type: "mix",
      // group: "1",
      max_vol: parseFloat(values.volume),
      interval_vol: parseFloat(values.interval_volume),
    });
    actions.resetForm();
  }

  function G4_handleSubmitData_alcohol(values, actions) {
    console.log("submitting edit data g4 alcohol", values);
    sendMessage("G4_form_data_alcohol", {
      // liquid_type: "alcohol",
      // group: "1",
      max_vol: parseFloat(values.volume),
      interval_vol: parseFloat(values.interval_volume),
    });
  }

  function start_measuring_func() {
    console.log("start gauge measurement");
    sendMessage("start_gauge_measurement", {
      start: "start_gauge_measurement",
    });
  }

  function intializeFunction() {
    console.log("initialize everything...");
    sendMessage("init", {
      start: "intialize",
    });
    // setMessage("");
  }

  useEffect(() => {
    console.log("use effect..");
    // setSelectedMenuItem('Calibration')

    console.log("initialize everything...");
    sendMessage("init", {
      start: "intialize",
    });

    sendMessage("refresh_data", {
      start: "refresh_data",
    });
    // setMessage("");

    if (socket && subscribeToEvent) {
      subscribeToEvent("refresh", (data) => {
        console.log("received after refresh", data);
        G1_ref_mix.current = data.message.G1__mix_particulars;
        G1_ref_alcohol.current = data.message.G1__alcohol_particulars;

        G2_ref_mix.current = data.message.G2__mix_particulars;
        G2_ref_alcohol.current = data.message.G2__alcohol_particulars;

        G3_ref_mix.current = data.message.G3__mix_particulars;
        G3_ref_alcohol.current = data.message.G3__alcohol_particulars;

        G4_ref_mix.current = data.message.G4__mix_particulars;
        G4_ref_alcohol.current = data.message.G4__alcohol_particulars;

        // G1_ref_mix.current = data.message.G1__mix_particulars;
        // G1_ref_alcohol.current = data.message.G1__alcohol_particulars;
      });

      subscribeToEvent("initialized", (data) => {
        // Handle incoming data here
        console.log("Received data:", data.message);
        setInitializationComplete(data.message);
      });

      subscribeToEvent("G1_mix_data_event", (data) => {
        console.log("receiving G1 mix data", data);
        // setMixData(data);
        G1_ref_mix.current = data.message;
        // console.log("ref", ref_mix.current);
      });

      subscribeToEvent("G1_alcohol_data_event", (data) => {
        console.log("receiving G1 alcohol data", data);
        G1_ref_alcohol.current = data.message;
      });

      subscribeToEvent("G2_mix_data_event", (data) => {
        console.log("receiving G2 mix data", data);
        // setMixData(data);
        G2_ref_mix.current = data.message;
        // console.log("ref", ref_mix.current);
      });

      subscribeToEvent("G2_alcohol_data_event", (data) => {
        console.log("receiving G2 alcohol data", data);
        G2_ref_alcohol.current = data.message;
      });

      subscribeToEvent("G3_mix_data_event", (data) => {
        console.log("receiving G3 mix data", data);
        // setMixData(data);
        G3_ref_mix.current = data.message;
        // console.log("ref", ref_mix.current);
      });

      subscribeToEvent("G3_alcohol_data_event", (data) => {
        console.log("receiving G3 alcohol data", data);
        G3_ref_alcohol.current = data.message;
      });

      subscribeToEvent("G4_mix_data_event", (data) => {
        console.log("receiving G4 mix data", data);
        // setMixData(data);
        G4_ref_mix.current = data.message;
        // console.log("ref", ref_mix.current);
      });

      subscribeToEvent("G4_alcohol_data_event", (data) => {
        console.log("receiving G4 alcohol data", data);
        G4_ref_alcohol.current = data.message;
      });

      subscribeToEvent("G1_measured_volume_data", (data) => {
        console.log("receiving G1 volume data", data);
        console.log("volume mix", data.message.volume_mix)
        // G1_measured_volume_ref.current = data.message;

        G1_ref_mix_vol.current = data.message.volume_mix;
        G1_ref_alcohol_vol.current = data.g1_mix_vol;
      });

      subscribeToEvent("G2_measured_volume_data", (data) => {
        console.log("receiving G2 measured volume data", data);
        G2_measured_volume_ref.current = data.message;
      });

      subscribeToEvent("G3_measured_volume_data", (data) => {
        console.log("receiving G4 measured volume data", data);
        G3_measured_volume_ref.current = data.message;
      });

      subscribeToEvent("G4_measured_volume_data", (data) => {
        console.log("receiving G4 measured volume data", data);
        G4_measured_volume_ref.current = data.message;
      });

      // subscribeToEvent("volume_data", (data) => {
      //   console.log("receiving volume_data", data);
      //   G1_ref_mix_vol.current = data.message;
      //   G1_ref_alcohol_vol.current = data.message;
      // });

      subscribeToEvent("measurement_stopped", (data) => {
        // Handle incoming data here
        console.log("message:", data.message);
      });

      // Don't forget to unsubscribe when the component unmounts to avoid memory leaks
      return () => {
        socket.off("initialized");
      };
    }
  }, [socket]);

  const incoming_json = {
    Group_1: {
      relay_mix: "ON",
      relay_alcohol: "OFF",
      Mix: {
        max_vol: G1_ref_mix.current.max_vol,
        interval_vol: G1_ref_mix.current.interval_vol,
      },

      Alcohol: {
        max_vol: G1_ref_alcohol.current.max_vol,
        interval_vol: G1_ref_alcohol.current.interval_vol,
      },
    },

    Group_2: {
      relay_mix: "ON",
      relay_alcohol: "OFF",
      Mix: {
        max_vol: G2_ref_mix.current.max_vol,
        interval_vol: G2_ref_mix.current.interval_vol,
      },

      Alcohol: {
        max_vol: G2_ref_alcohol.current.max_vol,
        interval_vol: G2_ref_alcohol.current.interval_vol,
      },
    },

    Group_3: {
      relay_mix: "ON",
      relay_alcohol: "OFF",
      Mix: {
        max_vol: G3_ref_mix.current.max_vol,
        interval_vol: G3_ref_mix.current.max_vol,
      },

      Alcohol: {
        max_vol: G3_ref_alcohol.current.max_vol,
        interval_vol: G3_ref_alcohol.current.max_vol,
      },
    },

    Group_4: {
      relay_mix: "ON",
      relay_alcohol: "OFF",
      Mix: {
        max_vol: G4_ref_mix.current.max_vol,
        interval_vol: G4_ref_mix.current.max_vol,
      },

      Alcohol: {
        max_vol: G4_ref_alcohol.current.max_vol,
        interval_vol: G4_ref_alcohol.current.max_vol,
      },
    },
  };

  const G1_gaugeData = [
    ["Label", "Value"],
    ["Mix", G1_ref_mix_vol.current],
    ["Alcohol", G1_ref_alcohol_vol.current],
  ];

  const G2_gaugeData = [
    ["Label", "Value"],
    ["Mix", G2_ref_mix_vol.current],
    ["Alcohol", G2_ref_alcohol_vol.current],
  ];

  const G3_gaugeData = [
    ["Label", "Value"],
    ["Mix", G3_ref_mix_vol.current],
    ["Alcohol", G3_ref_alcohol_vol.current],
  ];

  const G4_gaugeData = [
    ["Label", "Value"],
    ["Mix", G4_ref_mix_vol.current],
    ["Alcohol", G4_ref_alcohol_vol.current],
  ];

  return (
    <>
      {/* <SwitchComp /> */}
      <InitializeButtonComponent
        initFunction={intializeFunction}
        init_status={initialization_complete}
      />

      <Modal isOpen={openeditVolumeModal_G1_mix} onClose={closeModal_G1_mix}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Edit Volume for Mix G1</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Formik
              // initialValues={{ firstName: "" }}
              initialValues={{
                volume: "",

                // Add this field with an initial value
              }}
              onSubmit={G1_handleSubmitData_mix}
              validationSchema={validationSchema}
            >
              {(formikProps) => (
                <>
                  <Form>
                    <Box>
                      <Field name="volume">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.volume && form.touched.volume
                            }
                          >
                            <FormLabel htmlFor="max_volume" colorScheme="brand">
                              Maximum Volume: [Mix]
                            </FormLabel>
                            <Input
                              {...field}
                              id="volume"
                              placeholder="Maximum Volume"
                              _placeholder={{ opacity: 1, color: "gray.600" }}
                              // value={formfirstName}
                            />{" "}
                            <FormErrorMessage>
                              {form.errors.volume}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>

                      <Field name="interval_volume">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.volume && form.touched.volume
                            }
                          >
                            <FormLabel
                              htmlFor="interval_volume"
                              colorScheme="brand"
                            >
                              Interval Volume: [Mix]
                            </FormLabel>
                            <Input
                              {...field}
                              id="interval_volume"
                              placeholder="interval Volume"
                              _placeholder={{ opacity: 1, color: "gray.600" }}
                              // value={formfirstName}
                            />{" "}
                            <FormErrorMessage>
                              {form.errors.interval_volume}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>
                    </Box>

                    <HStack>
                      <Button
                        mt={6}
                        colorScheme="red"
                        isLoading={formikProps.isSubmitting}
                        type="submit"
                        disabled={formikProps.isValid} // Disable the button if the form is not valid
                      >
                        Submit
                      </Button>
                    </HStack>
                  </Form>
                </>
              )}
            </Formik>
          </ModalBody>

          {/* <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={deactivate}>
              Close
            </Button>
            <Button variant="ghost">Secondary Action</Button>
          </ModalFooter> */}
        </ModalContent>
      </Modal>

      <Modal
        isOpen={openeditVolumeModal_G1_alcohol}
        onClose={closeModal_G1_alcohol}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Edit Volumes for Alcohol G1</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Formik
              // initialValues={{ firstName: "" }}
              initialValues={{
                volume: "",

                // Add this field with an initial value
              }}
              onSubmit={G1_handleSubmitData_alcohol}
              validationSchema={validationSchema}
            >
              {(formikProps) => (
                <>
                  <Form>
                    <Box>
                      <Field name="volume">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.volume && form.touched.volume
                            }
                          >
                            <FormLabel htmlFor="volume" colorScheme="brand">
                              Maximum Volume: [Alcohol]
                            </FormLabel>
                            <Input
                              {...field}
                              id="interval_volume"
                              placeholder="interval volume"
                              _placeholder={{ opacity: 1, color: "gray.600" }}
                              // value={formfirstName}
                            />{" "}
                            <FormErrorMessage>
                              {form.errors.volume}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>

                      <Field name="interval_volume">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.volume && form.touched.volume
                            }
                          >
                            <FormLabel htmlFor="volume" colorScheme="brand">
                              Interval Volume: [Alcohol]
                            </FormLabel>
                            <Input
                              {...field}
                              id="volume"
                              placeholder="Volume"
                              _placeholder={{ opacity: 1, color: "gray.600" }}
                              // value={formfirstName}
                            />{" "}
                            <FormErrorMessage>
                              {form.errors.volume}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>
                    </Box>

                    <HStack>
                      <Button
                        mt={6}
                        colorScheme="red"
                        isLoading={formikProps.isSubmitting}
                        type="submit"
                        disabled={formikProps.isValid} // Disable the button if the form is not valid
                      >
                        Submit
                      </Button>
                    </HStack>
                  </Form>
                </>
              )}
            </Formik>
          </ModalBody>

          {/* <ModalFooter>
              <Button colorScheme="blue" mr={3} onClick={deactivate}>
                Close
              </Button>
              <Button variant="ghost">Secondary Action</Button>
            </ModalFooter> */}
        </ModalContent>
      </Modal>

      <Modal isOpen={openeditVolumeModal_G2_mix} onClose={closeModal_G2_mix}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Edit Volume for Mix G2</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Formik
              // initialValues={{ firstName: "" }}
              initialValues={{
                volume: "",

                // Add this field with an initial value
              }}
              onSubmit={G2_handleSubmitData_mix}
              validationSchema={validationSchema}
            >
              {(formikProps) => (
                <>
                  <Form>
                    <Box>
                      <Field name="volume">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.volume && form.touched.volume
                            }
                          >
                            <FormLabel htmlFor="max_volume" colorScheme="brand">
                              Maximum Volume: [Mix]
                            </FormLabel>
                            <Input
                              {...field}
                              id="volume"
                              placeholder="Maximum Volume"
                              _placeholder={{ opacity: 1, color: "gray.600" }}
                              // value={formfirstName}
                            />{" "}
                            <FormErrorMessage>
                              {form.errors.volume}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>

                      <Field name="interval_volume">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.volume && form.touched.volume
                            }
                          >
                            <FormLabel
                              htmlFor="interval_volume"
                              colorScheme="brand"
                            >
                              Interval Volume: [Mix]
                            </FormLabel>
                            <Input
                              {...field}
                              id="interval_volume"
                              placeholder="interval Volume"
                              _placeholder={{ opacity: 1, color: "gray.600" }}
                              // value={formfirstName}
                            />{" "}
                            <FormErrorMessage>
                              {form.errors.interval_volume}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>
                    </Box>

                    <HStack>
                      <Button
                        mt={6}
                        colorScheme="red"
                        isLoading={formikProps.isSubmitting}
                        type="submit"
                        disabled={formikProps.isValid} // Disable the button if the form is not valid
                      >
                        Submit
                      </Button>
                    </HStack>
                  </Form>
                </>
              )}
            </Formik>
          </ModalBody>

          {/* <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={deactivate}>
              Close
            </Button>
            <Button variant="ghost">Secondary Action</Button>
          </ModalFooter> */}
        </ModalContent>
      </Modal>

      <Modal
        isOpen={openeditVolumeModal_G2_alcohol}
        onClose={closeModal_G2_alcohol}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Edit Volumes for Alcohol G2</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Formik
              // initialValues={{ firstName: "" }}
              initialValues={{
                volume: "",

                // Add this field with an initial value
              }}
              onSubmit={G2_handleSubmitData_alcohol}
              validationSchema={validationSchema}
            >
              {(formikProps) => (
                <>
                  <Form>
                    <Box>
                      <Field name="volume">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.volume && form.touched.volume
                            }
                          >
                            <FormLabel htmlFor="volume" colorScheme="brand">
                              Maximum Volume: [Alcohol]
                            </FormLabel>
                            <Input
                              {...field}
                              id="interval_volume"
                              placeholder="interval volume"
                              _placeholder={{ opacity: 1, color: "gray.600" }}
                              // value={formfirstName}
                            />{" "}
                            <FormErrorMessage>
                              {form.errors.volume}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>

                      <Field name="interval_volume">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.volume && form.touched.volume
                            }
                          >
                            <FormLabel htmlFor="volume" colorScheme="brand">
                              Interval Volume: [Alcohol]
                            </FormLabel>
                            <Input
                              {...field}
                              id="volume"
                              placeholder="Volume"
                              _placeholder={{ opacity: 1, color: "gray.600" }}
                              // value={formfirstName}
                            />{" "}
                            <FormErrorMessage>
                              {form.errors.volume}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>
                    </Box>

                    <HStack>
                      <Button
                        mt={6}
                        colorScheme="red"
                        isLoading={formikProps.isSubmitting}
                        type="submit"
                        disabled={formikProps.isValid} // Disable the button if the form is not valid
                      >
                        Submit
                      </Button>
                    </HStack>
                  </Form>
                </>
              )}
            </Formik>
          </ModalBody>

          {/* <ModalFooter>
              <Button colorScheme="blue" mr={3} onClick={deactivate}>
                Close
              </Button>
              <Button variant="ghost">Secondary Action</Button>
            </ModalFooter> */}
        </ModalContent>
      </Modal>

      <Modal isOpen={openeditVolumeModal_G3_mix} onClose={closeModal_G3_mix}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Edit Volume for Mix G3</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Formik
              // initialValues={{ firstName: "" }}
              initialValues={{
                volume: "",

                // Add this field with an initial value
              }}
              onSubmit={G3_handleSubmitData_mix}
              validationSchema={validationSchema}
            >
              {(formikProps) => (
                <>
                  <Form>
                    <Box>
                      <Field name="volume">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.volume && form.touched.volume
                            }
                          >
                            <FormLabel htmlFor="max_volume" colorScheme="brand">
                              Maximum Volume: [Mix]
                            </FormLabel>
                            <Input
                              {...field}
                              id="volume"
                              placeholder="Maximum Volume"
                              _placeholder={{ opacity: 1, color: "gray.600" }}
                              // value={formfirstName}
                            />{" "}
                            <FormErrorMessage>
                              {form.errors.volume}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>

                      <Field name="interval_volume">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.volume && form.touched.volume
                            }
                          >
                            <FormLabel
                              htmlFor="interval_volume"
                              colorScheme="brand"
                            >
                              Interval Volume: [Mix]
                            </FormLabel>
                            <Input
                              {...field}
                              id="interval_volume"
                              placeholder="interval Volume"
                              _placeholder={{ opacity: 1, color: "gray.600" }}
                              // value={formfirstName}
                            />{" "}
                            <FormErrorMessage>
                              {form.errors.interval_volume}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>
                    </Box>

                    <HStack>
                      <Button
                        mt={6}
                        colorScheme="red"
                        isLoading={formikProps.isSubmitting}
                        type="submit"
                        disabled={formikProps.isValid} // Disable the button if the form is not valid
                      >
                        Submit
                      </Button>
                    </HStack>
                  </Form>
                </>
              )}
            </Formik>
          </ModalBody>

          {/* <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={deactivate}>
              Close
            </Button>
            <Button variant="ghost">Secondary Action</Button>
          </ModalFooter> */}
        </ModalContent>
      </Modal>

      <Modal
        isOpen={openeditVolumeModal_G3_alcohol}
        onClose={closeModal_G3_alcohol}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Edit Volumes for Alcohol G3</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Formik
              // initialValues={{ firstName: "" }}
              initialValues={{
                volume: "",

                // Add this field with an initial value
              }}
              onSubmit={G3_handleSubmitData_alcohol}
              validationSchema={validationSchema}
            >
              {(formikProps) => (
                <>
                  <Form>
                    <Box>
                      <Field name="volume">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.volume && form.touched.volume
                            }
                          >
                            <FormLabel htmlFor="volume" colorScheme="brand">
                              Maximum Volume: [Alcohol]
                            </FormLabel>
                            <Input
                              {...field}
                              id="interval_volume"
                              placeholder="interval volume"
                              _placeholder={{ opacity: 1, color: "gray.600" }}
                              // value={formfirstName}
                            />{" "}
                            <FormErrorMessage>
                              {form.errors.volume}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>

                      <Field name="interval_volume">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.volume && form.touched.volume
                            }
                          >
                            <FormLabel htmlFor="volume" colorScheme="brand">
                              Interval Volume: [Alcohol]
                            </FormLabel>
                            <Input
                              {...field}
                              id="volume"
                              placeholder="Volume"
                              _placeholder={{ opacity: 1, color: "gray.600" }}
                              // value={formfirstName}
                            />{" "}
                            <FormErrorMessage>
                              {form.errors.volume}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>
                    </Box>

                    <HStack>
                      <Button
                        mt={6}
                        colorScheme="red"
                        isLoading={formikProps.isSubmitting}
                        type="submit"
                        disabled={formikProps.isValid} // Disable the button if the form is not valid
                      >
                        Submit
                      </Button>
                    </HStack>
                  </Form>
                </>
              )}
            </Formik>
          </ModalBody>

          {/* <ModalFooter>
              <Button colorScheme="blue" mr={3} onClick={deactivate}>
                Close
              </Button>
              <Button variant="ghost">Secondary Action</Button>
            </ModalFooter> */}
        </ModalContent>
      </Modal>

      <Modal isOpen={openeditVolumeModal_G4_mix} onClose={closeModal_G4_mix}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Edit Volume for Mix G4</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Formik
              // initialValues={{ firstName: "" }}
              initialValues={{
                volume: "",

                // Add this field with an initial value
              }}
              onSubmit={G4_handleSubmitData_mix}
              validationSchema={validationSchema}
            >
              {(formikProps) => (
                <>
                  <Form>
                    <Box>
                      <Field name="volume">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.volume && form.touched.volume
                            }
                          >
                            <FormLabel htmlFor="max_volume" colorScheme="brand">
                              Maximum Volume: [Mix]
                            </FormLabel>
                            <Input
                              {...field}
                              id="volume"
                              placeholder="Maximum Volume"
                              _placeholder={{ opacity: 1, color: "gray.600" }}
                              // value={formfirstName}
                            />{" "}
                            <FormErrorMessage>
                              {form.errors.volume}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>

                      <Field name="interval_volume">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.volume && form.touched.volume
                            }
                          >
                            <FormLabel
                              htmlFor="interval_volume"
                              colorScheme="brand"
                            >
                              Interval Volume: [Mix]
                            </FormLabel>
                            <Input
                              {...field}
                              id="interval_volume"
                              placeholder="interval Volume"
                              _placeholder={{ opacity: 1, color: "gray.600" }}
                              // value={formfirstName}
                            />{" "}
                            <FormErrorMessage>
                              {form.errors.interval_volume}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>
                    </Box>

                    <HStack>
                      <Button
                        mt={6}
                        colorScheme="red"
                        isLoading={formikProps.isSubmitting}
                        type="submit"
                        disabled={formikProps.isValid} // Disable the button if the form is not valid
                      >
                        Submit
                      </Button>
                    </HStack>
                  </Form>
                </>
              )}
            </Formik>
          </ModalBody>

          {/* <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={deactivate}>
              Close
            </Button>
            <Button variant="ghost">Secondary Action</Button>
          </ModalFooter> */}
        </ModalContent>
      </Modal>

      <Modal
        isOpen={openeditVolumeModal_G4_alcohol}
        onClose={closeModal_G4_alcohol}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Edit Volumes for Alcohol G4</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Formik
              // initialValues={{ firstName: "" }}
              initialValues={{
                volume: "",

                // Add this field with an initial value
              }}
              onSubmit={G4_handleSubmitData_alcohol}
              validationSchema={validationSchema}
            >
              {(formikProps) => (
                <>
                  <Form>
                    <Box>
                      <Field name="volume">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.volume && form.touched.volume
                            }
                          >
                            <FormLabel htmlFor="volume" colorScheme="brand">
                              Maximum Volume: [Alcohol]
                            </FormLabel>
                            <Input
                              {...field}
                              id="interval_volume"
                              placeholder="interval volume"
                              _placeholder={{ opacity: 1, color: "gray.600" }}
                              // value={formfirstName}
                            />{" "}
                            <FormErrorMessage>
                              {form.errors.volume}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>

                      <Field name="interval_volume">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.volume && form.touched.volume
                            }
                          >
                            <FormLabel htmlFor="volume" colorScheme="brand">
                              Interval Volume: [Alcohol]
                            </FormLabel>
                            <Input
                              {...field}
                              id="volume"
                              placeholder="Volume"
                              _placeholder={{ opacity: 1, color: "gray.600" }}
                              // value={formfirstName}
                            />{" "}
                            <FormErrorMessage>
                              {form.errors.volume}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>
                    </Box>

                    <HStack>
                      <Button
                        mt={6}
                        colorScheme="red"
                        isLoading={formikProps.isSubmitting}
                        type="submit"
                        disabled={formikProps.isValid} // Disable the button if the form is not valid
                      >
                        Submit
                      </Button>
                    </HStack>
                  </Form>
                </>
              )}
            </Formik>
          </ModalBody>

          {/* <ModalFooter>
              <Button colorScheme="blue" mr={3} onClick={deactivate}>
                Close
              </Button>
              <Button variant="ghost">Secondary Action</Button>
            </ModalFooter> */}
        </ModalContent>
      </Modal>

      <div className="flex flex-row bg-gray-200">
        <div className="w-[200px] border-2 mt-2 flex flex-col bg-gray-100 rounded-xl m-3">
          <div className="m-3">
            <p className="font-bold text-4xl">Options</p>
          </div>

          <div className="m-5 mt-8 gap-8 flex flex-col">
            <div
              onClick={() => handleMenuItemClick("Dashboard")}
              className={`menu-item p-2 rounded-md cursor-pointer ${
                selectedMenuItem === "Dashboard"
                  ? "bg-blue-500 text-white"
                  : "bg-gray-100"
              }`}
            >
              <p>Dashboard</p>
            </div>
            <div
              onClick={() => handleMenuItemClick("Analytics")}
              className={`menu-item p-2 rounded-md cursor-pointer ${
                selectedMenuItem === "Analytics"
                  ? "bg-blue-500 text-white"
                  : "bg-gray-100"
              }`}
            >
              <p>Analytics</p>
            </div>

            <div>
              <Button colorScheme="blue" onClick={start_measuring_func}>
                Start measurement
              </Button>
            </div>
          </div>
        </div>

        <div className="m-4 flex flex-col gap-2">
          {selectedMenuItem === "Dashboard" && (
            <>
              <div className="border-2 shadow-md w-[800px] h-[280px] rounded-xl bg-gray-100">
                <h2 className="ml-3">Group 1</h2>
                {/* Content for "Calibration" menu item */}
                <div className="flex flex-row">
                  <Chart
                    width={400}
                    height={200}
                    chartType="Gauge"
                    loader={<div>Loading Chart</div>}
                    data={G1_gaugeData}
                    options={{
                      redFrom: 450,
                      redTo: 500,
                      yellowFrom: 400,
                      yellowTo: 500,
                      minorTicks: 5,
                      max: 500,
                    }}
                    rootProps={{ "data-testid": "1" }}
                  />

                  <div className=" flex flex-col w-[200px] justify-center  h-[150px] bg-gray-0 border-2 shadow-lg m-4 rounded-lg">
                    <div className="ml-4 text-sm">
                      {" "}
                      <p className=" font-bold-md text-lg">Mix</p>
                      <div className="flex flex-row gap-1">
                        <p className="mb-1">
                          max vol:{" "}
                          <span className="font-bold text-xl">
                            {incoming_json.Group_1.Mix.max_vol}
                          </span>
                        </p>
                        {/* <BiEditAlt /> */}
                      </div>
                      <div className="flex flex-row gap-1">
                        <p className="mb-1">
                          int vol:{" "}
                          <span className="font-bold text-xl">
                            {incoming_json.Group_1.Mix.interval_vol}
                          </span>
                        </p>

                        {/* <BiEditAlt /> */}
                      </div>
                      <Button
                        onClick={handleConfigurationModal_G1_mix}
                        colorScheme="teal"
                        size="sm"
                      >
                        Edit
                      </Button>
                    </div>
                  </div>

                  <div className=" flex flex-col w-[200px] justify-center  h-[150px] bg-gray-0 border-2 shadow-lg m-4 rounded-lg">
                    <div className="ml-4 text-sm">
                      {" "}
                      <p className=" font-bold-md text-lg">Alcohol</p>
                      <p className="mb-1">
                        max vol:{" "}
                        <span className="font-bold text-xl">
                          {incoming_json.Group_1.Alcohol.max_vol}
                        </span>
                      </p>
                      {/* <p className="font-bold text-2xl ">{19000}</p> */}
                      <p className="mb-1">
                        interval vol:{" "}
                        <span className="font-bold text-xl">
                          {incoming_json.Group_1.Alcohol.interval_vol}
                        </span>
                      </p>
                      <Button
                        onClick={handleConfigurationModal_G1_alcohol}
                        colorScheme="teal"
                        size="sm"
                      >
                        Edit
                      </Button>
                    </div>
                  </div>
                </div>

                <div className="flex flex-row gap-40 ml-20 ">
                  <div className="h-4 w-10 font-bold text-green-600">
                    {incoming_json.Group_1.relay_mix}
                  </div>

                  <div className="h-4 w-10 font-bold text-red-600 ">
                    {incoming_json.Group_1.relay_alcohol}
                  </div>

                  <div className=" ml-44 h-4 w-10 font-bold text-red-600 border-3  ">
                    {/* <Button
                      onClick={handleConfigurationModal_G1}
                      colorScheme="teal"
                      size="sm"
                    >
                      Edit
                    </Button> */}
                  </div>

                  {/* <p className="">TAP:ON</p>
                <p className="">TAP:ON</p> */}
                </div>
              </div>

              {/* START OF GROUP 2 */}

              <div className="border-2 shadow-md w-[800px] h-[280px] rounded-xl bg-gray-100">
                <h2 className="ml-3">Group 2</h2>
                {/* Content for "Calibration" menu item */}
                <div className="flex flex-row">
                  <Chart
                    width={400}
                    height={200}
                    chartType="Gauge"
                    loader={<div>Loading Chart</div>}
                    data={G2_gaugeData}
                    options={{
                      redFrom: 450,
                      redTo: 500,
                      yellowFrom: 400,
                      yellowTo: 500,
                      minorTicks: 5,
                      max: 500,
                    }}
                    rootProps={{ "data-testid": "1" }}
                  />

                  <div className=" flex flex-col w-[200px] justify-center  h-[150px] bg-gray-0 border-2 shadow-lg m-4 rounded-lg">
                    <div className="ml-4 text-sm">
                      {" "}
                      <p className=" font-bold-md text-lg">Mix</p>
                      <div className="flex flex-row gap-1">
                        <p className="mb-1">
                          max vol:{" "}
                          <span className="font-bold text-xl">
                            {incoming_json.Group_2.Mix.max_vol}
                          </span>
                        </p>
                        {/* <BiEditAlt /> */}

                        {/* <Button
                        onClick={handleConfigurationModal_G1_alcohol}
                        colorScheme="teal"
                        size="sm"
                      >
                        Edit
                      </Button> */}
                      </div>
                      {/* <p className="font-bold text-2xl ">{19000}</p> */}
                      <div className="flex flex-row gap-1">
                        <p className="mb-1">
                          int vol:{" "}
                          <span className="font-bold text-xl">
                            {incoming_json.Group_2.Mix.interval_vol}
                          </span>
                        </p>

                        <Button
                          onClick={handleConfigurationModal_G2_mix}
                          colorScheme="teal"
                          size="sm"
                        >
                          Edit
                        </Button>

                        {/* <BiEditAlt /> */}
                      </div>
                    </div>
                  </div>

                  <div className=" flex flex-col w-[200px] justify-center  h-[150px] bg-gray-0 border-2 shadow-lg m-4 rounded-lg">
                    <div className="ml-4 text-sm">
                      {" "}
                      <p className=" font-bold-md text-lg">Alcohol</p>
                      <p className="mb-1">
                        max vol:{" "}
                        <span className="font-bold text-xl">
                          {incoming_json.Group_2.Alcohol.max_vol}
                        </span>
                      </p>
                      {/* <p className="font-bold text-2xl ">{19000}</p> */}
                      <p className="mb-1">
                        interval vol:{" "}
                        <span className="font-bold text-xl">
                          {incoming_json.Group_2.Alcohol.interval_vol}
                        </span>
                      </p>
                      <Button
                        onClick={handleConfigurationModal_G2_alcohol}
                        colorScheme="teal"
                        size="sm"
                      >
                        Edit
                      </Button>
                      {/* <p className="font-bold text-2xl ">{19000}</p> */}
                      {/* <div className="flex flex-row text-center gap-1">
                    {" "}
                    <p className="">Relay Status: </p>
                    <div className="h-4 w-4 bg-green-500 rounded-lg mt-1" />
                  </div> */}
                    </div>
                  </div>
                </div>

                <div className="flex flex-row gap-40 ml-20 ">
                  <div className="h-4 w-10 font-bold text-green-600">
                    {incoming_json.Group_2.relay_mix}
                  </div>

                  <div className="h-4 w-10 font-bold text-red-600 ">
                    {incoming_json.Group_2.relay_alcohol}
                  </div>
                  {/* <p className="">TAP:ON</p>
                <p className="">TAP:ON</p> */}
                </div>
              </div>

              <div className="border-2 shadow-md w-[800px] h-[250px] rounded-xl bg-gray-100">
                <h2 className="ml-3">Group 3</h2>
                {/* Content for "Calibration" menu item */}
                <div className="flex flex-row">
                  <Chart
                    width={400}
                    height={200}
                    chartType="Gauge"
                    loader={<div>Loading Chart</div>}
                    data={G3_gaugeData}
                    options={{
                      redFrom: 450,
                      redTo: 500,
                      yellowFrom: 400,
                      yellowTo: 500,
                      minorTicks: 5,
                      max: 500,
                    }}
                    rootProps={{ "data-testid": "1" }}
                  />

                  <div className=" flex flex-col w-[200px] justify-center  h-[150px] bg-gray-0 border-2 shadow-lg m-4 rounded-lg">
                    <div className="ml-4 text-sm">
                      {" "}
                      <p className=" font-bold-md text-lg">Mix</p>
                      <div className="flex flex-row gap-1">
                        <p className="mb-1">
                          max vol:{" "}
                          <span className="font-bold text-xl">
                            {incoming_json.Group_3.Mix.max_vol}
                          </span>
                        </p>

                        {/* <BiEditAlt /> */}
                      </div>
                      {/* <p className="font-bold text-2xl ">{19000}</p> */}
                      <div className="flex flex-row gap-1">
                        <p className="mb-1">
                          int vol:{" "}
                          <span className="font-bold text-xl">
                            {incoming_json.Group_3.Mix.interval_vol}
                          </span>
                        </p>

                        {/* <BiEditAlt /> */}
                        <Button
                          onClick={handleConfigurationModal_G3_mix}
                          colorScheme="teal"
                          size="sm"
                        >
                          Edit
                        </Button>
                      </div>
                    </div>
                  </div>

                  <div className=" flex flex-col w-[200px] justify-center  h-[150px] bg-gray-0 border-2 shadow-lg m-4 rounded-lg">
                    <div className="ml-4 text-sm">
                      {" "}
                      <p className=" font-bold-md text-lg">Alcohol</p>
                      <p className="mb-1">
                        max vol:{" "}
                        <span className="font-bold text-xl">
                          {incoming_json.Group_3.Alcohol.max_vol}
                        </span>
                      </p>
                      {/* <p className="font-bold text-2xl ">{19000}</p> */}
                      <p className="mb-1">
                        interval vol:{" "}
                        <span className="font-bold text-xl">
                          {incoming_json.Group_3.Alcohol.interval_vol}
                        </span>
                      </p>
                      <Button
                        onClick={handleConfigurationModal_G3_alcohol}
                        colorScheme="teal"
                        size="sm"
                      >
                        Edit
                      </Button>
                      {/* <p className="font-bold text-2xl ">{19000}</p> */}
                      {/* <div className="flex flex-row text-center gap-1">
                    {" "}
                    <p className="">Relay Status: </p>
                    <div className="h-4 w-4 bg-green-500 rounded-lg mt-1" />
                  </div> */}
                    </div>
                  </div>
                </div>

                <div className="flex flex-row gap-48 ml-20 ">
                  <div className="h-4 w-10 font-bold text-green-600">
                    {incoming_json.Group_3.relay_mix}
                  </div>

                  <div className="h-4 w-10 font-bold text-red-600 ">
                    {incoming_json.Group_3.relay_alcohol}
                  </div>
                  {/* <p className="">TAP:ON</p>
                <p className="">TAP:ON</p> */}
                </div>
              </div>

              <div className="border-2 shadow-md w-[800px] h-[250px] rounded-xl bg-gray-100">
                <h2 className="ml-3">Group 4</h2>
                {/* Content for "Calibration" menu item */}
                <div className="flex flex-row">
                  <Chart
                    width={400}
                    height={200}
                    chartType="Gauge"
                    loader={<div>Loading Chart</div>}
                    data={G4_gaugeData}
                    options={{
                      redFrom: 450,
                      redTo: 500,
                      yellowFrom: 400,
                      yellowTo: 500,
                      minorTicks: 5,
                      max: 500,
                    }}
                    rootProps={{ "data-testid": "1" }}
                  />

                  <div className=" flex flex-col w-[200px] justify-center  h-[150px] bg-gray-0 border-2 shadow-lg m-4 rounded-lg">
                    <div className="ml-4 text-sm">
                      {" "}
                      <p className=" font-bold-md text-lg">Mix</p>
                      <div className="flex flex-row gap-1">
                        <p className="mb-1">
                          max vol:{" "}
                          <span className="font-bold text-xl">
                            {incoming_json.Group_4.Mix.max_vol}
                          </span>
                        </p>

                        {/* <BiEditAlt /> */}
                      </div>
                      {/* <p className="font-bold text-2xl ">{19000}</p> */}
                      <div className="flex flex-row gap-1">
                        <p className="mb-1">
                          int vol:{" "}
                          <span className="font-bold text-xl">
                            {incoming_json.Group_4.Mix.interval_vol}
                          </span>
                        </p>

                        {/* <BiEditAlt /> */}
                        <Button
                          onClick={handleConfigurationModal_G4_mix}
                          colorScheme="teal"
                          size="sm"
                        >
                          Edit
                        </Button>
                      </div>
                    </div>
                  </div>

                  <div className=" flex flex-col w-[200px] justify-center  h-[150px] bg-gray-0 border-2 shadow-lg m-4 rounded-lg">
                    <div className="ml-4 text-sm">
                      {" "}
                      <p className=" font-bold-md text-lg">Alcohol</p>
                      <p className="mb-1">
                        max vol:{" "}
                        <span className="font-bold text-xl">
                          {incoming_json.Group_4.Alcohol.max_vol}
                        </span>
                      </p>
                      {/* <p className="font-bold text-2xl ">{19000}</p> */}
                      <p className="mb-1">
                        interval vol:{" "}
                        <span className="font-bold text-xl">
                          {incoming_json.Group_4.Alcohol.interval_vol}
                        </span>
                      </p>
                      <Button
                        onClick={handleConfigurationModal_G4_alcohol}
                        colorScheme="teal"
                        size="sm"
                      >
                        Edit
                      </Button>
                      {/* <p className="font-bold text-2xl ">{19000}</p> */}
                      {/* <div className="flex flex-row text-center gap-1">
                    {" "}
                    <p className="">Relay Status: </p>
                    <div className="h-4 w-4 bg-green-500 rounded-lg mt-1" />
                  </div> */}
                    </div>
                  </div>
                </div>

                <div className="flex flex-row gap-48 ml-20 ">
                  <div className="h-4 w-10 font-bold text-green-600">
                    {incoming_json.Group_3.relay_mix}
                  </div>

                  <div className="h-4 w-10 font-bold text-red-600 ">
                    {incoming_json.Group_3.relay_alcohol}
                  </div>
                  {/* <p className="">TAP:ON</p>
                <p className="">TAP:ON</p> */}
                </div>
              </div>
            </>
          )}
        </div>

        <div>
          {selectedMenuItem === "Analytics" &&
            // <>
            //   <CalibrationComponent />
            // </>

            navigate("/calibration")}
        </div>
      </div>
    </>
  );
};

export default DashBoardComponent;
